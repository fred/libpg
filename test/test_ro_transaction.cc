/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"

#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/pg_ro_transaction.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"
#include "libpg/strong_types.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

#include <stdexcept>
#include <utility>

BOOST_AUTO_TEST_SUITE(TestRoTransaction)

namespace {

void test_exec_in_transaction(const LibPg::PgRoTransaction& transaction)
{
    const LibPg::PgResultTuples dbres = exec(transaction, "SELECT 'foo'::TEXT");
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
    BOOST_CHECK_EQUAL(LibPg::PgResultTuples{exec(static_cast<const LibPg::PgConnection&>(transaction), "SELECT 'bar'::TEXT")}.size(), LibPg::RowIndex{1});
}

constexpr bool dummy_exception_check(const std::exception&)
{
    return true;
}

void test_transaction(LibPg::PgRoTransaction first_transaction)
{
    test_exec_in_transaction(first_transaction);
    LibPg::PgRoTransaction second_transaction{commit(std::move(first_transaction))};
    test_exec_in_transaction(second_transaction);
    LibPg::PgRoTransaction third_transaction{rollback(std::move(second_transaction))};
    try
    {
        exec(third_transaction, "SELECT 'foo'::BAR");
    }
    catch (const LibPg::ExecFailure& e)
    {
        BOOST_CHECK_EQUAL(e.error_code(), LibPg::SqlState::undefined_object);
        LibPg::PgRoTransaction fourth_transaction{rollback(std::move(third_transaction))};
        test_exec_in_transaction(fourth_transaction);
        LibPg::PgRoTransaction fifth_transaction{std::move(fourth_transaction)};
        BOOST_CHECK_EXCEPTION(test_exec_in_transaction(fourth_transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
        test_exec_in_transaction(fifth_transaction);
        commit(std::move(fifth_transaction));
        BOOST_CHECK_EXCEPTION(test_exec_in_transaction(fifth_transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
    }
    catch (const std::exception& e)
    {
        BOOST_TEST_MESSAGE("caught exception: " << e.what());
        throw;
    }
    catch (...)
    {
        BOOST_TEST_MESSAGE("caught unknown exception");
        throw;
    }
}

struct HasTableTestTransaction
{
    HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "CREATE TABLE test_transaction (id INT UNIQUE NOT NULL)");
    }
    ~HasTableTestTransaction()
    {
        try
        {
            const LibPg::PgConnection conn = Test::get_db_conn();
            const LibPg::PgResultNoData dbres = exec(conn, "DROP TABLE test_transaction");
        }
        catch (...)
        {
            BOOST_TEST_MESSAGE("caught unknown exception");
        }
    }
};

}//namespace {anonymous}

BOOST_AUTO_TEST_CASE(test_transaction_from_dsn)
{
    LibPg::PgRoTransaction transaction{Test::get_dsn()};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_CASE(test_transaction_from_conn)
{
    LibPg::PgRoTransaction transaction{Test::get_db_conn()};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_CASE(test_transaction_from_transaction)
{
    LibPg::PgRoTransaction src_transaction{Test::get_db_conn()};
    LibPg::PgRoTransaction transaction{std::move(src_transaction)};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_CASE(test_transaction_isolation_level)
{
    using IsolationLevel = LibPg::StrongString<struct IsolationLevelTag_>;
    using ReadOnlyAccess = LibPg::StrongString<struct ReadOnlyAccessTag_>;
    using Deferrable = LibPg::StrongString<struct DeferrableTag_>;
    static const auto session_default = []()
    {
        const auto result = exec(
                Test::get_db_conn(),
                LibPg::make_query() << "SELECT " << LibPg::item<IsolationLevel>() << "current_setting('default_transaction_isolation')"
                                                 << LibPg::item<Deferrable>() << "current_setting('default_transaction_deferrable')");
        BOOST_REQUIRE(result.size() == LibPg::RowIndex{1});
        return result.front();
    }();
    static const auto check_transaction = [](LibPg::PgRoTransaction transaction,
                                             const IsolationLevel& isolation_level,
                                             const Deferrable& deferrable)
    {
        static const auto query = LibPg::make_query() <<
                "SELECT " << LibPg::item<IsolationLevel>() << "current_setting('transaction_isolation')"
                          << LibPg::item<ReadOnlyAccess>() << "current_setting('transaction_read_only')"
                          << LibPg::item<Deferrable>() << "current_setting('transaction_deferrable')";
        const auto result = exec(transaction, query);
        BOOST_REQUIRE(result.size() == LibPg::RowIndex{1});
        BOOST_CHECK_EQUAL(result.front().get<IsolationLevel>(), isolation_level);
        BOOST_CHECK_EQUAL(result.front().get<ReadOnlyAccess>(), ReadOnlyAccess{"on"});
        BOOST_CHECK_EQUAL(result.front().get<Deferrable>(), deferrable);
        return commit(std::move(transaction));
    };
    auto conn0 = Test::get_db_conn();
    auto conn1 = check_transaction(LibPg::PgRoTransaction{std::move(conn0), LibPg::PgTransaction::session_default},
                                   session_default.get<IsolationLevel>(),
                                   session_default.get<Deferrable>());
    auto conn2 = check_transaction(LibPg::PgRoTransaction{std::move(conn1), LibPg::PgTransaction::read_committed},
                                   IsolationLevel{"read committed"},
                                   session_default.get<Deferrable>());
    auto conn3 = check_transaction(LibPg::PgRoTransaction{std::move(conn2), LibPg::PgTransaction::read_uncommitted},
                                   IsolationLevel{"read uncommitted"},
                                   session_default.get<Deferrable>());
    auto conn4 = check_transaction(LibPg::PgRoTransaction{std::move(conn3), LibPg::PgTransaction::repeatable_read},
                                   IsolationLevel{"repeatable read"},
                                   session_default.get<Deferrable>());
    auto conn5 = check_transaction(LibPg::PgRoTransaction{std::move(conn4), LibPg::PgTransaction::serializable},
                                   IsolationLevel{"serializable"},
                                   session_default.get<Deferrable>());
    auto conn6 = check_transaction(LibPg::PgRoTransaction{std::move(conn5), LibPg::PgTransaction::serializable_deferrable},
                                   IsolationLevel{"serializable"},
                                   Deferrable{"on"});
    auto conn7 = check_transaction(LibPg::PgRoTransaction{std::move(conn6), LibPg::PgTransaction::serializable_not_deferrable},
                                   IsolationLevel{"serializable"},
                                   Deferrable{"off"});
}

BOOST_FIXTURE_TEST_CASE(test_transaction_write, HasTableTestTransaction)
{
    static const auto write_operation = [](const LibPg::PgConnection& conn)
    {
        const auto arg = LibPg::make_fixed_size_query_arguments(0);
        const LibPg::PgResultTuples dbres = exec(conn,
                                                 "INSERT INTO test_transaction(id) "
                                                 "VALUES(" + arg.parameter<0>().as_integer() + ") "
                                                 "RETURNING id",
                                                 arg);
    };
    {
        LibPg::PgRwTransaction transaction{Test::get_dsn()};
        BOOST_CHECK_NO_THROW(write_operation(transaction));
        BOOST_CHECK_NO_THROW(commit(std::move(transaction)));
    }
    {
        LibPg::PgRoTransaction transaction{Test::get_dsn()};
        BOOST_CHECK_EXCEPTION(
                write_operation(transaction),
                LibPg::ExecFailure,
                [](const LibPg::ExecFailure& e) { return e.error_code() == LibPg::SqlState::read_only_sql_transaction; });
        BOOST_CHECK_NO_THROW(commit(std::move(transaction)));
    }
    {
        LibPg::PgRoTransaction transaction{Test::get_dsn()};
        BOOST_CHECK_EXCEPTION(
                write_operation(transaction),
                LibPg::ExecFailure,
                [](const LibPg::ExecFailure& e) { return e.error_code() == LibPg::SqlState::read_only_sql_transaction; });
        BOOST_CHECK_NO_THROW(rollback(std::move(transaction)));
    }
}

BOOST_AUTO_TEST_SUITE_END()//TestRoTransaction
