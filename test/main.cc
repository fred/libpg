/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestLibPg

#include "setup.hh"

#include "libpg/pg_connection.hh"
#include "libpg/libpq_layer.hh"
#include "libpg/detail/libpq_layer_impl.hh"

#include "../src/experimental/strong_type.hh"

#include <boost/test/unit_test.hpp>

#include <cstring>
#include <mutex>
#include <set>
#include <sstream>
#include <utility>

namespace  {

void show_query_argument_value(std::ostream& out, int idx, const char* value)
{
    if (idx != 0)
    {
        out << ", ";
    }
    out << "$" << (idx + 1) << ":";
    if (value == nullptr)
    {
        out << "NULL";
    }
    else
    {
        out << "\"" << value << "\"";
    }
}

auto values_to_string(const char* const* values, int number_of_values)
{
    std::ostringstream out;
    out << "[";
    for (int idx = 0; idx < number_of_values; ++idx)
    {
        show_query_argument_value(out, idx, values[idx]);
    }
    out << "]";
    return std::move(out).str();
}

#if 0
if (this->is_tuples())
{
    if (this->get_number_of_rows() == 1)
    {
        static constexpr int row_idx = 0;
        const auto col_idx_end = this->get_number_of_columns();
        for (int col_idx = 0; col_idx < col_idx_end; ++col_idx)
        {
            const auto col_name = this->get_column_name(col_idx);
            if (0 < col_idx)
            {
                std::cout << " ";
            }
            if (col_name.empty() || (col_name == "?column?"))
            {
                std::cout << col_idx << ':';
            }
            else
            {
                std::cout << '"' << col_name << "\":";
            }
            if (this->is_null(row_idx, col_idx))
            {
                std::cout << "NULL";
            }
            else
            {
                std::cout << '"' << this->get_not_null_value(row_idx, col_idx) << '"';
            }
        }
        std::cout << std::endl;
    }
    else
    {
        std::cout << "rows: " << this->get_number_of_rows() << std::endl;
    }
}
#endif

}//namespace {anonymous}

#undef USE_CONNECTION_POOL

class GlobalFixture
{
public:
    GlobalFixture()
    {
        this->set_my_libpq_interface();
        Test::get_dsn();
    }
    ~GlobalFixture()
    {
        LibPg::set_libpq_layer(nullptr);
    }
private:
    void set_my_libpq_interface()noexcept
    {
        LibPg::set_libpq_layer(static_cast<LibPg::LibPqLayer*>(&my_libpq_layer_impl_));
    }
    class LibPq : public LibPg::Detail::LibPqLayerSimpleImplementation
    {
    public:
        LibPq()
            : mutable_{&mutable_data_}
        { }
        ~LibPq()override
        {
        }
    private:
        using Parent = LibPg::Detail::LibPqLayerSimpleImplementation;
        ::PGconn* PQconnectdbParams(const char* const* keywords,
                                    const char* const* values,
                                    int expand_dbname)const override
        {
#ifdef USE_CONNECTION_POOL
            auto* const conn = [this, keywords, values, expand_dbname]()
            {
                auto* const conn = mutable_->from_pool(keywords, values, expand_dbname);
                if (conn != nullptr)
                {
                    return conn;
                }
                return this->Parent::PQconnectdbParams(keywords, values, expand_dbname);
            }();
#else
            auto* const conn = this->Parent::PQconnectdbParams(keywords, values, expand_dbname);
#endif
            mutable_->on_create(conn);
            this->PQsetNoticeProcessor(conn, [](void*, const char* message) { BOOST_TEST_MESSAGE((std::string{message, std::strlen(message) - 1})); }, nullptr);
            return conn;
        }
        ::PGresult* PQexec(::PGconn* conn, const char* query)const override
        {
            BOOST_TEST_MESSAGE("query: " << query);
            auto* const res = this->Parent::PQexec(conn, query);
            mutable_->on_create(res);
            return res;
        }
        ::PGresult* PQexecParams(::PGconn* conn,
                                 const char* command,
                                 int nParams,
                                 const ::Oid* paramTypes,
                                 const char* const* paramValues,
                                 const int* paramLengths,
                                 const int* paramFormats,
                                 int resultFormat)const override
        {
            BOOST_TEST_MESSAGE("query: " << command << " " << values_to_string(paramValues, nParams));
            auto* const res = this->Parent::PQexecParams(conn, command, nParams, paramTypes, paramValues, paramLengths, paramFormats, resultFormat);
            mutable_->on_create(res);
            return res;
        }
        void PQfinish(::PGconn* conn)const override
        {
            mutable_->on_destroy(conn);
#ifdef USE_CONNECTION_POOL
            if (!mutable_->store_in_pool(conn))
            {
                this->Parent::PQfinish(conn);
            }
#else
            this->Parent::PQfinish(conn);
#endif
        }
        void PQclear(::PGresult* res)const override
        {
            mutable_->on_destroy(res);
            return this->Parent::PQclear(res);
        }
        class Mutable
        {
        public:
            ~Mutable()
            {
                const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                if (!pg_conns_.empty())
                {
                    std::cerr << pg_conns_.size() << " connections not freed" << std::endl;
                }
                if (!pg_results_.empty())
                {
                    std::cerr << pg_results_.size() << " results not freed" << std::endl;
                }
#ifdef USE_CONNECTION_POOL
                for (auto* const conn : connection_pool_)
                {
                    ::PQfinish(conn);
                }
                connection_pool_.clear();
#endif
            }
            void on_create(const ::PGconn* conn)
            {
                const bool insertion_took_place = [conn, this]()
                {
                    const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                    return pg_conns_.insert(conn).second;
                }();
                BOOST_CHECK_MESSAGE(insertion_took_place, "existing connection created");
            }
            void on_destroy(const ::PGconn* conn)
            {
                const bool exists = [conn, this]()
                {
                    const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                    return 0 < pg_conns_.erase(conn);
                }();
                BOOST_CHECK_MESSAGE(exists , "connection does not exist");
            }
            void on_create(const ::PGresult* res)
            {
                const bool insertion_took_place = [res, this]()
                {
                    const std::unique_lock<std::mutex> lock{this->mutex_, std::defer_lock};
                    return this->pg_results_.insert(res).second;
                }();
                BOOST_CHECK_MESSAGE(insertion_took_place, "existing result created");
            }
            void on_destroy(const ::PGresult* res)
            {
                const bool exists = [res, this]()
                {
                    const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                    return 0 < pg_results_.erase(res);
                }();
                BOOST_CHECK_MESSAGE(exists, "result does not exist");
            }
#ifdef USE_CONNECTION_POOL
            ::PGconn* from_pool(const char* const*,
                                const char* const*,
                                int)
            {
                const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                if (!connection_pool_.empty())
                {
                    auto top = connection_pool_.begin();
                    auto* const conn = *top;
                    connection_pool_.erase(top);
                    return conn;
                }
                return nullptr;
            }
            bool store_in_pool(::PGconn* conn)
            {
                if ((::PQstatus(conn) != ::CONNECTION_OK) ||
                    (::PQtransactionStatus(conn) != ::PQTRANS_IDLE))
                {
                    switch (::PQtransactionStatus(conn))
                    {
                        case ::PQTRANS_ACTIVE:
                            std::cout << "PQTRANS_ACTIVE" << std::endl;
                            break;
                        case ::PQTRANS_IDLE:
                            std::cout << "PQTRANS_IDLE" << std::endl;
                            break;
                        case ::PQTRANS_INERROR:
                            std::cout << "PQTRANS_INERROR" << std::endl;
                            break;
                        case ::PQTRANS_INTRANS:
                            std::cout << "PQTRANS_INTRANS" << std::endl;
                            break;
                        case ::PQTRANS_UNKNOWN:
                            std::cout << "PQTRANS_UNKNOWN" << std::endl;
                            break;
                    }
                    BOOST_TEST_MESSAGE("connection can not be stored in the pool");
                    const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                    connection_pool_.erase(conn);
                    return false;
                }
                const std::unique_lock<std::mutex> lock{mutex_, std::defer_lock};
                connection_pool_.insert(conn);
                return true;
            }
#endif
        private:
            std::mutex mutex_;
            std::set<const ::PGconn*> pg_conns_;
            std::set<const ::PGresult*> pg_results_;
#ifdef USE_CONNECTION_POOL
            std::set<::PGconn*> connection_pool_;
#endif
        } mutable_data_;
        Mutable* mutable_;
    };
    LibPq my_libpq_layer_impl_;
};

namespace Test {

const LibPg::Dsn& get_dsn()
{
    static const LibPg::Dsn dsn = []()
    {
        LibPg::Dsn dsn;
        dsn.host = LibPg::Dsn::Host{"localhost"};
        dsn.host_addr = LibPg::Dsn::HostAddr{boost::asio::ip::address::from_string("127.0.0.1")};
        dsn.port = LibPg::Dsn::Port{std::uint16_t{11112}};
        dsn.db_name = LibPg::Dsn::DbName{"fred"};
        dsn.user = LibPg::Dsn::User{"fred"};
        dsn.password = LibPg::Dsn::Password{"password"};
        dsn.connect_timeout = LibPg::Dsn::ConnectTimeout{std::chrono::seconds{2}};
        return dsn;
    }();
    return dsn;
}

LibPg::PgConnection get_db_conn()
{
    return LibPg::PgConnection{get_dsn()};
}

}//namespace Test

BOOST_GLOBAL_FIXTURE(GlobalFixture);
