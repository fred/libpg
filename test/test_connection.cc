/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "setup.hh"

#include "libpg/pg_connection.hh"
#include "libpg/pg_exception.hh"

#include <boost/test/unit_test.hpp>

#include <stdexcept>

BOOST_AUTO_TEST_SUITE(TestConnection)

BOOST_AUTO_TEST_CASE(test_dsn)
{
    const auto dsn = []()
    {
        LibPg::Dsn dsn;
        dsn.host = LibPg::Dsn::Host{"dbmaster"};
        dsn.host_addr = LibPg::Dsn::HostAddr{boost::asio::ip::address::from_string("2001:1488:fffe:2::11")};
        dsn.port = LibPg::Dsn::Port{std::uint16_t{5432}};
        dsn.db_name = LibPg::Dsn::DbName{"fred"};
        dsn.user = LibPg::Dsn::User{"view"};
        dsn.password = LibPg::Dsn::Password{"bflmpsvz"};
        dsn.connect_timeout = LibPg::Dsn::ConnectTimeout{std::chrono::seconds(2)};
        return dsn;
    };
    BOOST_CHECK_NO_THROW(dsn());
}

BOOST_AUTO_TEST_CASE(test_pg_connection)
{
    BOOST_CHECK_NO_THROW(const LibPg::PgConnection conn = Test::get_db_conn());
}

BOOST_AUTO_TEST_CASE(test_pg_connection_connect_failure)
{
    BOOST_CHECK_EXCEPTION(
            const LibPg::PgConnection conn{[]()
                                           {
                                               LibPg::Dsn dsn;
                                               dsn.host = LibPg::Dsn::Host{"localhost"};
                                               dsn.host_addr = LibPg::Dsn::HostAddr{boost::asio::ip::address::from_string("127.0.0.10")};
                                               dsn.port = LibPg::Dsn::Port{std::uint16_t{11112}};
                                               dsn.db_name = LibPg::Dsn::DbName{"fred"};
                                               dsn.user = LibPg::Dsn::User{"fred"};
                                               dsn.password = LibPg::Dsn::Password{"password"};
                                               dsn.connect_timeout = LibPg::Dsn::ConnectTimeout{std::chrono::seconds{2}};
                                               return dsn;
                                           }()},
            LibPg::ConnectFailure,
            [](const LibPg::ConnectFailure&) { return true; });
}

BOOST_AUTO_TEST_SUITE_END()//TestConnection
