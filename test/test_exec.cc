/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"
#include "use.hh"

#include "libpg/pg_connection.hh"
#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/strong_types.hh"
#include "libpg/sql_state/code.hh"
#include "libpg/query.hh"

#include <boost/test/unit_test.hpp>

#include <stdexcept>
#include <utility>

namespace {

enum class TestEnum
{
    first,
    second,
    last = second
};

decltype(auto) wrap_into_psql_representation(TestEnum value)
{
    switch (value)
    {
        case TestEnum::first: return std::string{"first"};
        case TestEnum::second: return std::string{"second"};
    }
    throw std::runtime_error("unexpected value");
}

TestEnum unwrap_from_psql_representation(const TestEnum&, const char* src)
{
    if (src == nullptr)
    {
        struct MissingValue : std::invalid_argument
        {
            MissingValue() : std::invalid_argument{"value is NULL"} {}
        };
        throw MissingValue{};
    }
    if (src == wrap_into_psql_representation(TestEnum::first))
    {
        return TestEnum::first;
    }
    if (src == wrap_into_psql_representation(TestEnum::second))
    {
        return TestEnum::second;
    }
    if (src == std::string{"last"})
    {
        return TestEnum::last;
    }
    struct InvalidValue : std::invalid_argument
    {
        InvalidValue() : std::invalid_argument{"unable to convert to Enum value"} {}
    };
    throw InvalidValue{};
}

template <int number_of_columns>
struct ColumnsRequired
{
    ColumnsRequired(LibPg::PgResult::ConstValuePseudoIterator begin, LibPg::PgResult::ConstValuePseudoIterator end)
    {
        if ((begin + number_of_columns) != end)
        {
            throw std::runtime_error{"unexpected number of columns"};
        }
    }
};

std::ostream& operator<<(std::ostream& out, TestEnum value)
{
    return out << wrap_into_psql_representation(value);
}

template <typename ExpectedErrorCode>
auto has_error_code(ExpectedErrorCode)
{
    return [](const LibPg::ExecFailure& e)
        {
            return e.error_code() == ExpectedErrorCode{};//compare runtime error code with compile time constant
        };
}

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestExec)

namespace {

void check_result(const LibPg::PgResultTuples& dbres)
{
    enum class Enum
    {
        a,
        b,
        c
    };
    static const auto enum_from_string = [](const std::string& value)
    {
        if (value == "a")
        {
            return Enum::a;
        }
        if (value == "b")
        {
            return Enum::b;
        }
        if (value == "c")
        {
            return Enum::c;
        }
        struct InvalidValue : std::invalid_argument
        {
            InvalidValue() : std::invalid_argument{"unable to convert to Enum value"} {}
        };
        throw InvalidValue{};
    };
    static const auto enum_from_value = [](const LibPg::PgResult::Value& value)
    {
        if (value.is_null())
        {
            struct MissingValue : std::invalid_argument
            {
                MissingValue() : std::invalid_argument{"value is NULL"} {}
            };
            throw MissingValue{};
        }
        return enum_from_string(value.as<std::string>());
    };
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<int>(), 1);
    using SomeId = LibStrong::ArithmeticSequence<int, struct SomeIdTag_>;
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<SomeId>(), SomeId{1});
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{1}].as<bool>(), true);
    BOOST_CHECK(enum_from_value(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{2}]) == Enum::a);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{3}].as<TestEnum>(), TestEnum::first);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{4}].as<TestEnum>(), TestEnum::second);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{5}].as<TestEnum>(), TestEnum::last);
    {
        const auto row = dbres[LibPg::RowIndex{0}];
        BOOST_CHECK_EQUAL(row[LibPg::ColumnIndex{0}].as<int>(), 1);
        BOOST_CHECK_EQUAL(row[LibPg::ColumnIndex{0}].as<SomeId>(), SomeId{1});
        BOOST_CHECK_EQUAL(row[LibPg::ColumnIndex{1}].as<bool>(), true);
        BOOST_CHECK(enum_from_value(row[LibPg::ColumnIndex{2}]) == Enum::a);
        BOOST_CHECK_EQUAL(row[LibPg::ColumnIndex{3}].as<TestEnum>(), TestEnum::first);
        BOOST_CHECK_EQUAL(row[LibPg::ColumnIndex{4}].as<TestEnum>(), TestEnum::second);
        BOOST_CHECK_EQUAL(row[LibPg::ColumnIndex{5}].as<TestEnum>(), TestEnum::last);
    }
    {
        const auto row = dbres[LibPg::RowIndex{0}].as<std::tuple<SomeId, bool, std::string, TestEnum, TestEnum, TestEnum>>();
        BOOST_CHECK_EQUAL(std::get<0>(row), SomeId{1});
        BOOST_CHECK_EQUAL(std::get<1>(row), true);
        BOOST_CHECK(enum_from_string(std::get<2>(row)) == Enum::a);
        BOOST_CHECK_EQUAL(std::get<3>(row), TestEnum::first);
        BOOST_CHECK_EQUAL(std::get<4>(row), TestEnum::second);
        BOOST_CHECK_EQUAL(std::get<5>(row), TestEnum::last);
    }
    {
        struct MyRow
        {
            MyRow(LibPg::PgResult::ConstValuePseudoIterator begin, LibPg::PgResult::ConstValuePseudoIterator end)
                : check_number_of_columns{begin, end},
                  id{(*begin).as<SomeId>()},
                  is_true{(*(begin + 1)).as<bool>()},
                  my_enum{enum_from_value(*(begin + 2))},
                  test_enums{(*(begin + 3)).as<TestEnum>(), (*(begin + 4)).as<TestEnum>(), (*(begin + 5)).as<TestEnum>()}
            { }
            ColumnsRequired<6> check_number_of_columns;
            SomeId id;
            bool is_true;
            Enum my_enum;
            std::array<TestEnum, 3> test_enums;
        };

        const auto row = LibPg::row_cast<MyRow>(dbres[LibPg::RowIndex{0}]);
        BOOST_CHECK_EQUAL(row.id, SomeId{1});
        BOOST_CHECK_EQUAL(row.is_true, true);
        BOOST_CHECK(row.my_enum == Enum::a);
        BOOST_CHECK_EQUAL(row.test_enums[0], TestEnum::first);
        BOOST_CHECK_EQUAL(row.test_enums[1], TestEnum::second);
        BOOST_CHECK_EQUAL(row.test_enums[2], TestEnum::last);
    }
    {
        struct MyRow
        {
            MyRow(LibPg::PgResult::Row columns)
                : check_number_of_columns{columns.begin(), columns.end()},
                  id{columns[LibPg::ColumnIndex{0}].as<SomeId>()},
                  is_true{columns[LibPg::ColumnIndex{1}].as<bool>()},
                  my_enum{enum_from_value(columns[LibPg::ColumnIndex{2}])},
                  test_enums{columns[LibPg::ColumnIndex{3}].as<TestEnum>(),
                             columns[LibPg::ColumnIndex{4}].as<TestEnum>(),
                             columns[LibPg::ColumnIndex{5}].as<TestEnum>()}
            { }
            ColumnsRequired<6> check_number_of_columns;
            SomeId id;
            bool is_true;
            Enum my_enum;
            std::array<TestEnum, 3> test_enums;
        };

        const auto row = dbres[LibPg::RowIndex{0}].as<MyRow>();
        BOOST_CHECK_EQUAL(row.id, SomeId{1});
        BOOST_CHECK_EQUAL(row.is_true, true);
        BOOST_CHECK(row.my_enum == Enum::a);
        BOOST_CHECK_EQUAL(row.test_enums[0], TestEnum::first);
        BOOST_CHECK_EQUAL(row.test_enums[1], TestEnum::second);
        BOOST_CHECK_EQUAL(row.test_enums[2], TestEnum::last);
    }
    for (const auto row : dbres)
    {
        int column_idx = 0;
        for (const auto value : row)
        {
            switch (column_idx)
            {
                case 0:
                {
                    BOOST_CHECK_EQUAL(value.as<int>(), 1);
                    int i;
                    value.into(i);
                    BOOST_CHECK_EQUAL(i, 1);
                    BOOST_CHECK_EQUAL(LibPg::value_cast<SomeId>(value), SomeId{i});
                    BOOST_CHECK_EQUAL(value.as<SomeId>(), SomeId{1});
                    SomeId id{0};
                    value.into(id);
                    BOOST_CHECK_EQUAL(id, SomeId{i});
                    break;
                }
                case 1:
                {
                    BOOST_CHECK_EQUAL(value.as<bool>(), true);
                    bool v;
                    value.into(v);
                    BOOST_CHECK_EQUAL(v, true);
                    break;
                }
                case 2:
                {
                    BOOST_CHECK(enum_from_value(value) == Enum::a);
                    break;
                }
                case 3:
                {
                    BOOST_CHECK_EQUAL(value.as<TestEnum>(), TestEnum::first);
                    TestEnum v;
                    value.into(v);
                    BOOST_CHECK_EQUAL(v, TestEnum::first);
                    break;
                }
                case 4:
                {
                    BOOST_CHECK_EQUAL(value.as<TestEnum>(), TestEnum::second);
                    TestEnum v;
                    value.into(v);
                    BOOST_CHECK_EQUAL(v, TestEnum::second);
                    break;
                }
                case 5:
                {
                    BOOST_CHECK_EQUAL(value.as<TestEnum>(), TestEnum::second);
                    BOOST_CHECK_EQUAL(value.as<TestEnum>(), TestEnum::last);
                    TestEnum v;
                    value.into(v);
                    BOOST_CHECK_EQUAL(v, TestEnum::second);
                    BOOST_CHECK_EQUAL(v, TestEnum::last);
                    break;
                }
                default:
                    BOOST_CHECK_EQUAL(column_idx < 6, true);
                    break;
            }
            ++column_idx;
        }
        BOOST_CHECK_EQUAL(column_idx, 6);
    }
}

void report_error(const LibPg::ExecFailure& e)
{
    if (e.error_code() == LibPg::SqlState::integrity_constraint_violation_class)
    {
        BOOST_TEST_MESSAGE("integrity_constraint_violation_class " << e.error_code() << " exception caught: " << e.what());
    }
    else if (e.error_code() == LibPg::SqlState::foreign_key_violation)
    {
        BOOST_TEST_MESSAGE("foreign_key_violation exception caught: " << e.what());
    }
    else if (e.error_code() == LibPg::SqlState::protocol_violation)
    {
        BOOST_TEST_MESSAGE("protocol_violation exception caught: " << e.what());
    }
    else
    {
        BOOST_TEST_MESSAGE("unexpected exception caught: " << e.what() << "; error code = \"" << e.error_code() << "\"");
    }
}

}//namespace {anonymous}

BOOST_AUTO_TEST_CASE(test_pg_exec_1)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    try
    {
        const LibPg::PgResultTuples dbres = exec(conn, "SELECT 1,1=1,'a','first','second','last'");
        check_result(dbres);
    }
    catch (const LibPg::ExecFailure& e)
    {
        report_error(e);
    }
}

BOOST_AUTO_TEST_CASE(test_pg_exec_2)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    try
    {
        LibPg::ExtendableQueryArguments arguments{};
        const std::string query = "SELECT " + arguments.add(1).as_integer() + ","
                                         "1=" + arguments.add(1).as_integer() + "," +
                                          arguments.add("a").as_text() + "," +
                                          arguments.add(TestEnum::first).as_text() + "," +
                                          arguments.add(TestEnum::second).as_text() + "," +
                                          arguments.add("last").as_text();
        const LibPg::PgResultTuples dbres = exec(conn, query, arguments);
        check_result(dbres);
    }
    catch (const LibPg::ExecFailure& e)
    {
        report_error(e);
    }
}

BOOST_AUTO_TEST_CASE(test_pg_exec_3)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    try
    {
        const LibPg::PgResultTuples dbres = exec(conn, "SELECT $1::INT,"
                                                              "$2::BOOLEAN,"
                                                              "$3::TEXT,"
                                                              "$4::TEXT,"
                                                              "$5::TEXT,"
                                                              "$6::TEXT", LibPg::make_fixed_size_query_arguments(1, true, "a", "first", "second", "last"));
        check_result(dbres);
    }
    catch (const LibPg::ExecFailure& e)
    {
        report_error(e);
    }
}

BOOST_AUTO_TEST_CASE(test_pg_exec_4)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    try
    {
        using ContactId = typename LibStrong::ArithmeticSequence<int, struct ContactIdTag_>::Optional;
        using ContactName = LibPg::StrongString<struct ContactNameTag_>;
        using ContactValidFrom = LibPg::StrongComparable<TestEnum, struct ContactValidFromTag_>;
        using ContactValidTo = LibPg::StrongComparable<TestEnum, struct ContactValidToTag_>;
        using ContactBirthdate = LibPg::StrongString<struct ContactBirthdateTag_>;
        using Args = LibPg::NamedQueryArguments<ContactId, ContactName, ContactValidFrom, ContactValidTo, ContactBirthdate>;
        const std::string query = "SELECT COALESCE(" + Args::parameter<ContactId>().as_integer() + ",1)," +
                                          Args::parameter<ContactId>().as_integer() + " IS NULL," +
                                          Args::parameter<ContactName>().as_text() + "," +
                                          Args::parameter<ContactValidFrom>().as_text() + "," +
                                          Args::parameter<ContactValidTo>().as_text() + "," +
                                          Args::parameter<ContactBirthdate>().as_text();
        const LibPg::PgResultTuples dbres = exec(conn, query, Args{ContactName{"a"},
                                                                   ContactValidFrom{TestEnum::first},
                                                                   ContactValidTo{TestEnum::second},
                                                                   ContactBirthdate{"last"},
                                                                   ContactId{}});
        check_result(dbres);
    }
    catch (const LibPg::ExecFailure& e)
    {
        report_error(e);
    }
}

BOOST_AUTO_TEST_CASE(test_fixed_size_query_arguments)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    {
        const auto args = LibPg::make_fixed_size_query_arguments()
                                  .push_back("nazdar")
                                  .push_back("prde")
                                  .push_back("jak")
                                  .push_back("je?");
        const auto query = "SELECT " + args.parameter<0>().as_varchar<>() + "," +
                                       args.parameter<1>().as_varchar<4>() + "," +
                                       args.parameter<2>().as_char<3>() + "," +
                                       args.parameter<3>().as_text();
        const LibPg::PgResultTuples dbres = exec(conn, query, args);
        BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
        BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<std::string>(), "nazdar");
        BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{1}].as<std::string>(), "prde");
        BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{2}].as<std::string>(), "jak");
        BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{3}].as<std::string>(), "je?");
    }
    BOOST_CHECK_EQUAL(Test::Use{conn}.select("nazdar").of_type<LibPg::PsqlType::VarChar<1>>().as<std::string>(), "n");
    BOOST_CHECK_EQUAL(Test::Use{conn}.select("prde").of_type<LibPg::PsqlType::VarChar<5>>().as<std::string>(), "prde");
    BOOST_CHECK_EQUAL(Test::Use{conn}.select("jak").of_type<LibPg::PsqlType::Char<>>().as<std::string>(), "j");
    BOOST_CHECK_EQUAL(Test::Use{conn}.select("je?").of_type<LibPg::PsqlType::Char<4>>().as<std::string>(), "je? ");

    BOOST_CHECK_EQUAL(Test::Use{conn}.select(false).of_type<LibPg::PsqlType::Boolean>().as<bool>(), false);
    BOOST_CHECK_EQUAL(Test::Use{conn}.select(true).of_type<LibPg::PsqlType::Boolean>().as<bool>(), true);
    BOOST_CHECK_EQUAL(Test::Use{conn}.select(LibPg::null_value).of_type<LibPg::PsqlType::Boolean>().is_null(), true);
}

BOOST_AUTO_TEST_CASE(test_varchar_too_long)
{
    LibPg::PgRwTransaction transaction{Test::get_db_conn()};
    BOOST_CHECK_NO_THROW(exec(transaction, LibPg::make_query("CREATE TABLE foo (bar VARCHAR(5))")));
    static const auto insert_bar_into_foo = LibPg::make_query() << "INSERT INTO foo (bar) VALUES (" << LibPg::parameter<std::string>().as_text() << ")";
    BOOST_CHECK_NO_THROW(exec(transaction, insert_bar_into_foo, std::string{"áéíóú"}));
    BOOST_CHECK_EXCEPTION(exec(transaction, insert_bar_into_foo, std::string{"áéíóúů"}),
                          LibPg::ExecFailure, has_error_code(LibPg::SqlState::string_data_right_truncation));
    rollback(std::move(transaction));
}

BOOST_AUTO_TEST_SUITE_END()//TestExec
