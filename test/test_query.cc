/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"

#include "libpg/query.hh"

#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/strong_types.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

#include <exception>
#include <iostream>

namespace {

bool dummy_check(const LibPg::PgException& e)
{
    BOOST_TEST_MESSAGE("PgException: " << e.what());
    return true;
}

//demonstrates error code usage
template <typename ErrorCode>
auto has_error_code(ErrorCode)
{
    return [](const LibPg::ExecFailure& e)
    {
        BOOST_TEST_MESSAGE("PgException: " << e.what() << ", error code = " << e.error_code());
        using ExpectedErrorCode = ErrorCode;
        return e.error_code() == ExpectedErrorCode{};//compare runtime error code with compile time constant
    };
}

using ObjectId = LibStrong::ArithmeticSequence<int, struct ObjectIdTag_>;
using Length = LibStrong::ArithmeticSequence<int, struct LengthTag_>;

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestQuery)

BOOST_AUTO_TEST_CASE(test_query)
{
    const auto conn = Test::get_db_conn();
    {
        const LibPg::PgResultTuples dbres = exec(conn,
                                                 "SELECT 1,2,3");
        BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
        const auto row = LibPg::row_cast<std::tuple<int, int, int>>(*dbres.begin());
        BOOST_CHECK_EQUAL(std::get<0>(row), 1);
        BOOST_CHECK_EQUAL(std::get<1>(row), 2);
        BOOST_CHECK_EQUAL(std::get<2>(row), 3);
    }
    {
        using Args = LibPg::FixedSizeQueryArguments<3>;
        const LibPg::PgResultTuples dbres = exec(conn,
                                                 "SELECT " + Args::parameter<0>().as_integer() + "," +
                                                             Args::parameter<1>().as_integer() + "," +
                                                             Args::parameter<2>().as_integer(),
                                                 LibPg::make_fixed_size_query_arguments(1, 2, 3));
        BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
        const auto row = LibPg::row_cast<std::tuple<int, int, int>>(*dbres.begin());
        BOOST_CHECK_EQUAL(std::get<0>(row), 1);
        BOOST_CHECK_EQUAL(std::get<1>(row), 2);
        BOOST_CHECK_EQUAL(std::get<2>(row), 3);
    }
    {
        using Args = LibPg::FixedSizeQueryArguments<3>;
        const LibPg::PgResultTuples dbres = exec(conn,
                                                 "SELECT " + Args::parameter<0>().as_integer() + "," +
                                                             Args::parameter<1>().as_integer() + "," +
                                                             Args::parameter<2>().as_integer(),
                                                 1, 2, 3);
        BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
        const auto row = LibPg::row_cast<std::tuple<int, int, int>>(*dbres.begin());
        BOOST_CHECK_EQUAL(std::get<0>(row), 1);
        BOOST_CHECK_EQUAL(std::get<1>(row), 2);
        BOOST_CHECK_EQUAL(std::get<2>(row), 3);
    }
    {
        const auto query = LibPg::make_query("BEGIN");
        exec(conn, query);
        exec(conn, LibPg::make_query("ROLLBACK"));
    }
    BOOST_CHECK_EXCEPTION((LibPg::PgResultNoData{exec(conn, LibPg::make_query(""))}), LibPg::PgException, dummy_check);
    BOOST_CHECK_EXCEPTION((exec(conn, LibPg::make_query("SELE"))), LibPg::ExecFailure, has_error_code(LibPg::SqlState::syntax_error));
#if 0
    const auto query = LibPg::make_query() <<
            "SELECT" << LibPg::item<ContactId>{} << "c.id" <<
                        LibPg::item<Street>{} << "COALESCE(c.street,'') "
            "FROM contact c "
            "JOIN domain d ON d.registrant_id=c.id "
            "WHERE d.id=" << LibPg::parameter<DomainId>().as_big_int();
    const auto domain_info_rows = exec(conn, query, {DomainId{5}});
    expected(domain_info_rows.size() <= 1);
    for (const auto domain_info : domain_info_rows)
    {
        expected(domain_info.get<ContactId>() == ContactId{7});
        expected(domain_info.get<Street>() == "Milešovská");
    }
    expected(exec(conn,
                  (LibPg::make_query() <<
                           "SELECT " + LibPg::ColumnIgnore{"0"} + " "
                           "FROM contact "
                           "WHERE id=" + LibPg::parameter_big_int<DomainId>()),
                  {DomainId{0}}).empty());
#endif
    {
        const auto query = LibPg::make_query("INSERT INTO dummy_table(id,len,max_len) VALUES(") << LibPg::parameter<ObjectId>().as_big_int() << ","
                                                                                                << LibPg::parameter<Length>().as_integer() << ","
                                                                                                << LibPg::parameter<Length>().as_big_int() << "+1)";
        BOOST_CHECK_EXCEPTION((exec(conn, query, {ObjectId{1}, Length{42}})), LibPg::ExecFailure, has_error_code(LibPg::SqlState::undefined_table));
        BOOST_CHECK_EXCEPTION((exec(conn, query, {Length{1}, ObjectId{42}})), LibPg::ExecFailure, has_error_code(LibPg::SqlState::undefined_table));
    }
    {
        using MaxLength = LibPg::StrongCountableStreamable<int, struct MaxLengthTag_>;
        const auto query = LibPg::make_query("SELECT") << LibPg::item<ObjectId>() << LibPg::parameter<ObjectId>().as_big_int()
                                                       << LibPg::item<Length>() << LibPg::parameter<Length>().as_integer()
                                                       << LibPg::item<MaxLength>() << LibPg::parameter<Length>().as_big_int() << "+1";
        {
            const auto dbres = exec(conn, query, {ObjectId{1}, Length{42}});
            BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
            BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{3});
            for (const auto row : dbres)
            {
                BOOST_CHECK_EQUAL(row.get<ObjectId>(), ObjectId{1});
                BOOST_CHECK_EQUAL(row.get<Length>(), Length{42});
                BOOST_CHECK_EQUAL(row.get<MaxLength>(), MaxLength{43});
            }
        }
        {
            const auto dbres = exec(conn, query, {Length{42}, ObjectId{1}});
            BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
            BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{3});
            for (const auto row : dbres)
            {
                BOOST_CHECK_EQUAL(row.get<ObjectId>(), ObjectId{1});
                BOOST_CHECK_EQUAL(row.get<Length>(), Length{42});
                BOOST_CHECK_EQUAL(row.get<MaxLength>(), MaxLength{43});
            }
        }
    }
}

BOOST_AUTO_TEST_SUITE_END()//TestQuery
