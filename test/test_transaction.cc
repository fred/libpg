/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"

#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"
#include "libpg/strong_types.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

#include <stdexcept>
#include <utility>

BOOST_AUTO_TEST_SUITE(TestTransaction)

namespace {

void test_exec_in_transaction(const LibPg::PgTransaction& transaction)
{
    const LibPg::PgResultTuples dbres = exec(transaction, "SELECT 'foo'::TEXT");
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
    BOOST_CHECK_EQUAL(LibPg::PgResultTuples{exec(static_cast<const LibPg::PgConnection&>(transaction), "SELECT 'bar'::TEXT")}.size(), LibPg::RowIndex{1});
}

constexpr bool dummy_exception_check(const std::exception&)
{
    return true;
}

void test_transaction(LibPg::PgRwTransaction&& first_transaction)
{
    test_exec_in_transaction(first_transaction);
    LibPg::PgRwTransaction second_transaction{commit(std::move(first_transaction))};
    test_exec_in_transaction(second_transaction);
    LibPg::PgRwTransaction third_transaction{rollback(std::move(second_transaction))};
    try
    {
        exec(third_transaction, "SELECT 'foo'::BAR");
    }
    catch (const LibPg::ExecFailure& e)
    {
        static constexpr auto undefined_object = LibPg::SqlState::make_sql_state_class<'4', '2'>().subclass<'7', '0', '4'>();
        BOOST_CHECK_EQUAL(e.error_code(), undefined_object);
        LibPg::PgRwTransaction fourth_transaction{rollback(std::move(third_transaction))};
        test_exec_in_transaction(fourth_transaction);
        LibPg::PgRwTransaction fifth_transaction{std::move(fourth_transaction)};
        BOOST_CHECK_EXCEPTION(test_exec_in_transaction(fourth_transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
        test_exec_in_transaction(fifth_transaction);
        commit(std::move(fifth_transaction));
        BOOST_CHECK_EXCEPTION(test_exec_in_transaction(fifth_transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
    }
    catch (const std::exception& e)
    {
        BOOST_TEST_MESSAGE("caught exception: " << e.what());
        throw;
    }
    catch (...)
    {
        BOOST_TEST_MESSAGE("caught unknown exception");
        throw;
    }
}

struct HasTableTestTransaction
{
    HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "CREATE TABLE test_transaction (id INT UNIQUE NOT NULL)");
    }
    ~HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "DROP TABLE test_transaction");
    }
};

}//namespace {anonymous}

BOOST_AUTO_TEST_CASE(test_transaction_from_dsn)
{
    LibPg::PgRwTransaction transaction{Test::get_dsn()};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_CASE(test_transaction_from_conn)
{
    LibPg::PgRwTransaction transaction{Test::get_db_conn()};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_CASE(test_transaction_from_transaction)
{
    LibPg::PgRwTransaction src_transaction{Test::get_db_conn()};
    LibPg::PgRwTransaction transaction{std::move(src_transaction)};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_CASE(test_transaction_isolation_levels)
{
    using IsolationLevel = LibPg::StrongString<struct IsolationLevelTag_>;
    using ReadOnlyAccess = LibPg::StrongString<struct ReadOnlyAccessTag_>;
    using Deferrable = LibPg::StrongString<struct DeferrableTag_>;
    static const auto session_default = []()
    {
        const auto result = exec(
                Test::get_db_conn(),
                LibPg::make_query() << "SELECT " << LibPg::item<IsolationLevel>() << "current_setting('default_transaction_isolation')"
                                                 << LibPg::item<Deferrable>() << "current_setting('default_transaction_deferrable')");
        BOOST_REQUIRE(result.size() == LibPg::RowIndex{1});
        return result.front();
    }();
    static const auto check_transaction = [](LibPg::PgRwTransaction transaction,
                                             const IsolationLevel& isolation_level,
                                             const Deferrable& deferrable)
    {
        static const auto query = LibPg::make_query() <<
                "SELECT " << LibPg::item<IsolationLevel>() << "current_setting('transaction_isolation')"
                          << LibPg::item<ReadOnlyAccess>() << "current_setting('transaction_read_only')"
                          << LibPg::item<Deferrable>() << "current_setting('transaction_deferrable')";
        const auto result = exec(transaction, query);
        BOOST_REQUIRE(result.size() == LibPg::RowIndex{1});
        BOOST_CHECK_EQUAL(result.front().get<IsolationLevel>(), isolation_level);
        BOOST_CHECK_EQUAL(result.front().get<ReadOnlyAccess>(), ReadOnlyAccess{"off"});
        BOOST_CHECK_EQUAL(result.front().get<Deferrable>(), deferrable);
        return commit(std::move(transaction));
    };
    auto conn0 = Test::get_db_conn();
    auto conn1 = check_transaction(LibPg::PgRwTransaction{std::move(conn0), LibPg::PgTransaction::session_default},
                                   session_default.get<IsolationLevel>(),
                                   session_default.get<Deferrable>());
    auto conn2 = check_transaction(LibPg::PgRwTransaction{std::move(conn1), LibPg::PgTransaction::read_committed},
                                   IsolationLevel{"read committed"},
                                   session_default.get<Deferrable>());
    auto conn3 = check_transaction(LibPg::PgRwTransaction{std::move(conn2), LibPg::PgTransaction::read_uncommitted},
                                   IsolationLevel{"read uncommitted"},
                                   session_default.get<Deferrable>());
    auto conn4 = check_transaction(LibPg::PgRwTransaction{std::move(conn3), LibPg::PgTransaction::repeatable_read},
                                   IsolationLevel{"repeatable read"},
                                   session_default.get<Deferrable>());
    auto conn5 = check_transaction(LibPg::PgRwTransaction{std::move(conn4), LibPg::PgTransaction::serializable},
                                   IsolationLevel{"serializable"},
                                   session_default.get<Deferrable>());
    auto conn6 = check_transaction(LibPg::PgRwTransaction{std::move(conn5), LibPg::PgTransaction::serializable_deferrable},
                                   IsolationLevel{"serializable"},
                                   Deferrable{"on"});
    auto conn7 = check_transaction(LibPg::PgRwTransaction{std::move(conn6), LibPg::PgTransaction::serializable_not_deferrable},
                                   IsolationLevel{"serializable"},
                                   Deferrable{"off"});
}

BOOST_FIXTURE_TEST_CASE(test_transaction_isolation, HasTableTestTransaction)
{
    const auto insert_two_ids = [](const std::array<int, 2>& ids)
    {
        LibPg::PgRwTransaction transaction{Test::get_dsn()};
        {
            const auto arg = LibPg::make_fixed_size_query_arguments(ids[0]);
            const LibPg::PgResultTuples dbres = exec(transaction, "INSERT INTO test_transaction(id) "
                                                                  "VALUES(" + arg.parameter<0>().as_integer() + ") "
                                                                  "RETURNING id",
                                                                  arg);
            BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
            BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{1});
            BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<int>(), ids[0]);
        }
        {
            const auto arg = LibPg::make_named_query_arguments(ids[1]);
            const LibPg::PgResultNoData cmdres = exec(transaction, "INSERT INTO test_transaction(id) "
                                                                   "VALUES(" + arg.parameter<int>().as_integer() + ")",
                                                                   arg);
            BOOST_CHECK_EQUAL(cmdres.get_number_of_rows_affected(), LibPg::RowIndex{1});
        }
        return commit(std::move(transaction));
    };
    LibPg::PgRwTransaction transaction_one{insert_two_ids({1, 2})};
    LibPg::PgRwTransaction transaction_two{insert_two_ids({3, 4})};
    static const auto insert_id = [](const LibPg::PgRwTransaction& transaction, const LibPg::FixedSizeQueryArguments<1>& arg)
    {
        const LibPg::PgResultNoData cmdres = exec(transaction, "INSERT INTO test_transaction(id) "
                                                               "VALUES(" + arg.parameter<0>().as_integer() + ")",
                                                               arg);
        BOOST_CHECK_EQUAL(cmdres.get_number_of_rows_affected(), LibPg::RowIndex{1});
    };
    static const auto visibility_required = [](const LibPg::PgConnection& conn, const LibPg::FixedSizeQueryArguments<1>& arg)
    {
        const LibPg::PgResultTuples dbres = exec(conn, "SELECT COUNT(*) FROM test_transaction "
                                                       "WHERE id=" + arg.parameter<0>().as_integer(),
                                                       arg);
        BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
        BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}].size(), LibPg::ColumnIndex{1});
        BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<int>(), 1);
    };
    static const auto invisibility_required = [](const LibPg::PgConnection& conn, const LibPg::FixedSizeQueryArguments<1>& arg)
    {
        const LibPg::PgResultTuples dbres = exec(conn, "SELECT id FROM test_transaction "
                                                       "WHERE id=" + arg.parameter<0>().as_integer(),
                                                       arg);
        BOOST_CHECK(dbres.empty());
        BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{1});
    };
    static const auto delete_id = [](const LibPg::PgRwTransaction& transaction, const LibPg::FixedSizeQueryArguments<1>& arg)
    {
        const LibPg::PgResultNoData cmdres = exec(transaction, "DELETE FROM test_transaction "
                                                               "WHERE id=" + arg.parameter<0>().as_integer(),
                                                               arg);
        BOOST_CHECK_EQUAL(cmdres.get_number_of_rows_affected(), LibPg::RowIndex{1});
    };

    static constexpr int value1 = 5;
    static const auto arg1 = LibPg::make_fixed_size_query_arguments(value1);
    static constexpr int value2 = 6;
    static const auto arg2 = LibPg::make_fixed_size_query_arguments(value2);
    invisibility_required(transaction_one, arg1);
    invisibility_required(transaction_one, arg2);
    invisibility_required(transaction_two, arg1);
    invisibility_required(transaction_two, arg2);

    insert_id(transaction_one, arg1);
    visibility_required(transaction_one, arg1);
    invisibility_required(transaction_one, arg2);
    invisibility_required(transaction_two, arg1);
    invisibility_required(transaction_two, arg2);

    insert_id(transaction_two, arg2);
    visibility_required(transaction_one, arg1);
    invisibility_required(transaction_one, arg2);
    invisibility_required(transaction_two, arg1);
    visibility_required(transaction_two, arg2);

    LibPg::PgConnection conn1 = commit(std::move(transaction_one));
    visibility_required(conn1, arg1);
    invisibility_required(conn1, arg2);
    visibility_required(transaction_two, arg1);
    visibility_required(transaction_two, arg2);

    LibPg::PgConnection conn2 = commit(std::move(transaction_two));
    visibility_required(conn1, arg1);
    visibility_required(conn1, arg2);
    visibility_required(conn2, arg1);
    visibility_required(conn2, arg2);

    {
        LibPg::PgRwTransaction transaction_one{std::move(conn1)};
        LibPg::PgRwTransaction transaction_two{std::move(conn2)};
        visibility_required(transaction_one, arg1);
        visibility_required(transaction_one, arg2);
        visibility_required(transaction_two, arg1);
        visibility_required(transaction_two, arg2);

        delete_id(transaction_one, arg1);
        invisibility_required(transaction_one, arg1);
        visibility_required(transaction_one, arg2);
        visibility_required(transaction_two, arg1);
        visibility_required(transaction_two, arg2);

        delete_id(transaction_two, arg2);
        invisibility_required(transaction_one, arg1);
        visibility_required(transaction_one, arg2);
        visibility_required(transaction_two, arg1);
        invisibility_required(transaction_two, arg2);

        const LibPg::PgConnection conn2 = rollback(std::move(transaction_two));
        invisibility_required(transaction_one, arg1);
        visibility_required(transaction_one, arg2);
        visibility_required(conn2, arg1);
        visibility_required(conn2, arg2);

        const LibPg::PgConnection conn1 = rollback(std::move(transaction_one));
        visibility_required(conn1, arg1);
        visibility_required(conn1, arg2);
        visibility_required(conn2, arg1);
        visibility_required(conn2, arg2);
    }
}

BOOST_AUTO_TEST_SUITE_END()//TestTransaction
