/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"

#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/prepared_transaction.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

#include <stdexcept>
#include <utility>

BOOST_AUTO_TEST_SUITE(TestPreparedTransaction)

namespace {

struct HasTableTestTransaction
{
    HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "CREATE TABLE test_transaction (id INT UNIQUE NOT NULL)");
    }
    ~HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "DROP TABLE test_transaction");
    }
};

constexpr bool dummy_exception_check(const std::exception&)
{
    return true;
}

void insert_id(const LibPg::PgConnection& conn, int id)
{
    const LibPg::PgResultNoData cmdres = exec(conn, "INSERT INTO test_transaction(id) "
                                                    "VALUES(" + LibPg::query_parameter_at_position(0).as_integer() + ")",
                                                    LibPg::make_fixed_size_query_arguments(id));
    BOOST_CHECK_EQUAL(cmdres.get_number_of_rows_affected(), LibPg::RowIndex{1});
}

void visibility_required(const LibPg::PgConnection& conn, int id)
{
    const LibPg::PgResultTuples dbres = exec(conn, "SELECT COUNT(*) FROM test_transaction "
                                                   "WHERE id=" + LibPg::query_parameter_at_position(0).as_integer(),
                                                   LibPg::make_fixed_size_query_arguments(id));
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}].size(), LibPg::ColumnIndex{1});
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<int>(), 1);
}

void invisibility_required(const LibPg::PgConnection& conn, int id)
{
    const LibPg::PgResultTuples dbres = exec(conn, "SELECT id FROM test_transaction "
                                                   "WHERE id=" + LibPg::query_parameter_at_position(0).as_integer(),
                                                   LibPg::make_fixed_size_query_arguments(id));
    BOOST_CHECK(dbres.empty());
    BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{1});
};

void to_connection(const LibPg::PgConnection&) { }

}//namespace {anonymous}

BOOST_FIXTURE_TEST_CASE(test_commit_prepared_transaction_1, HasTableTestTransaction)
{
    LibPg::PgRwTransaction transaction{Test::get_dsn()};
    invisibility_required(transaction, 1);
    insert_id(transaction, 1);
    visibility_required(transaction, 1);
    const auto transaction_id = LibPg::PreparedTransactionId{"prepared transaction test number one"};
    const auto conn1 = prepare_transaction(std::move(transaction), transaction_id);
    BOOST_CHECK_EXCEPTION(to_connection(transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
    const LibPg::PgConnection conn2 = Test::get_db_conn();
    invisibility_required(conn2, 1);
    commit_prepared_transaction(conn1, transaction_id);
    visibility_required(conn1, 1);
    visibility_required(conn2, 1);
}

BOOST_FIXTURE_TEST_CASE(test_commit_prepared_transaction_2, HasTableTestTransaction)
{
    const auto transaction_id = LibPg::PreparedTransactionId{"prepared transaction test number two"};
    {
        LibPg::PgRwTransaction transaction{Test::get_dsn()};
        invisibility_required(transaction, 1);
        insert_id(transaction, 1);
        visibility_required(transaction, 1);
        prepare_transaction(std::move(transaction), transaction_id);
        BOOST_CHECK_EXCEPTION(to_connection(transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
    }
    const LibPg::PgConnection conn1 = Test::get_db_conn();
    invisibility_required(conn1, 1);
    const LibPg::PgConnection conn2 = Test::get_db_conn();
    commit_prepared_transaction(conn2, transaction_id);
    visibility_required(conn1, 1);
    visibility_required(conn2, 1);
}

BOOST_FIXTURE_TEST_CASE(test_rollback_prepared_transaction_1, HasTableTestTransaction)
{
    LibPg::PgRwTransaction transaction{Test::get_dsn()};
    invisibility_required(transaction, 1);
    insert_id(transaction, 1);
    visibility_required(transaction, 1);
    const auto transaction_id = LibPg::PreparedTransactionId{"prepared transaction test number one"};
    const auto conn1 = prepare_transaction(std::move(transaction), transaction_id);
    BOOST_CHECK_EXCEPTION(to_connection(transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
    const LibPg::PgConnection conn2 = Test::get_db_conn();
    invisibility_required(conn2, 1);
    rollback_prepared_transaction(conn1, transaction_id);
    invisibility_required(conn1, 1);
    invisibility_required(conn2, 1);
}

BOOST_FIXTURE_TEST_CASE(test_rollback_prepared_transaction_2, HasTableTestTransaction)
{
    const auto transaction_id = LibPg::PreparedTransactionId{"prepared transaction test number two"};
    {
        LibPg::PgRwTransaction transaction{Test::get_dsn()};
        invisibility_required(transaction, 1);
        insert_id(transaction, 1);
        visibility_required(transaction, 1);
        prepare_transaction(std::move(transaction), transaction_id);
        BOOST_CHECK_EXCEPTION(to_connection(transaction), LibPg::NoConnectionAvailable, dummy_exception_check);
    }
    const LibPg::PgConnection conn1 = Test::get_db_conn();
    invisibility_required(conn1, 1);
    const LibPg::PgConnection conn2 = Test::get_db_conn();
    rollback_prepared_transaction(conn2, transaction_id);
    invisibility_required(conn1, 1);
    invisibility_required(conn2, 1);
}

BOOST_AUTO_TEST_SUITE_END()//TestPreparedTransaction
