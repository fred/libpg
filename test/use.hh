/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef USE_HH_E337BD66E26D5E929EE77C06D7776F2A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define USE_HH_E337BD66E26D5E929EE77C06D7776F2A

#include "libpg/pg_connection.hh"
#include "libpg/pg_result.hh"

#include <boost/test/unit_test.hpp>

#include <string>
#include <utility>

namespace Test {

class Use
{
public:
    Use(const LibPg::PgConnection& conn)
        : conn_{conn}
    { }
    class Argument
    {
    public:
        template <typename ParameterType>
        auto of_type()const
        {
            const std::string query = "SELECT " + args_.parameter<0>().template as<ParameterType>();
            const LibPg::PgResultTuples dbres = exec(conn_, query, args_);
            BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
            BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}].size(), LibPg::ColumnIndex{1});
            return dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}];
        }
    private:
        template <typename T>
        Argument(const Use& use, T&& argument)
            : conn_{use.conn_},
              args_{std::forward<T>(argument)}
        { }
        const LibPg::PgConnection& conn_;
        const LibPg::FixedSizeQueryArguments<1> args_;
        friend class Use;
    };
    template <typename ArgumentType>
    auto select(ArgumentType&& argument)const
    {
        return Argument{*this, std::forward<ArgumentType>(argument)};
    }
private:
    const LibPg::PgConnection& conn_;
};

}//namespace Test

#endif//USE_HH_E337BD66E26D5E929EE77C06D7776F2A
