/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SETUP_HH_8860C648B036A395937C8F0CBE62C97D//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define SETUP_HH_8860C648B036A395937C8F0CBE62C97D

#include "libpg/pg_connection.hh"

namespace Test {

const LibPg::Dsn& get_dsn();

LibPg::PgConnection get_db_conn();

}//namespace Test

#endif//SETUP_HH_8860C648B036A395937C8F0CBE62C97D
