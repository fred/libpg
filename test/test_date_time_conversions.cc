/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "setup.hh"
#include "use.hh"

#include "libpg/pg_connection.hh"
#include "libpg/pg_ro_transaction.hh"
#include "libpg/query.hh"
#include "libpg/time_zone.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

namespace {

template <typename T>
T unwrap(const char* str)
{
    return LibPg::from_nullable_string(LibPg::fake_const_ref<T>(), str);
}

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestDateTimeConversions)

BOOST_AUTO_TEST_CASE(test_date_time_conversions)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    const Test::Use using_conn{conn};
    const char time[]{"2020-01-02 13:10:20.123456999"};
    const std::chrono::system_clock::time_point chrono_time{unwrap<std::chrono::system_clock::time_point>(time)};
    const boost::posix_time::ptime boost_time{boost::posix_time::time_from_string(time)};
    const char date[]{"2020-01-02"};
    const boost::gregorian::date boost_date{boost::gregorian::from_simple_string(date)};
    const char duration[]{"1:23"};
    const boost::posix_time::time_duration boost_duration{boost::posix_time::duration_from_string(duration)};
    const std::chrono::nanoseconds chrono_duration{(1 * 60ull + 23) * 60 * 1000 * 1000 * 1000};
    const char duration_frac[]{"0:01:23.123456789"};
    const boost::posix_time::time_duration boost_duration_frac{boost::posix_time::duration_from_string(duration_frac)};
    const std::chrono::nanoseconds chrono_duration_frac{(1 * 60ull + 23) * 1000 * 1000 * 1000 + 123456789};

    BOOST_CHECK(using_conn.select(boost_date).of_type<LibPg::PsqlType::Date>().as<boost::gregorian::date>() == boost_date);
    BOOST_CHECK(using_conn.select(boost_time).of_type<LibPg::PsqlType::Timestamp>().as<std::chrono::system_clock::time_point>() == chrono_time);
    const auto chrono_time_exact = std::chrono::system_clock::from_time_t(1577970620) + std::chrono::nanoseconds(123456999);
    BOOST_CHECK(using_conn.select(chrono_time_exact).of_type<LibPg::PsqlType::Timestamp>().as<std::chrono::system_clock::time_point>() == chrono_time + std::chrono::microseconds{1});
    BOOST_CHECK(using_conn.select(unwrap<boost::gregorian::date>(date)).of_type<LibPg::PsqlType::Date>().as<boost::gregorian::date>() == boost_date);
    {
        const auto value = using_conn.select(unwrap<boost::posix_time::ptime>(time)).of_type<LibPg::PsqlType::TimestampWithoutTimeZone>();
        BOOST_CHECK(value.as<std::chrono::system_clock::time_point>() == chrono_time);
        BOOST_CHECK(value.as<boost::posix_time::ptime>() == boost_time);
    }
    {
        const auto value = using_conn.select(chrono_time).of_type<LibPg::PsqlType::Timestamp>();
        BOOST_CHECK(value.as<std::chrono::system_clock::time_point>() == chrono_time);
        BOOST_CHECK(value.as<boost::posix_time::ptime>() == boost_time);
    }
    {
        const auto value = using_conn.select(time).of_type<LibPg::PsqlType::Timestamp>();
        BOOST_CHECK(value.as<std::chrono::system_clock::time_point>() == chrono_time + std::chrono::microseconds{1});
        BOOST_CHECK(value.as<boost::posix_time::ptime>() == boost_time + boost::posix_time::microseconds{1});
    }
    {
        const auto value = using_conn.select(boost_duration).of_type<LibPg::PsqlType::Time>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), std::string{"0"} + duration + ":00");
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration);
    }
    {
        const auto value = using_conn.select(boost_duration).of_type<LibPg::PsqlType::TimeWithTimeZone>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), std::string{"0"} + duration + ":00+00");
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration);
    }
    {
        const auto value = using_conn.select(duration).of_type<LibPg::PsqlType::Text>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), duration);
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration);
    }
    {
        const auto value = using_conn.select(chrono_duration).of_type<LibPg::PsqlType::Time>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), std::string{"0"} + duration + ":00");
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration);
    }
    {
        const auto value = using_conn.select(boost_duration_frac).of_type<LibPg::PsqlType::Time>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), (std::string{"0"} + duration_frac).substr(0, 15));
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration_frac);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration_frac - std::chrono::nanoseconds(789));
    }
    {
        const auto value = using_conn.select(duration_frac).of_type<LibPg::PsqlType::Text>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), duration_frac);
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration_frac);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration_frac - std::chrono::nanoseconds(789));
    }
    {
        const auto value = using_conn.select(chrono_duration_frac).of_type<LibPg::PsqlType::Time>();
        BOOST_CHECK_EQUAL(value.as<std::string>(), (std::string{"0"} + duration_frac).substr(0, 15));
        BOOST_CHECK(value.as<boost::posix_time::time_duration>() == boost_duration_frac);
        BOOST_CHECK(value.as<std::chrono::nanoseconds>() == chrono_duration_frac - std::chrono::nanoseconds(789));
    }
}

extern const char* valid_time_zone;
extern const char* invalid_time_zone;

BOOST_AUTO_TEST_CASE(test_time_zone)
{
    const LibPg::PgConnection conn = Test::get_db_conn();
    const Test::Use using_conn{conn};
    BOOST_CHECK_EQUAL(using_conn.select(LibPg::TimeZone::utc()).of_type<LibPg::PsqlType::VarChar<>>().as<std::string>(), "UTC");
    BOOST_CHECK_EQUAL(using_conn.select(LibPg::make_time_zone<valid_time_zone>()).of_type<LibPg::PsqlType::Text>().as<std::string>(), valid_time_zone);
    {
        LibPg::PgRoTransaction transaction{Test::get_db_conn()};
        const LibPg::PgConnection& conn = transaction;
        BOOST_CHECK_EXCEPTION(make_time_zone(transaction, "Europe/Praha"), LibPg::PgException,
                              [](const LibPg::PgException& e) { return std::strcmp(e.what(), "invalid time zone") == 0; });
        BOOST_CHECK_NO_THROW(make_time_zone(conn, "Europe/Prague"));
        BOOST_CHECK_NO_THROW(make_time_zone(transaction, "Europe/Prague"));
        BOOST_CHECK_EXCEPTION(make_time_zone(conn, "Europe/Praha"), LibPg::PgException,
                              [](const LibPg::PgException& e) { return std::strcmp(e.what(), "time zone does not exist in pg_timezone_names") == 0; });

        const auto is_time_zone_valid = [&](const LibPg::TimeZone& time_zone)
        {
            static const auto query = LibPg::make_query() <<
                    "SELECT" << LibPg::item<int>() << "0 "
                    "FROM (SELECT '2019-10-27 00:00:00'::TIMESTAMP AT TIME ZONE " << LibPg::parameter<LibPg::TimeZone>().as_text() << ") AS c(t) "
                    "WHERE t=(t+'1DAY'::INTERVAL)";
            try
            {
                transaction.recoverable([&](const LibPg::PgRoTransaction& transaction) { exec(transaction, query, time_zone); });
                return true;
            }
            catch (...)
            {
                return false;
            }
        };
        BOOST_CHECK(is_time_zone_valid(LibPg::TimeZone::utc()));
        BOOST_CHECK(is_time_zone_valid(LibPg::make_time_zone<valid_time_zone>()));
        BOOST_CHECK(!is_time_zone_valid(LibPg::make_time_zone<invalid_time_zone>()));
    }
}

const char* valid_time_zone = "Antarctica/Vostok";
const char* invalid_time_zone = "Antarctica/Vosto";

BOOST_AUTO_TEST_SUITE_END()//TestDateTimeConversions
