/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"
#include "use.hh"

#include "libpg/pg_connection.hh"
#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/pg_ro_transaction.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"
#include "libpg/strong_types.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

#include <stdexcept>

// *************************************************************
// *    How To: Add Wrapper/Unwrapper For User Defined Type    *
// *************************************************************

namespace {

template <typename T, typename Tag>
using ComparableStreamable = LibStrong::TypeWithSkills<T, Tag, LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>;

enum class UserDefinedType
{
    first,
    second,
};

//signalize wrapper/unwrapper errors
struct WrapperError : std::exception { };
struct UnwrapperError : std::exception { };

// 1. in the same namespace define function wrap_into_psql_representation which converts value of
//    given type into its psql string representation
decltype(auto) wrap_into_psql_representation(UserDefinedType value)
{
    switch (value)
    {
        case UserDefinedType::first: return std::string{"first"};
        case UserDefinedType::second: return std::string{"second"};
    }
    struct UnexpectedValue : WrapperError
    {
        const char* what()const noexcept override { return "unexpected value of UserDefinedType"; }
    };
    throw UnexpectedValue{};
}

// 2. in the same namespace define function unwrap_from_psql_representation returning UserDefinedType and having
//    two parameters, the first is const UserDefinedType& and the second is a const char* with psql representation
//    of the UserDefinedType value
UserDefinedType unwrap_from_psql_representation(const UserDefinedType&, const char* src)
{
    if (src == nullptr)
    {
        struct MissingValue : UnwrapperError
        {
            const char* what()const noexcept override { return "value is NULL"; }
        };
        throw MissingValue{};
    }
    if (src == wrap_into_psql_representation(UserDefinedType::first))
    {
        return UserDefinedType::first;
    }
    if (src == wrap_into_psql_representation(UserDefinedType::second))
    {
        return UserDefinedType::second;
    }
    struct InvalidValue : UnwrapperError
    {
        const char* what()const noexcept override { return "unable to convert to Enum value"; }
    };
    throw InvalidValue{};
}

void test_exec(const LibPg::PgConnection& conn)
{
    const LibPg::PgResultTuples dbres = exec(conn, "SELECT 'foo'::TEXT");
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
    const auto query = LibPg::make_query() << "SELECT" << LibPg::item<std::string>() << LibPg::parameter<std::string>().as_text();
    BOOST_CHECK_EQUAL((exec(conn, query, std::string{"bar"}).size()), LibPg::RowIndex{1});
}

void test_exec_in_transaction(const LibPg::PgTransaction& transaction)
{
    const LibPg::PgResultTuples dbres = exec(transaction, "SELECT 'foo'::TEXT");
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{1});
    test_exec(transaction);//implicit conversion const PgTransaction& -> const PgConnection&
    BOOST_CHECK_EQUAL((exec(transaction,//implicit conversion const PgTransaction& -> const PgConnection&
                            LibPg::make_query() << "SELECT" << LibPg::item<std::string>() << LibPg::parameter<std::string>().as_text(),
                            std::string{"bar"}).size()), LibPg::RowIndex{1});
}

bool dummy_check(const std::exception&) { return true; }

decltype(auto) test_transaction(LibPg::PgRwTransaction first_transaction)
{
    test_exec_in_transaction(first_transaction);//by const PgTransaction&
    LibPg::PgRwTransaction second_transaction{commit(std::move(first_transaction))};//connection moved from the first to the second transaction
    test_exec_in_transaction(second_transaction);//by const PgTransaction&
    LibPg::PgRwTransaction third_transaction{rollback(std::move(second_transaction))};//connection moved from the second to the third transaction
    try
    {
        exec(third_transaction, "SELECT 'foo'::BAR");//broken query breaks transaction
        throw std::logic_error{"unreachable code was reached"};
    }
    catch (const LibPg::ExecFailure& e)
    {
        BOOST_CHECK_EQUAL(e.error_code(), LibPg::SqlState::undefined_object);
        LibPg::PgRwTransaction fourth_transaction{rollback(std::move(third_transaction))};//rollback repairs transaction and moves connection to the fourth transaction
        test_exec_in_transaction(fourth_transaction);//by const PgTransaction&
        LibPg::PgRwTransaction fifth_transaction{std::move(fourth_transaction)};//connection moved from the fourth to the fifth transaction
        BOOST_CHECK_EXCEPTION(test_exec_in_transaction(fourth_transaction), LibPg::NoConnectionAvailable, dummy_check);//the fourth transaction is not usable
        test_exec_in_transaction(fifth_transaction);//by const PgTransaction&
        LibPg::PgConnection conn = commit(std::move(fifth_transaction));//commit moves connection from the fifth transaction
        BOOST_CHECK_EXCEPTION(test_exec_in_transaction(fifth_transaction), LibPg::NoConnectionAvailable, dummy_check);//the fifth transaction is not usable
        return conn;
    }
    catch (const std::exception& e)
    {
        BOOST_TEST_MESSAGE("caught exception: " << e.what());
        throw;
    }
    catch (...)
    {
        BOOST_TEST_MESSAGE("caught unknown exception");
        throw;
    }
}

// not necessary; just for usage in boost unit test framework macro BOOST_CHECK_EQUAL etc.
std::ostream& operator<<(std::ostream& out, UserDefinedType value);

void expected(bool live_up_to_expectations)
{
    if (!live_up_to_expectations)
    {
        struct ExpectationViolation : std::exception
        {
            const char* what()const noexcept override { return "expectation violation"; }
        };
        throw ExpectationViolation{};
    }
}

struct Required
{
    explicit Required(bool requirement_satisfied)
    {
        expected(requirement_satisfied);
    }
};

std::ostream& operator<<(std::ostream& out, UserDefinedType value)
{
    return out << *LibPg::make_one_query_argument_from_nullable(value);
}

void report_error(const LibPg::ExecFailure& e)
{
    if (e.error_code() == LibPg::SqlState::integrity_constraint_violation_class)
    {
        BOOST_TEST_MESSAGE("integrity_constraint_violation_class " << e.error_code() << " exception caught: " << e.what());
    }
    else if (e.error_code() == LibPg::SqlState::foreign_key_violation)
    {
        BOOST_TEST_MESSAGE("foreign_key_violation exception caught: " << e.what());
    }
    else if (e.error_code() == LibPg::SqlState::protocol_violation)
    {
        BOOST_TEST_MESSAGE("protocol_violation exception caught: " << e.what());
    }
    else
    {
        BOOST_TEST_MESSAGE("unexpected exception caught: " << e.what() << "; error code = \"" << e.error_code() << "\"");
    }
}

struct HasConnection
{
    HasConnection() : conn{Test::get_db_conn()} { }
    const LibPg::PgConnection conn;
};

struct HasTable : HasConnection
{
    HasTable(std::string name)
        : HasConnection{},
          table_name{std::move(name)}
    {
        LibPg::PgResultNoData{exec(conn, "DROP TABLE IF EXISTS " + table_name)};
        LibPg::PgResultNoData{exec(conn, "CREATE TABLE " + table_name + " (id INT UNIQUE NOT NULL)")};
    }
    ~HasTable()
    {
        try
        {
            const LibPg::PgResultNoData dbres = exec(conn, "DROP TABLE " + table_name);
        }
        catch (...)
        {
        }
    }
    const std::string table_name;
};

struct TableManagement : HasTable
{
    TableManagement(std::string name) : HasTable{std::move(name)} { }
    template <typename T>
    auto insert_id(T&& id)const
    {
        using OneArgument = LibPg::FixedSizeQueryArguments<1>;
        const LibPg::PgResultNoData cmdres = // expected "no data" result
                exec(conn,
                     "INSERT INTO " + table_name + "(id) VALUES(" + OneArgument::parameter<0>().as_integer() + ")",
                     OneArgument{std::forward<T>(id)});
        auto result = cmdres.get_number_of_rows_affected();
        BOOST_CHECK_EQUAL(result, LibPg::RowIndex{1});
        return result;
    }
};

//demonstrates error code usage
template <typename ExpectedErrorCode>
auto has_error_code(ExpectedErrorCode)
{
    return [](const LibPg::ExecFailure& e)
        {
            return e.error_code() == ExpectedErrorCode{};//compare runtime error code with compile time constant
        };
}

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestHowTo)

BOOST_AUTO_TEST_CASE(show_how_to_add_wrapper_unwrapper_for_user_defined_type)
{
    // obtain connection (see Test::get_db_conn())
    const LibPg::PgConnection conn = Test::get_db_conn();
    try
    {
        // for usage in query with just three parameters
        using ThreeArgs = LibPg::FixedSizeQueryArguments<3>;
        // put query together with parameters ...
        static const auto query =
                "SELECT " + ThreeArgs::parameter<0>().as_text() + "," +                      // $1::TEXT               - parameter type specified by method name
                            ThreeArgs::parameter<1>().as_varchar<10>() + "," +               // $2::VARCHAR(10)        - parameter type specified by method name and template parameter
                            ThreeArgs::parameter<2>().as<LibPg::PsqlType::Integer>() + "," + // $3::INTEGER            - parameter type specified by type
                            ThreeArgs::parameter<0>().as_char<2>() + "," +                   // $1::CHAR(2) = "fi"
                            ThreeArgs::parameter<0>().as_type("CHAR(6)");                    // $1::CHAR(6) = "first " - parameter type specified by string
        // ... prepare arguments ...
        static const auto args = ThreeArgs{UserDefinedType::first, //wrapper in use
                                           UserDefinedType::second,//wrapper in use
                                           42};
        // ... and execute query with given arguments
        const LibPg::PgResultTuples dbres = exec(conn, query, args);// tuples expected as a result
        BOOST_REQUIRE(dbres.size() == LibPg::RowIndex{1});                    //check the number of rows
        BOOST_REQUIRE(dbres.get_number_of_columns() == LibPg::ColumnIndex{5});//and columns
        const auto row = dbres[LibPg::RowIndex{0}].as<std::tuple<UserDefinedType, UserDefinedType, int, std::string, std::string>>();//unwrapper in use
        BOOST_CHECK_EQUAL(std::get<0>(row), UserDefinedType::first);
        BOOST_CHECK_EQUAL(std::get<1>(row), UserDefinedType::second);
        BOOST_CHECK_EQUAL(std::get<2>(row), 42);
        BOOST_CHECK_EQUAL(std::get<3>(row), "fi");
        BOOST_CHECK_EQUAL(std::get<4>(row), "first ");
        BOOST_CHECK_EXCEPTION(LibPg::make_fixed_size_query_arguments(static_cast<UserDefinedType>(42)), WrapperError, dummy_check);//42 -> UserDefinedType
        BOOST_CHECK_EXCEPTION(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{2}].as<UserDefinedType>(), UnwrapperError, dummy_check);//"42" -> UserDefinedType
        BOOST_CHECK_EXCEPTION(dbres[LibPg::RowIndex{1}], LibPg::PgException, dummy_check);//row index 1 is out of range
        const auto columns = dbres[LibPg::RowIndex{0}];
        BOOST_CHECK_EXCEPTION(columns[LibPg::ColumnIndex{5}], LibPg::PgException, dummy_check);//column index 5 is out of range
    }
    catch (const LibPg::ExecFailure& e)
    {
        BOOST_TEST_MESSAGE("no exception expected");
        report_error(e);
        throw;
    }
    catch (...)
    {
        BOOST_TEST_MESSAGE("no exception expected");
        throw;
    }
}

// *******************************
// *    How To: Handle Result    *
// *******************************

struct HandleResultTableManagement : TableManagement
{
    HandleResultTableManagement() : TableManagement{"test_handle_result"} { }
};

BOOST_FIXTURE_TEST_CASE(show_how_to_handle_result, HandleResultTableManagement)
{
    try
    {
        this->insert_id(1);
        this->insert_id(2);
        this->insert_id(3);
        using Offset = LibStrong::Type<LibPg::RowIndex, struct OffsetTag_, LibStrong::Skill::Countable>;
        using Limit = LibStrong::Type<LibPg::RowIndex, struct OffsetLimit_, LibStrong::Skill::Countable>;
        // for usage in query with just two parameters - Offset and Limit
        using Args = LibPg::NamedQueryArguments<Offset, Limit>;
        // put query together with parameters and execute it with given arguments
        static const auto query =
            "SELECT id,"
                   "id+10,"
                   "id<2,"
                   "CASE WHEN id<2 THEN id-1 ELSE NULL END "
            "FROM " + table_name + " "
            "WHERE 0<id "
            "ORDER BY id "
            "OFFSET " + Args::parameter<Offset>().as_integer() + " "
            "LIMIT " + Args::parameter<Limit>().as_integer();
        const auto get_info = [&](const Args& args)
        {
            LibPg::PgResultTuples dbres = exec(conn, query, args);
            BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{4});
            return dbres;
        };
        static const auto check_column_0 = [](int id, int value)
        {
            BOOST_CHECK_EQUAL(value, id);
        };
        static const auto check_column_1 = [](int id, int value)
        {
            BOOST_CHECK_EQUAL(value, id + 10);
        };
        static const auto check_column_2 = [](int id, bool value)
        {
            BOOST_CHECK_EQUAL(value, id < 2);
        };
        static const auto check_column_3 = [](int id, const boost::optional<int>& value)
        {
            if (id < 2)
            {
                BOOST_CHECK_EQUAL(*value, id - 1);
            }
            else
            {
                BOOST_CHECK(value == boost::none);
            }
        };
        static const auto check_columns = [](int id, int value0, int value1, bool value2, const boost::optional<int>& value3)
        {
            check_column_0(id, value0);
            check_column_1(id, value1);
            check_column_2(id, value2);
            check_column_3(id, value3);
        };
        static const auto check_result = [](LibPg::PgResultTuples dbres, Offset offset, Limit limit)
        {
            BOOST_CHECK_LE(dbres.size(), *limit);
            BOOST_CHECK_EQUAL(dbres.size(), std::min(*limit, LibPg::RowIndex{3} - *offset));
            // 1. iterate by using RowIndex
            for (LibPg::RowIndex row_index{0}; row_index < dbres.size(); ++row_index)
            {
                const auto id = *(row_index + LibPg::RowIndex{1} + *offset);
                // 1.1 iterate by using ColumnIndex
                for (LibPg::ColumnIndex col_index{0}; col_index < dbres.get_number_of_columns(); ++col_index)
                {
                    //each column must be individually converted to its C++ type representation
                    switch (*col_index)
                    {
                        case 0:
                        {
                            // use Value::as<Type>()
                            const auto value = dbres[row_index][col_index].as<int>();
                            check_column_0(id, value);
                            break;
                        }
                        case 1:
                        {
                            int value;
                            // use Value::into(variable)
                            dbres[row_index][col_index].into(value);
                            check_column_1(id, value);
                            break;
                        }
                        case 2:
                        {
                            // use value_cast<Type>(Value)
                            const auto value = LibPg::value_cast<bool>(dbres[row_index][col_index]);
                            check_column_2(id, value);
                            break;
                        }
                        case 3:
                        {
                            const auto value = dbres[row_index][col_index].as<boost::optional<int>>();
                            check_column_3(id, value);
                            break;
                        }
                    }
                }
                // 1.2 iterate by using columns iterator
                {
                    const auto row = dbres[row_index];
                    LibPg::ColumnIndex col_index{0};
                    //each column must be individually converted to its C++ type representation
                    for (const auto dbvalue : row)
                    {
                        switch (*col_index)
                        {
                            case 0:
                            {
                                const auto value = dbvalue.as<int>();
                                check_column_0(id, value);
                                break;
                            }
                            case 1:
                            {
                                const auto value = dbvalue.as<int>();
                                check_column_1(id, value);
                                break;
                            }
                            case 2:
                            {
                                const auto value = dbvalue.as<bool>();
                                check_column_2(id, value);
                                break;
                            }
                            case 3:
                            {
                                const auto value = dbvalue.as<boost::optional<int>>();
                                check_column_3(id, value);
                                break;
                            }
                        }
                        ++col_index;
                    }
                }
                // 1.3 direct access (dbres[][])
                {
                    check_columns(id,
                                  dbres[row_index][LibPg::ColumnIndex{0}].as<int>(),
                                  dbres[row_index][LibPg::ColumnIndex{1}].as<int>(),
                                  dbres[row_index][LibPg::ColumnIndex{2}].as<bool>(),
                                  dbres[row_index][LibPg::ColumnIndex{3}].as<boost::optional<int>>());
                }
                // 1.4 via a tuple conversion
                {
                    //all columns are converted at once into their C++ types representations stored in std::tuple
                    const auto columns = dbres[row_index].as<std::tuple<int, int, bool, boost::optional<int>>>();
                    check_columns(id,
                                  std::get<0>(columns),
                                  std::get<1>(columns),
                                  std::get<2>(columns),
                                  std::get<3>(columns));
                }
                // 1.5 via NamedColumns
                {
                    //tag the type by the name
                    using Value0 = LibStrong::Type<int, struct Value0Tag_>;
                    using Value1 = ComparableStreamable<int, struct Value1Tag_>;
                    using Value2 = ComparableStreamable<bool, struct Value2Tag_>;
                    using Value3 = ComparableStreamable<boost::optional<int>, struct Value3Tag_>;
                    using Columns = LibPg::NamedColumns<Value0, Value1, Value2, Value3>;

                    const auto columns = dbres[row_index].as<Columns>();
                    //named types use as an index
                    check_columns(id,
                                  *columns.get<Value0>(),
                                  *columns.get<Value1>(),
                                  *columns.get<Value2>(),
                                  *columns.get<Value3>());
                }
            }
            // 2. iterate by using row iterator
            int row_idx = 0;
            for (const auto row : dbres)
            {
                const auto id = row_idx + 1 + **offset;
                // 2.1 iterate by using ColumnIndex
                for (LibPg::ColumnIndex col_index{0}; col_index < dbres.get_number_of_columns(); ++col_index)
                {
                    switch (*col_index)
                    {
                    case 0:
                    {
                        const auto value = row[col_index].as<int>();
                        check_column_0(id, value);
                        break;
                    }
                    case 1:
                    {
                        const auto value = row[col_index].as<int>();
                        check_column_1(id, value);
                        break;
                    }
                    case 2:
                    {
                        const auto value = row[col_index].as<bool>();
                        check_column_2(id, value);
                        break;
                    }
                    case 3:
                    {
                        const auto value = row[col_index].as<boost::optional<int>>();
                        check_column_3(id, value);
                        break;
                    }
                    }
                }
                // 2.2 iterate by using columns iterator
                {
                    LibPg::ColumnIndex col_index{0};
                    for (const auto dbvalue : row)
                    {
                        switch (*col_index)
                        {
                        case 0:
                        {
                            const auto value = dbvalue.as<int>();
                            check_column_0(id, value);
                            break;
                        }
                        case 1:
                        {
                            const auto value = dbvalue.as<int>();
                            check_column_1(id, value);
                            break;
                        }
                        case 2:
                        {
                            const auto value = dbvalue.as<bool>();
                            check_column_2(id, value);
                            break;
                        }
                        case 3:
                        {
                            const auto value = dbvalue.as<boost::optional<int>>();
                            check_column_3(id, value);
                            break;
                        }
                        }
                        ++col_index;
                    }
                }
                // 2.3 direct access
                {
                    check_columns(id,
                                  row[LibPg::ColumnIndex{0}].as<int>(),
                                  row[LibPg::ColumnIndex{1}].as<int>(),
                                  row[LibPg::ColumnIndex{2}].as<bool>(),
                                  row[LibPg::ColumnIndex{3}].as<boost::optional<int>>());
                }
                // 2.4 via a tuple conversion
                {
                    const auto columns = row.as<std::tuple<int, int, bool, boost::optional<int>>>();
                    check_columns(id,
                                  std::get<0>(columns),
                                  std::get<1>(columns),
                                  std::get<2>(columns),
                                  std::get<3>(columns));
                }
                // 2.5 via NamedColumns
                {
                    using Value0 = ComparableStreamable<int, struct Value0Tag_>;
                    using Value1 = ComparableStreamable<int, struct Value1Tag_>;
                    using Value2 = ComparableStreamable<bool, struct Value2Tag_>;
                    using Value3 = ComparableStreamable<boost::optional<int>, struct Value3Tag_>;
                    using Columns = LibPg::NamedColumns<Value0, Value1, Value2, Value3>;

                    const auto columns = row.as<Columns>();
                    check_columns(id,
                                  *columns.get<Value0>(),
                                  *columns.get<Value1>(),
                                  *columns.get<Value2>(),
                                  *columns.get<Value3>());
                }
                ++row_idx;
            }
        };
        const auto exec_and_check = [get_info](Offset offset, Limit limit)
        {
            check_result(get_info(Args{offset, limit}), offset, limit);
            check_result(get_info(Args{limit, offset}), offset, limit);
            check_result(get_info(LibPg::make_named_query_arguments(offset, limit)), offset, limit);
            check_result(get_info(LibPg::make_named_query_arguments(limit, offset)), offset, limit);
        };
        exec_and_check(Offset{LibPg::RowIndex{0}}, Limit{LibPg::RowIndex{10}});
        exec_and_check(Offset{LibPg::RowIndex{1}}, Limit{LibPg::RowIndex{3}});
        exec_and_check(Offset{LibPg::RowIndex{1}}, Limit{LibPg::RowIndex{1}});
        exec_and_check(Offset{LibPg::RowIndex{2}}, Limit{LibPg::RowIndex{10}});
    }
    catch (const LibPg::ExecFailure& e)
    {
        BOOST_TEST_MESSAGE("no exception expected");
        report_error(e);
        throw;
    }
    catch (...)
    {
        BOOST_TEST_MESSAGE("no exception expected");
        throw;
    }
}

// ***************************
// *    How To: Use Query    *
// ***************************

struct UseQueryTableManagement : HasConnection
{
    UseQueryTableManagement()
        : HasConnection{},
          table_contact{"test_use_query_contact"},
          table_contact_address{"test_use_query_contact_address"}
    {
        LibPg::PgResultNoData{exec(conn, "DROP TABLE IF EXISTS " + table_contact_address)};
        LibPg::PgResultNoData{exec(conn, "DROP TABLE IF EXISTS " + table_contact)};
        exec(conn, LibPg::make_query("CREATE TABLE ") << table_contact << " "
                                         "(id SERIAL PRIMARY KEY,"
                                          "name TEXT)");
        exec(conn, LibPg::make_query("CREATE TABLE ") << table_contact_address << " "
                                         "(id SERIAL PRIMARY KEY,"
                                          "contact_id INT REFERENCES " << table_contact << "(id),"
                                          "street VARCHAR(100))");
    }
    ~UseQueryTableManagement()
    {
        try
        {
            LibPg::PgResultNoData{exec(conn, "DROP TABLE IF EXISTS " + table_contact_address)};
        }
        catch (...) { }
        try
        {
            LibPg::PgResultNoData{exec(conn, "DROP TABLE IF EXISTS " + table_contact)};
        }
        catch (...) { }
    }
    const std::string table_contact;
    const std::string table_contact_address;
    using ContactId = LibStrong::ArithmeticSequence<int, struct ContactIdTag_>;
    using ContactName = LibPg::StrongString<struct ContactNameTag_>;
    using AddressId = LibStrong::ArithmeticSequence<int, struct AddressIdTag_>;
    using Street = LibPg::StrongString<struct StreetTag_>;
};

BOOST_FIXTURE_TEST_CASE(show_how_to_use_query, UseQueryTableManagement)
{
    // 1. create SQL command which does not return tuples
    {
        const auto insert_given_contact = LibPg::make_query("INSERT INTO ") << table_contact << "(id,name) "
                                                            "VALUES(" << LibPg::parameter<ContactId>().as_integer() << ",NULL)";
        const auto cmdres = exec(conn, insert_given_contact, ContactId{0});
        static_assert(std::is_same<std::decay_t<decltype(cmdres)>, LibPg::PgResultNoData>::value, "result must be of LibPg::PgResultNoData type");
    }
    // 2. create SQL command returning tuples
    {
        const auto insert_new_contact = LibPg::make_query("INSERT INTO ") << table_contact << "(name) "
                                                          "VALUES(" << LibPg::parameter<ContactName>().as_text() << ") "
                                                          "RETURNING" << LibPg::item<ContactId>() << "id";
        {
            const auto cmdres = exec(conn, insert_new_contact, ContactName{"James"});
            static_assert(std::is_same<std::decay_t<decltype(cmdres)>, LibPg::View<std::tuple<ContactId>>>::value,
                          "result must be of LibPg::View<LibPg::NamedColumns<ContactId>> type");
            BOOST_CHECK(!cmdres.empty());
            BOOST_CHECK_EQUAL(cmdres.size(), LibPg::RowIndex{1});
            BOOST_CHECK_EQUAL(cmdres.front().get<ContactId>(), ContactId{1});
        }
        const auto add_contact_address = LibPg::make_query("INSERT INTO ") << table_contact_address << "(contact_id,street) "
                                                           "VALUES(" << LibPg::parameter<ContactId>().as_integer() << ","
                                                                     << LibPg::parameter<Street>().as_text() << ") "
                                                           "RETURNING" << LibPg::item<AddressId>() << "id";
        {
            const auto cmdres = exec(conn, add_contact_address, {ContactId{1}, Street{"Strangeways 007"}});
            static_assert(std::is_same<std::decay_t<decltype(cmdres)>, LibPg::View<std::tuple<AddressId>>>::value,
                          "result must be of LibPg::View<LibPg::NamedColumns<AddressId>> type");
            BOOST_CHECK(!cmdres.empty());
            BOOST_CHECK_EQUAL(cmdres.size(), LibPg::RowIndex{1});
            BOOST_CHECK_EQUAL(cmdres.front().get<AddressId>(), AddressId{1});
        }
    }
    // 3. create SQL query returning tuples
    {
        const auto contact_info = LibPg::make_query() <<
                "SELECT" << LibPg::item<ContactId>() << "c.id"
                         << LibPg::nullable_item<ContactName>() << "c.name"
                         << LibPg::nullable_item<AddressId>() << "a.id"
                         << LibPg::nullable_item<Street>() << "a.street " <<
                "FROM " << table_contact << " c "
                "LEFT JOIN " << table_contact_address << " a ON a.contact_id=c.id "
                "WHERE c.id=" << LibPg::parameter<ContactId>().as_integer();
        using Columns = std::tuple<ContactId, LibStrong::Optional<ContactName>, LibStrong::Optional<AddressId>, LibStrong::Optional<Street>>;
        using Row = LibPg::NamedColumns<ContactId, LibStrong::Optional<ContactName>, LibStrong::Optional<AddressId>, LibStrong::Optional<Street>>;
        {
            const auto contact = exec(conn, contact_info, ContactId{0});
            static_assert(std::is_same<std::decay_t<decltype(contact)>, LibPg::View<Columns>>::value, "contact must be of LibPg::View<Columns> type");
            BOOST_CHECK(!contact.empty());
            BOOST_CHECK_EQUAL(contact.size(), LibPg::RowIndex{1});
            const auto info = contact.front();
            static_assert(std::is_same<std::decay_t<decltype(info)>, Row>::value, "info must be of Columns type");
            BOOST_CHECK_EQUAL(info.get<ContactId>(), ContactId{0});
            BOOST_CHECK(info.is_null<ContactName>());
            BOOST_CHECK(info.is_null<AddressId>());
            BOOST_CHECK(info.is_null<Street>());
        }
        {
            const auto contact = exec(conn, contact_info, ContactId{1});
            static_assert(std::is_same<std::decay_t<decltype(contact)>, LibPg::View<Columns>>::value, "contact must be of LibPg::View<Columns> type");
            BOOST_CHECK(!contact.empty());
            BOOST_CHECK_EQUAL(contact.size(), LibPg::RowIndex{1});
            const auto info = contact.front();
            static_assert(std::is_same<std::decay_t<decltype(info)>, Row>::value, "info must be of Columns type");
            BOOST_CHECK_EQUAL(info.get<ContactId>(), ContactId{1});
            BOOST_CHECK(!info.is_null<ContactName>());
            BOOST_CHECK_EQUAL(info.get_nullable<ContactName>(), ContactName{"James"});
            BOOST_CHECK(!info.is_null<AddressId>());
            BOOST_CHECK_EQUAL(info.get_nullable<AddressId>(), AddressId{1});
            BOOST_CHECK(!info.is_null<Street>());
            BOOST_CHECK_EQUAL(info.get_nullable<Street>(), Street{"Strangeways 007"});
        }
    }
    // 4. iterate over tuples
    {
        const auto contact_list = LibPg::make_query("SELECT") << LibPg::item<ContactId>() << "c.id"
                                                              << LibPg::nullable_item<ContactName>() << "c.name"
                                                              << LibPg::nullable_item<AddressId>() << "a.id"
                                                              << LibPg::nullable_item<Street>() << "a.street " <<
                                                    "FROM " << table_contact << " c "
                                                    "LEFT JOIN " << table_contact_address << " a ON a.contact_id=c.id "
                                                    "ORDER BY 1";
        using Columns = std::tuple<ContactId, LibStrong::Optional<ContactName>, LibStrong::Optional<AddressId>, LibStrong::Optional<Street>>;
        using Row = LibPg::NamedColumns<ContactId, LibStrong::Optional<ContactName>, LibStrong::Optional<AddressId>, LibStrong::Optional<Street>>;
        const auto list = exec(conn, contact_list);
        static_assert(std::is_same<std::decay_t<decltype(list)>, LibPg::View<Columns>>::value, "list must be of LibPg::View<Columns> type");
        BOOST_CHECK(!list.empty());
        BOOST_CHECK_EQUAL(list.size(), LibPg::RowIndex{2});
        int cnt = 0;
        for (const auto info : list)
        {
            static_assert(std::is_same<std::decay_t<decltype(info)>, Row>::value, "info must be of Columns type");
            switch (cnt)
            {
                case 0:
                    {
                        BOOST_CHECK_EQUAL(info.get<ContactId>(), ContactId{0});
                        BOOST_CHECK(info.is_null<ContactName>());
                        BOOST_CHECK(info.is_null<AddressId>());
                        BOOST_CHECK(info.is_null<Street>());
                        break;
                    }
                case 1:
                    {
                        BOOST_CHECK_EQUAL(info.get<ContactId>(), ContactId{1});
                        BOOST_CHECK(!info.is_null<ContactName>());
                        BOOST_CHECK_EQUAL(info.get_nullable<ContactName>(), ContactName{"James"});
                        BOOST_CHECK(!info.is_null<AddressId>());
                        BOOST_CHECK_EQUAL(info.get_nullable<AddressId>(), AddressId{1});
                        BOOST_CHECK(!info.is_null<Street>());
                        BOOST_CHECK_EQUAL(info.get_nullable<Street>(), Street{"Strangeways 007"});
                    }
            }
            ++cnt;
        }
        BOOST_CHECK_EQUAL(cnt, 2);
    }
}

// *********************************
// *    How To: Use Error Codes    *
// *********************************

struct ErrorCodeTableManagement : TableManagement
{
    ErrorCodeTableManagement() : TableManagement{"test_error_code"} { }
};

BOOST_FIXTURE_TEST_CASE(show_how_to_use_error_code, ErrorCodeTableManagement)
{
    try
    {
        using TwoArgs = LibPg::FixedSizeQueryArguments<2>;
        // put query together with parameters and execute it with given arguments
        static const auto two_params_query =
                "SELECT " + TwoArgs::parameter<0>().as_integer() + "," +
                            TwoArgs::parameter<1>().as_integer();
        static const auto one_arg = LibPg::make_fixed_size_query_arguments(42);
        BOOST_CHECK_EXCEPTION((exec(conn, two_params_query, one_arg)),                                 //query has two parameters but receives just one argument
                              LibPg::ExecFailure, has_error_code(LibPg::SqlState::protocol_violation));//it causes protocol_violation
        BOOST_CHECK_EXCEPTION(this->insert_id(LibPg::null_value),                                      //insert NULL into NOT NULL column
                              LibPg::ExecFailure, has_error_code(LibPg::SqlState::not_null_violation));//it causes not_null_violation
        const auto insert_one = [&]()
        {
            BOOST_CHECK_EQUAL(this->insert_id(1), LibPg::RowIndex{1});
        };
        insert_one();
        BOOST_CHECK_EXCEPTION(insert_one(),                                                          //insert the same id for the second time into UNIQUE column id
                              LibPg::ExecFailure, has_error_code(LibPg::SqlState::unique_violation));//it causes unique_violation
    }
    catch (const LibPg::ExecFailure& e)
    {
        BOOST_TEST_MESSAGE("no exception expected");
        report_error(e);
        throw;
    }
    catch (...)
    {
        BOOST_TEST_MESSAGE("no exception expected");
        throw;
    }
}

// *********************************
// *    How To: Use Transaction    *
// *********************************

BOOST_AUTO_TEST_CASE(show_how_to_create_transaction_from_dsn)
{
    LibPg::PgRwTransaction transaction{Test::get_dsn()};
    test_transaction(std::move(transaction));
    test_transaction(LibPg::PgRwTransaction{Test::get_dsn()});
}

BOOST_AUTO_TEST_CASE(show_how_to_create_transaction_from_conn)
{
    LibPg::PgRwTransaction transaction{Test::get_db_conn()};
    test_transaction(LibPg::PgRwTransaction{test_transaction(std::move(transaction))});
    test_transaction(LibPg::PgRwTransaction{Test::get_db_conn()});
}

BOOST_AUTO_TEST_CASE(show_how_to_create_transaction_from_another_transaction)
{
    LibPg::PgRwTransaction src_transaction{Test::get_db_conn()};
    LibPg::PgRwTransaction transaction{std::move(src_transaction)};
    test_transaction(std::move(transaction));
}

BOOST_AUTO_TEST_SUITE_END()//TestHowTo
