/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup.hh"

#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/strong_types.hh"
#include "libpg/sql_state/code.hh"

#include <boost/test/unit_test.hpp>

#include <set>
#include <stdexcept>
#include <utility>

BOOST_AUTO_TEST_SUITE(TestSavepoint)

namespace {

struct HasTableTestTransaction
{
    HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "CREATE TABLE test_transaction (id INT UNIQUE NOT NULL)");
    }
    ~HasTableTestTransaction()
    {
        const LibPg::PgConnection conn = Test::get_db_conn();
        const LibPg::PgResultNoData dbres = exec(conn, "DROP TABLE test_transaction");
    }
};

template <typename T>
void insert_id(const LibPg::PgRwTransaction& transaction, T&& id)
{
    const LibPg::PgResultNoData cmdres = exec(transaction,
            "INSERT INTO test_transaction(id) "
            "VALUES(" + LibPg::FixedSizeQueryArguments<1>::parameter<0>().as_integer() + ")",
            LibPg::make_fixed_size_query_arguments(std::forward<T>(id)));
    BOOST_CHECK_EQUAL(cmdres.get_number_of_rows_affected(), LibPg::RowIndex{1});
}

void delete_id(const LibPg::PgRwTransaction& transaction, int id)
{
    const LibPg::PgResultTuples cmdres = exec(transaction,
            "DELETE FROM test_transaction "
            "WHERE id=" + LibPg::FixedSizeQueryArguments<1>::parameter<0>().as_integer() + " "
            "RETURNING id",
            LibPg::make_fixed_size_query_arguments(id));
    BOOST_CHECK_EQUAL(cmdres.size(), LibPg::RowIndex{1});
}

constexpr bool dummy_exception_check(const std::exception&)
{
    return true;
}

}//namespace {anonymous}

BOOST_FIXTURE_TEST_CASE(test_recoverable_simple, HasTableTestTransaction)
{
    LibPg::PgRwTransaction transaction{Test::get_dsn()};
    BOOST_CHECK_EQUAL(LibPg::PgResultTuples{exec(transaction, "SELECT * FROM test_transaction LIMIT 1")}.size(), LibPg::RowIndex{0});

    for (const int id : {1, 2, 3, 4, 1, 5})
    {
        insert_id(transaction, id + 10);
        try
        {
            const auto action = [id](const LibPg::PgRwTransaction& transaction)
            {
                insert_id(transaction, id);
                delete_id(transaction, id + 10);
            };
            transaction.recoverable(action);
        }
        catch (const LibPg::ExecFailure& e)
        {
            if (e.error_code() != LibPg::SqlState::unique_violation)
            {
                throw;
            }
        }
    }

    const LibPg::PgConnection conn = commit(std::move(transaction));
    const LibPg::PgResultTuples dbres = exec(conn, "SELECT * FROM test_transaction ORDER BY 1");
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{6});
    BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{1});
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<int>(), 1);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{1}][LibPg::ColumnIndex{0}].as<int>(), 2);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{2}][LibPg::ColumnIndex{0}].as<int>(), 3);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{3}][LibPg::ColumnIndex{0}].as<int>(), 4);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{4}][LibPg::ColumnIndex{0}].as<int>(), 5);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{5}][LibPg::ColumnIndex{0}].as<int>(), 11);
}

BOOST_FIXTURE_TEST_CASE(test_recoverable_complex, HasTableTestTransaction)
{
    LibPg::PgRwTransaction transaction{Test::get_dsn()};
    BOOST_CHECK_EQUAL(LibPg::PgResultTuples{exec(transaction, "SELECT * FROM test_transaction LIMIT 1")}.size(), LibPg::RowIndex{0});

    using Pass = LibPg::StrongCountableStreamable<int, struct PassTag_>;
    Pass pass_cnt{0};
    std::set<Pass> primary_unique_violation_occurrences;
    std::set<Pass> secondary_unique_violation_occurrences;
    std::set<Pass> not_null_violation_occurrences;
    std::set<Pass> invalid_argument_occurrences;
    const auto record_occurrence = [&](std::set<Pass>& collector) { collector.insert(pass_cnt); };

    for (const int id : {1, 2, 1, 0, -1, 3})
    {
        try
        {
            enum class Result
            {
                no_problem,
                unique_violation,
            };
            const auto action = [&](const LibPg::PgRwTransaction& transaction)
            {
                if (0 <= id)
                {
                    insert_id(transaction, id);
                }
                try
                {
                    return transaction.recoverable([id](const LibPg::PgRwTransaction& transaction)
                                                   {
                                                       id != 0 ? insert_id(transaction, id)                //unique_violation
                                                               : insert_id(transaction, LibPg::null_value);//not_null_violation
                                                       BOOST_CHECK_EQUAL(id, -1);
                                                       if (id < 0)
                                                       {
                                                           throw std::invalid_argument{"negative id is not allowed"};
                                                       }
                                                       BOOST_CHECK(false);
                                                       return Result::no_problem;
                                                   });
                }
                catch (const LibPg::ExecFailure& e)
                {
                    if (e.error_code() == LibPg::SqlState::unique_violation)
                    {
                        record_occurrence(secondary_unique_violation_occurrences);
                        return Result::unique_violation;
                    }
                    throw;
                }
            };
            switch (transaction.recoverable(action))
            {
                case Result::no_problem :
                    BOOST_CHECK(false);
                    break;
                case Result::unique_violation :
                    BOOST_CHECK_EQUAL((std::set<Pass>{Pass{0}, Pass{1}, Pass{5}}).count(pass_cnt), 1);
                    break;
            }
        }
        catch (const LibPg::ExecFailure& e)
        {
            if (e.error_code() == LibPg::SqlState::not_null_violation)
            {
                BOOST_CHECK_EQUAL(pass_cnt, Pass{3});
                record_occurrence(not_null_violation_occurrences);
            }
            else if (e.error_code() == LibPg::SqlState::unique_violation)
            {
                BOOST_CHECK_EQUAL(pass_cnt, Pass{2});
                record_occurrence(primary_unique_violation_occurrences);
            }
            else
            {
                throw;
            }
        }
        catch (const std::invalid_argument&)
        {
            BOOST_CHECK_EQUAL(pass_cnt, Pass{4});
            record_occurrence(invalid_argument_occurrences);
        }
        ++pass_cnt;
    }
    BOOST_CHECK_EQUAL(pass_cnt, Pass{6});
    BOOST_CHECK(primary_unique_violation_occurrences == std::set<Pass>{Pass{2}});
    BOOST_CHECK(secondary_unique_violation_occurrences == (std::set<Pass>{Pass{0}, Pass{1}, Pass{5}}));
    BOOST_CHECK(not_null_violation_occurrences == std::set<Pass>{Pass{3}});
    BOOST_CHECK(invalid_argument_occurrences == std::set<Pass>{Pass{4}});

    const LibPg::PgConnection conn = commit(std::move(transaction));
    const LibPg::PgResultTuples dbres = exec(conn, "SELECT * FROM test_transaction ORDER BY 1");
    BOOST_CHECK_EQUAL(dbres.size(), LibPg::RowIndex{4});
    BOOST_CHECK_EQUAL(dbres.get_number_of_columns(), LibPg::ColumnIndex{1});
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<int>(), -1);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{1}][LibPg::ColumnIndex{0}].as<int>(), 1);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{2}][LibPg::ColumnIndex{0}].as<int>(), 2);
    BOOST_CHECK_EQUAL(dbres[LibPg::RowIndex{3}][LibPg::ColumnIndex{0}].as<int>(), 3);
}

BOOST_AUTO_TEST_SUITE_END()//TestSavepoint
