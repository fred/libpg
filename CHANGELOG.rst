CHANGELOG
=========

.. contents:: Releases
   :backlinks: none
   :local:

2.2.1 (2023-06-08)
------------------

* Update CI
* Update/Fix CMake build

2.2.0 (2021-12-17)
------------------

* Update CI
* Update CMake build
* Add possibility (hack!) to get raw pointer of ``libpq`` connection
  to provide support for collaboration with another database library
  - subject for removal in the future!

2.1.0 (2021-11-10)
------------------

* Bump dependency ``libstrong@1.1.0``
* Update/Fix CMake build

2.0.0 (2021-03-24)
------------------

* Switch to external ``libstrong`` library

1.1.0 (2021-02-19)
------------------

* Initial release
