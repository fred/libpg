/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/pg_result.hh"
#include "include/libpg/pg_exception.hh"
#include "include/libpg/detail/libpq_layer_interface.hh"

#include <boost/numeric/conversion/cast.hpp>

#include <utility>

namespace LibPg {

namespace {

const char* get_sql_state(const ::PGresult* result)
{
    return const_cast<const char*>(Detail::libpq_layer().PQresultErrorField(result, PG_DIAG_SQLSTATE));
}

const char* get_message_primary(const ::PGresult* result)
{
    return const_cast<const char*>(Detail::libpq_layer().PQresultErrorField(result, PG_DIAG_MESSAGE_PRIMARY));
}

class ExecFailureImpl : public ExecFailure
{
public:
    ExecFailureImpl(const PgConnection::ExecResult::ErrorCode& error_code, std::string msg)
        : error_code_{error_code},
          msg_{std::move(msg)}
    { }
    const char* what()const noexcept override
    {
        return msg_.c_str();
    }
    PgConnection::ExecResult::ErrorCode error_code()const noexcept override
    {
        return error_code_;
    }
private:
    const PgConnection::ExecResult::ErrorCode error_code_;
    const std::string msg_;
};

template <::ExecStatusType status>
class ExecFailureBy : public ExecFailureImpl
{
public:
    explicit ExecFailureBy(const PgConnection::ExecResult::ErrorCode& error_code)
        : ExecFailureImpl{error_code, Detail::libpq_layer().PQresStatus(status)}
    { }
};

template <>
class ExecFailureBy<::PGRES_FATAL_ERROR> : public ExecFailureImpl
{
public:
    explicit ExecFailureBy(const PgConnection::ExecResult::ErrorCode& error_code, std::string message)
        : ExecFailureImpl{error_code, std::move(message)}
    { }
};


bool is_correct_error_code(const char* str)noexcept
{
    return (str != nullptr) &&
           (str[0] != '\0') &&
           (str[1] != '\0') &&
           (str[2] != '\0') &&
           (str[3] != '\0') &&
           (str[4] != '\0') &&
           (str[5] == '\0');
}

auto make_major(const char* str)noexcept
{
    using ClassPart = std::array<char, 2>;
    if (is_correct_error_code(str))
    {
        return ClassPart{str[0], str[1]};
    }
    static constexpr ClassPart invalid_class_part{'\0', '\0'};
    return invalid_class_part;
}

auto make_minor(const char* str)noexcept
{
    using SubclassPart = std::array<char, 3>;
    if (is_correct_error_code(str))
    {
        return SubclassPart{str[2], str[3], str[4]};
    }
    static constexpr SubclassPart invalid_subclass_part{'\0', '\0', '\0'};
    return invalid_subclass_part;
}

::PGresult* to_pg_result(AliasForPGresult* result)
{
    return reinterpret_cast<::PGresult*>(result);
}

template <typename D>
::PGresult* to_pg_result(const std::unique_ptr<AliasForPGresult, D>& result)
{
    return to_pg_result(result.get());
}

}//namespace LibPg::{anonymous}

PgConnection::ExecResult::ExecResult()
{ }

PgConnection::ExecResult::ExecResult(ExecResult&& src)
    : result_{std::move(src.result_)}
{
}

PgConnection::ExecResult::ExecResult(AliasForPGresult* result)
    : result_(result)
{
    this->usability_required();
}

PgConnection::ExecResult& PgConnection::ExecResult::operator=(ExecResult&& src)
{
    result_ = std::move(src.result_);
    return *this;
}

PgConnection::ExecResult::~ExecResult()
{ }

void PgConnection::ExecResult::PGresultDeleter::operator()(AliasForPGresult* result)const noexcept
{
    if (result != nullptr)
    {
        Detail::libpq_layer().PQclear(to_pg_result(result));
    }
}

void PgConnection::ExecResult::usability_required()const
{
    if (result_ == nullptr)
    {
        struct ResultIsNull : PgException
        {
            const char* what()const noexcept override
            {
                return "result is NULL";
            }
        };
        throw ResultIsNull{};
    }
    switch (Detail::libpq_layer().PQresultStatus(to_pg_result(result_)))
    {
        case ::PGRES_EMPTY_QUERY: return;
        case ::PGRES_COMMAND_OK: return;
        case ::PGRES_TUPLES_OK: return;
        case ::PGRES_SINGLE_TUPLE: return;
        case ::PGRES_FATAL_ERROR: throw ExecFailureBy<::PGRES_FATAL_ERROR>{this->make_error_code(),
                                                                           get_message_primary(to_pg_result(result_))};
        case ::PGRES_COPY_IN: throw ExecFailureBy<::PGRES_COPY_IN>{this->make_error_code()};
        case ::PGRES_COPY_OUT: throw ExecFailureBy<::PGRES_COPY_OUT>{this->make_error_code()};
        case ::PGRES_COPY_BOTH: throw ExecFailureBy<::PGRES_COPY_BOTH>{this->make_error_code()};
        case ::PGRES_BAD_RESPONSE: throw ExecFailureBy<::PGRES_BAD_RESPONSE>{this->make_error_code()};
        case ::PGRES_NONFATAL_ERROR: throw ExecFailureBy<::PGRES_NONFATAL_ERROR>{this->make_error_code()};
    }
    throw ExecFailureImpl(this->make_error_code(), "exec done with unknown status");
}

void PgConnection::ExecResult::strict_usability_required()const
{
    switch (Detail::libpq_layer().PQresultStatus(to_pg_result(result_)))
    {
        case ::PGRES_EMPTY_QUERY:
            struct EmptyQuery : PgException
            {
                const char* what()const noexcept override { return "empty query not allowed"; }
            };
            throw EmptyQuery{};
        case ::PGRES_SINGLE_TUPLE:
            struct SingleTuple : PgException
            {
                const char* what()const noexcept override { return "single tuple not allowed"; }
            };
            throw SingleTuple{};
        case ::PGRES_COMMAND_OK:
            return;
        case ::PGRES_TUPLES_OK:
            return;
        default:
            throw ExecFailureImpl(this->make_error_code(), "exec done with unexpected status");
    }
}

void PgConnection::ExecResult::tuples_ok_required()const
{
    switch (Detail::libpq_layer().PQresultStatus(to_pg_result(result_)))
    {
        case ::PGRES_TUPLES_OK:
            return;
        case ::PGRES_EMPTY_QUERY:
            struct EmptyQuery : PgException
            {
                const char* what()const noexcept override { return "tuple expected, not an empty query"; }
            };
            throw EmptyQuery{};
        case ::PGRES_SINGLE_TUPLE:
            struct SingleTuple : PgException
            {
                const char* what()const noexcept override { return "tuple expected, not a single tuple"; }
            };
            throw SingleTuple{};
        case ::PGRES_COMMAND_OK:
            struct Command : PgException
            {
                const char* what()const noexcept override { return "tuples expected, not a command"; }
            };
            throw Command{};
        default:
            throw ExecFailureImpl(this->make_error_code(), "exec done with unexpected status");
    }
}

void PgConnection::ExecResult::command_ok_required()const
{
    switch (Detail::libpq_layer().PQresultStatus(to_pg_result(result_)))
    {
        case ::PGRES_COMMAND_OK:
            return;
        case ::PGRES_EMPTY_QUERY:
            struct EmptyQuery : PgException
            {
                const char* what()const noexcept override { return "command expected, not an empty query"; }
            };
            throw EmptyQuery{};
        case ::PGRES_SINGLE_TUPLE:
            struct SingleTuple : PgException
            {
                const char* what()const noexcept override { return "command expected, not a single tuple"; }
            };
            throw SingleTuple{};
        case ::PGRES_TUPLES_OK:
            struct Tuples : PgException
            {
                const char* what()const noexcept override { return "command expected, not the tuples"; }
            };
            throw Tuples{};
        default:
            throw ExecFailureImpl(this->make_error_code(), "exec done with unexpected status");
    }
}

bool PgConnection::ExecResult::empty()const noexcept
{
    return this->get_number_of_rows() == 0;
}

PgConnection::ExecResult::SizeType PgConnection::ExecResult::get_number_of_rows()const noexcept
{
    return Detail::libpq_layer().PQntuples(to_pg_result(result_));
}

PgConnection::ExecResult::SizeType PgConnection::ExecResult::get_number_of_columns()const noexcept
{
    return Detail::libpq_layer().PQnfields(to_pg_result(result_));
}

bool PgConnection::ExecResult::is_null(SizeType row_idx, SizeType column_idx)const
{
    if ((row_idx < 0) || (this->get_number_of_rows() <= row_idx))
    {
        struct RowIndexOutOfRange : PgException
        {
            const char* what()const noexcept override { return "row index out of range"; }
        };
        throw RowIndexOutOfRange{};
    }
    if ((column_idx < 0) || (this->get_number_of_columns() <= column_idx))
    {
        struct ColumnIndexOutOfRange : PgException
        {
            const char* what()const noexcept override { return "column index out of range"; }
        };
        throw ColumnIndexOutOfRange{};
    }
    return Detail::libpq_layer().PQgetisnull(to_pg_result(result_), row_idx, column_idx);
}

bool PgConnection::ExecResult::is_tuples()const
{
    return Detail::libpq_layer().PQresultStatus(to_pg_result(result_)) == ::PGRES_TUPLES_OK;
}

PgConnection::ExecResult::SizeType PgConnection::ExecResult::get_column_idx(const char* column_name)const
{
    const SizeType column_idx{Detail::libpq_layer().PQfnumber(to_pg_result(result_), column_name)};
    const bool name_match_column = column_idx != -1;
    if (!name_match_column)
    {
        struct ColumnNameOutOfRange : PgException
        {
            const char* what()const noexcept override { return "name does not match any column"; }
        };
        throw ColumnNameOutOfRange{};
    }
    return column_idx;
}

std::string PgConnection::ExecResult::get_not_null_value(SizeType row_idx, SizeType column_idx)const
{
    if (!this->is_null(row_idx, column_idx))
    {
        return std::string{Detail::libpq_layer().PQgetvalue(to_pg_result(result_), row_idx, column_idx),
                           boost::numeric_cast<std::string::size_type>(Detail::libpq_layer().PQgetlength(to_pg_result(result_), row_idx, column_idx))};
    }
    struct ValueIsNull : PgException
    {
        const char* what()const noexcept override { return "value is NULL"; }
    };
    throw ValueIsNull{};
}

std::string PgConnection::ExecResult::get_column_name(SizeType column_idx)const
{
    const char* const column_name = Detail::libpq_layer().PQfname(to_pg_result(result_), column_idx);
    if (column_name != nullptr)
    {
        return std::string{column_name};
    }
    struct ColumnIndexOutOfRange : PgException
    {
        const char* what()const noexcept override { return "column index is out of range"; }
    };
    throw ColumnIndexOutOfRange{};
}

PgConnection::ExecResult::SizeType PgConnection::ExecResult::get_number_of_rows_affected()const
{
    try
    {
        const char* const number_as_str = Detail::libpq_layer().PQcmdTuples(to_pg_result(result_));
        return std::stoi(std::string{number_as_str});
    }
    catch (const std::invalid_argument&)
    {
        struct NoValueAvailable : PgException
        {
            const char* what()const noexcept override { return "no value available"; }
        };
        throw NoValueAvailable{};
    }
    catch (const std::out_of_range&)
    {
        struct OutOfRangeValue : PgException
        {
            const char* what()const noexcept override { return "value is out of range"; }
        };
        throw OutOfRangeValue{};
    }
    catch (...)
    {
        struct UnexpectedException : PgException
        {
            const char* what()const noexcept override { return "conversion failed unexpectedly"; }
        };
        throw UnexpectedException{};
    }
}

PgConnection::ExecResult::ErrorCode PgConnection::ExecResult::make_error_code()const
{
    if (result_ == nullptr)
    {
        throw std::runtime_error("result is NULL");
    }
    return ErrorCode{get_sql_state(to_pg_result(result_))};
}

PgConnection::ExecResult::ErrorCode::ErrorCode(const char* error_code)noexcept
    : major_{make_major(error_code)},
      minor_{make_minor(error_code)}
{
    static_assert(std::tuple_size<decltype(major_)>::value == 2, "class part of the error code must be 2 characters long");
    static_assert(std::tuple_size<decltype(minor_)>::value == 3, "subclass part of the error code must be 3 characters long");
}


PgResult::PgResult(PgConnection::ExecResult exec_result)
    : result_{std::move(exec_result)}
{
    result_.strict_usability_required();
}

PgResult& PgResult::operator=(PgConnection::ExecResult&& exec_result)
{
    result_ = std::move(exec_result);
    result_.strict_usability_required();
    return *this;
}

PgResult::PgResult()
    : result_{}
{ }

PgResult::PgResult(PgResult&& src)
    : result_{std::move(src.result_)}
{ }

PgResult& PgResult::operator=(PgResult&& src)
{
    result_ = std::move(src.result_);
    return *this;
}

bool PgResult::empty()const noexcept
{
    return result_.empty();
}

RowIndex PgResult::size()const noexcept
{
    return RowIndex{result_.get_number_of_rows()};
}

std::string PgResult::get_column_name(ColumnIndex column_idx)const
{
    return result_.get_column_name(*column_idx);
}

PgResult::ConstRowPseudoIterator PgResult::begin()const
{
    return ConstRowPseudoIterator{*this, 0};
}

PgResult::ConstRowPseudoIterator PgResult::end()const
{
    return ConstRowPseudoIterator{*this, result_.get_number_of_rows()};
}

PgResult::ConstRowPseudoIterator PgResult::cbegin()const
{
    return this->begin();
}

PgResult::ConstRowPseudoIterator PgResult::cend()const
{
    return this->end();
}

PgResult::Row PgResult::operator[](RowIndex row_idx)const
{
    return *(this->begin() + *row_idx);
}

PgResult::Row PgResult::at(RowIndex row_idx)const
{
    return *(this->begin() + *row_idx);
}

void PgResult::tuples_ok_required()const
{
    result_.tuples_ok_required();
}

void PgResult::command_ok_required()const
{
    result_.command_ok_required();
}

RowIndex PgResult::get_number_of_rows()const noexcept
{
    return RowIndex{result_.get_number_of_rows()};
}

ColumnIndex PgResult::get_number_of_columns()const noexcept
{
    return ColumnIndex{result_.get_number_of_columns()};
}

RowIndex PgResult::get_number_of_rows_affected()const
{
    return RowIndex{result_.get_number_of_rows_affected()};
}

bool PgResult::is_null(SizeType row_idx, SizeType column_idx)const
{
    return  result_.is_null(row_idx, column_idx);
}

std::string PgResult::get_not_null_value(SizeType row_idx, SizeType column_idx)const
{
    return  result_.get_not_null_value(row_idx, column_idx);
}

PgResult::SizeType PgResult::get_column_idx(const char* column_name)const
{
    return  result_.get_column_idx(column_name);
}


PgResult::ConstValuePseudoIterator::ConstValuePseudoIterator(const Row* row, SizeType column_idx)noexcept
    : row_{row},
      column_idx_{column_idx}
{ }

PgResult::Value PgResult::ConstValuePseudoIterator::operator*()const
{
    if (can_be_dereferenced(*row_, column_idx_))
    {
        return (*row_)[ColumnIndex{column_idx_}];
    }
    struct OutOfRange : PgException
    {
        const char* what()const noexcept override { return "column index out of range"; }
    };
    throw OutOfRange{};
}

bool PgResult::ConstValuePseudoIterator::can_be_dereferenced(const Row& row, SizeType column_idx)noexcept
{
    return (0 <= column_idx) && (ColumnIndex{column_idx} < row.container_->get_number_of_columns());
}

PgResult::ConstValuePseudoIterator& PgResult::ConstValuePseudoIterator::add(SizeType offset)
{
    column_idx_ += offset;
    return *this;
}

PgResult::ConstValuePseudoIterator& PgResult::ConstValuePseudoIterator::sub(SizeType offset)
{
    column_idx_ -= offset;
    return *this;
}

PgResult::SizeType PgResult::ConstValuePseudoIterator::comparable()const
{
    return column_idx_;
}

PgResult::SizeType PgResult::ConstValuePseudoIterator::difference(const ConstValuePseudoIterator& rhs)const
{
    return column_idx_ - rhs.column_idx_;
}

PgResult::ConstRowPseudoIterator::ConstRowPseudoIterator(const PgResult& container, SizeType row_idx)noexcept
    : row_{&container, row_idx}
{ }

PgResult::Row PgResult::ConstRowPseudoIterator::operator*()const
{
    if ((0 <= row_.idx_) && (RowIndex{row_.idx_} < row_.container_->size()))
    {
        return row_;
    }
    struct OutOfRange : PgException
    {
        const char* what()const noexcept override { return "row index out of range"; }
    };
    throw OutOfRange{};
}

PgResult::ConstRowPseudoIterator& PgResult::ConstRowPseudoIterator::add(SizeType offset)
{
    row_.idx_ += offset;
    return *this;
}

PgResult::ConstRowPseudoIterator& PgResult::ConstRowPseudoIterator::sub(SizeType offset)
{
    row_.idx_ -= offset;
    return *this;
}

PgResult::SizeType PgResult::ConstRowPseudoIterator::comparable()const
{
    return row_.idx_;
}

PgResult::SizeType PgResult::ConstRowPseudoIterator::difference(const ConstRowPseudoIterator& rhs)const
{
    return row_.idx_ - rhs.row_.idx_;
}


PgResult::Row::Row(const PgResult* container, SizeType row_idx)
    : container_{container},
      idx_{row_idx}
{ }

PgResult::Row::Row(const Row& src)noexcept
    : container_{src.container_},
      idx_{src.idx_}
{ }

PgResult::Row& PgResult::Row::operator=(const Row& src)noexcept
{
    container_ = src.container_;
    idx_ = src.idx_;
    return *this;
}

bool PgResult::Row::empty()const noexcept
{
    return this->size() == ColumnIndex{0};
}

ColumnIndex PgResult::Row::size()const noexcept
{
    return this->get_number_of_columns();
}

ColumnIndex PgResult::Row::get_number_of_columns()const noexcept
{
    return container_->get_number_of_columns();
}

std::string PgResult::Row::get_column_name(ColumnIndex column_idx)const
{
    return container_->get_column_name(column_idx);
}

PgResult::Value PgResult::Row::operator[](ColumnIndex column_idx)const
{
    if (container_->is_null(idx_, *column_idx))
    {
        return Value(nullptr);
    }
    return Value(container_->get_not_null_value(idx_, *column_idx));
}

PgResult::Value PgResult::Row::operator[](const char* column_name)const
{
    return (*this)[ColumnIndex{container_->get_column_idx(column_name)}];
}

PgResult::Value PgResult::Row::operator[](const std::string& column_name)const
{
    return (*this)[column_name.c_str()];
}

PgResult::Value PgResult::Row::at(ColumnIndex column_idx)const
{
    return (*this)[column_idx];
}

PgResult::Value PgResult::Row::at(const char* column_name)const
{
    return (*this)[column_name];
}

PgResult::Value PgResult::Row::at(const std::string& column_name)const
{
    return this->at(column_name.c_str());
}

PgResult::ConstValuePseudoIterator PgResult::Row::begin()const
{
    return ConstValuePseudoIterator{this, 0};
}

PgResult::ConstValuePseudoIterator PgResult::Row::end()const
{
    return ConstValuePseudoIterator{this, *(container_->get_number_of_columns())};
}

PgResult::ConstValuePseudoIterator PgResult::Row::cbegin()const
{
    return this->begin();
}

PgResult::ConstValuePseudoIterator PgResult::Row::cend()const
{
    return this->end();
}

void PgResult::Row::required_number_of_columns(ColumnIndex required_value)const
{
    if (this->get_number_of_columns() != required_value)
    {
        struct UnexpectedNumberOfColumns : PgException
        {
            const char* what()const noexcept override { return "unexpected number of columns"; }
        };
        throw UnexpectedNumberOfColumns{};
    }
}

PgResult::Value::Value(std::string value)noexcept
    : str_value_{std::move(value)}
{ }

PgResult::Value::Value()noexcept
    : Value{nullptr}
{ }

PgResult::Value::Value(std::nullptr_t)noexcept
    : str_value_{boost::none}
{ }

PgResult::Value::Value(const Value& src)noexcept
    : str_value_{src.str_value_}
{ }

PgResult::Value& PgResult::Value::operator=(const Value& src)noexcept
{
    str_value_ = src.str_value_;
    return *this;
}

bool PgResult::Value::is_null()const noexcept
{
    return str_value_ == boost::none;
}

bool PgResult::Value::operator==(NullValueT)const noexcept
{
    return this->is_null();
}

bool PgResult::Value::operator!=(NullValueT)const noexcept
{
    return !((*this) == null_value);
}

bool operator==(NullValueT, const PgResult::Value& value)noexcept
{
    return value == null_value;
}

bool operator!=(NullValueT, const PgResult::Value& value)noexcept
{
    return value != null_value;
}

const char* PgResult::Value::get_raw_data()const
{
    if (this->is_null())
    {
        struct NoRawDataAvailable : PgException
        {
            const char* what()const noexcept override { return "value is NULL, no raw data available"; }
        };
        throw NoRawDataAvailable{};
    }
    return str_value_->c_str();
}

PgResultTuples::PgResultTuples(PgConnection::ExecResult exec_result)
    : result_{std::move(exec_result)}
{
    result_.tuples_ok_required();
}

PgResultTuples& PgResultTuples::operator=(PgConnection::ExecResult&& exec_result)
{
    result_ = std::move(exec_result);
    result_.tuples_ok_required();
    return *this;
}

PgResultTuples::PgResultTuples()
    : result_{}
{ }

PgResultTuples::PgResultTuples(PgResultTuples&& src)
    : result_{std::move(src.result_)}
{ }

PgResultTuples& PgResultTuples::operator=(PgResultTuples&& src)
{
    result_ = std::move(src.result_);
    return *this;
}

bool PgResultTuples::empty()const noexcept
{
    return result_.empty();
}

RowIndex PgResultTuples::size()const noexcept
{
    return result_.size();
}

RowIndex PgResultTuples::get_number_of_rows()const noexcept
{
    return result_.get_number_of_rows();
}

ColumnIndex PgResultTuples::get_number_of_columns()const noexcept
{
    return result_.get_number_of_columns();
}

std::string PgResultTuples::get_column_name(ColumnIndex column_idx)const
{
    return result_.get_column_name(column_idx);
}

RowIndex PgResultTuples::get_number_of_rows_affected()const
{
    return result_.get_number_of_rows_affected();
}

PgResultTuples::Row PgResultTuples::operator[](RowIndex row_idx)const
{
    return result_[row_idx];
}

PgResultTuples::Row PgResultTuples::at(RowIndex row_idx)const
{
    return result_.at(row_idx);
}

PgResultTuples::ConstRowPseudoIterator PgResultTuples::begin()const
{
    return result_.begin();
}

PgResultTuples::ConstRowPseudoIterator PgResultTuples::end()const
{
    return result_.end();
}

PgResultTuples::ConstRowPseudoIterator PgResultTuples::cbegin()const
{
    return result_.cbegin();
}

PgResultTuples::ConstRowPseudoIterator PgResultTuples::cend()const
{
    return result_.cend();
}

PgResultNoData::PgResultNoData(PgConnection::ExecResult exec_result)
    : result_{std::move(exec_result)}
{
    result_.command_ok_required();
}

PgResultNoData::PgResultNoData()
    : result_{}
{ }

PgResultNoData::PgResultNoData(PgResultNoData&& src)
    : result_{std::move(src.result_)}
{ }

PgResultNoData& PgResultNoData::operator=(PgResultNoData&& src)
{
    result_ = std::move(src.result_);
    return *this;
}

RowIndex PgResultNoData::get_number_of_rows_affected()const
{
    return result_.get_number_of_rows_affected();
}

}//namespace LibPg
