/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libpg/extendable_query_arguments.hh"
#include "include/libpg/pg_exception.hh"

#include <boost/numeric/conversion/cast.hpp>

#include <utility>

namespace LibPg {

QueryArguments<>::QueryArguments()
    : arguments_{}
{ }

int QueryArguments<>::size()const noexcept
{
    return boost::numeric_cast<int>(arguments_.size());
}

bool QueryArguments<>::empty()const noexcept
{
    return this->size() == 0;
}

QueryParameter<> QueryArguments<>::parameter(int idx)const
{
    if ((idx < 0) || (this->size() <= idx))
    {
        struct IndexOutOfRange : PgException
        {
            const char* what()const noexcept { return "idx out of range"; }
        };
        throw IndexOutOfRange{};
    }
    return query_parameter_at_position(idx);
}

std::vector<const char*> QueryArguments<>::get_arguments()const
{
    std::vector<const char*> data;
    data.reserve(arguments_.size());
    for (const auto& argument : arguments_)
    {
        data.push_back(argument == boost::none ? nullptr
                                               : argument->c_str());
    }
    return data;
}

}//namespace LibPg
