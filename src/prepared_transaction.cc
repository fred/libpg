/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libpg/prepared_transaction.hh"
#include "include/libpg/pg_result.hh"

namespace LibPg {

namespace {

decltype(auto) get_valid_prepared_transaction_id(std::string value)
{
    if (value.empty())
    {
        struct PreparedTransactionIdTooShort : PgException
        {
            const char* what()const noexcept override { return "prepared transaction id can not be empty"; }
        };
        throw PreparedTransactionIdTooShort{};
    }
    static constexpr auto max_length_of_prepared_transaction_id = 200u;
    if (max_length_of_prepared_transaction_id < value.length())
    {
        struct PreparedTransactionIdTooLong : PgException
        {
            const char* what()const noexcept override { return "prepared transaction id too long"; }
        };
        throw PreparedTransactionIdTooLong{};
    }
    //Postgres PREPARE TRANSACTION commands family doesn't accept parameters, requires immediate argument,
    //so I check unsafe character(s) in transaction identifier.
    if (value.find('\'') != std::string::npos)
    {
        struct PreparedTransactionIdTooUnsafe : PgException
        {
            const char* what()const noexcept override { return "prepared transaction id too unsafe"; }
        };
        throw PreparedTransactionIdTooUnsafe{};
    }
    return value;
}

}//namespace LibPg::{anonymous}

PreparedTransactionId::PreparedTransactionId(std::string prepared_transaction_id)
    : id_{get_valid_prepared_transaction_id(std::move(prepared_transaction_id))}
{ }

const std::string& PreparedTransactionId::get()const
{
    return id_;
}

PgConnection prepare_transaction(PgRwTransaction transaction, const PreparedTransactionId& id)
{
    PgConnection conn{std::move(transaction).get_conn()};
    const PgResultNoData cmdres = exec(conn, "PREPARE TRANSACTION '" + id.get() + "'");
    return conn;
}

void commit_prepared_transaction(const PgConnection& conn, const PreparedTransactionId& id)
{
    const PgResultNoData cmdres = exec(conn, "COMMIT PREPARED '" + id.get() + "'");
}

void rollback_prepared_transaction(const PgConnection& conn, const PreparedTransactionId& id)
{
    const PgResultNoData cmdres = exec(conn, "ROLLBACK PREPARED '" + id.get() + "'");
}

}//namespace LibPg
