/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/libpq_layer.hh"
#include "include/libpg/pg_exception.hh"
#include "include/libpg/detail/libpq_layer_interface.hh"

namespace LibPg {

LibPqLayer::~LibPqLayer()
{ }

LibPqLayer* set_libpq_layer(LibPqLayer* new_layer)
{
    if (new_layer == nullptr)
    {
        return static_cast<LibPqLayer*>(Detail::set_libpq_layer_impl(nullptr));
    }
    auto* const new_impl = dynamic_cast<Detail::LibPqLayerInterface*>(new_layer);
    if (new_impl == nullptr)
    {
        struct NotLibPqInterface : PgException
        {
            const char* what()const noexcept override { return "not libpq interface"; }
        };
        throw NotLibPqInterface{};
    }
    auto* const old_layer = Detail::set_libpq_layer_impl(new_impl);
    return static_cast<LibPqLayer*>(old_layer);
}

}//namespace LibPg
