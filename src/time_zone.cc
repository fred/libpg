/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/time_zone.hh"
#include "include/libpg/pg_connection.hh"
#include "include/libpg/pg_exception.hh"
#include "include/libpg/pg_ro_transaction.hh"
#include "include/libpg/pg_rw_transaction.hh"
#include "include/libpg/query.hh"
#include "include/libpg/sql_state/code.hh"

namespace LibPg {

TimeZone::TimeZone(std::string psql_representation)
    : psql_representation_{std::move(psql_representation)}
{ }

namespace HardcodedTimezones {

extern const char* utc;
const char* utc = "UTC";

}//namespace LibPg::HardcodedTimezones

TimeZone TimeZone::utc()
{
    return make_time_zone<HardcodedTimezones::utc>();
}

TimeZone make_time_zone(const PgConnection& conn, std::string psql_representation)
{
    const auto dbres = exec(conn,
                            make_query() << "SELECT" << item<int>() << "1 "
                                            "FROM pg_timezone_names "
                                            "WHERE LOWER(name)=LOWER(" << parameter<std::string>().as_text() << ")",
                            psql_representation);
    if (RowIndex{0} < dbres.size())
    {
        return TimeZone{std::move(psql_representation)};
    }
    struct TimeZoneDoesNotExist : PgException
    {
        const char* what()const noexcept override { return "time zone does not exist in pg_timezone_names"; }
    };
    throw TimeZoneDoesNotExist{};
}

TimeZone make_time_zone(const PgTransaction& transaction, std::string psql_representation)
{
    try
    {
        try_exec(transaction,
                 make_query() << "SELECT" << item<int>() << "0 "
                                 "FROM (SELECT '2019-10-27 00:00:00'::TIMESTAMP AT TIME ZONE " << parameter<std::string>().as_text() << ") AS c(t) "
                                 "WHERE t=(t+'1DAY'::INTERVAL)",
                 psql_representation);
        return TimeZone{std::move(psql_representation)};
    }
    catch (const ExecFailure& e)
    {
        if (e.error_code() == SqlState::invalid_parameter_value)
        {
            struct InvalidTimeZone : PgException
            {
                const char* what()const noexcept override { return "invalid time zone"; }
            };
            throw InvalidTimeZone{};
        }
        throw;
    }
    catch (...) { }
    struct CheckFailed : PgException
    {
        const char* what()const noexcept override { return "time zone check failed"; }
    };
    throw CheckFailed{};
}

TimeZone make_time_zone(const PgRoTransaction& transaction, std::string psql_representation)
{
    return make_time_zone(static_cast<const PgTransaction&>(transaction), std::move(psql_representation));
}

TimeZone make_time_zone(const PgRwTransaction& transaction, std::string psql_representation)
{
    return make_time_zone(static_cast<const PgTransaction&>(transaction), std::move(psql_representation));
}

const std::string& to_psql(const TimeZone& time_zone)
{
    return time_zone.psql_representation_;
}

}//namespace LibPg
