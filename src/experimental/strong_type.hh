/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRONG_TYPE_HH_C2D14599F7BD03A83679131D1C49B135//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define STRONG_TYPE_HH_C2D14599F7BD03A83679131D1C49B135

#include <iostream>
#include <type_traits>
#include <utility>

namespace Experimental {

template <typename Type, typename Tag, template <typename> class ...Features>
class StrongType : public Features<StrongType<Type, Tag, Features...>>...
{
public:
    using UnderlyingType = Type;
    explicit constexpr StrongType(UnderlyingType src)
        : value_{std::move(src)}
    { }
    constexpr const UnderlyingType& get()const&
    {
        return value_;
    }
    constexpr UnderlyingType&& get()&&
    {
        return std::move(value_);
    }
    template <typename F>
    constexpr StrongType& invoke(F&& fnc)&
    {
        std::forward<F>(fnc)(value_);
        return *this;
    }
private:
    UnderlyingType value_;
};

namespace Feature {

template <typename> class HasPrint;

template <typename Type, typename Tag, template <typename> class ...Features>
class HasPrint<StrongType<Type, Tag, Features...>>
{
private:
    using Derived = StrongType<Type, Tag, Features...>;
    friend std::ostream& operator<<(std::ostream& out, const Derived& value)
    {
        return out << value.get();
    }
};

template <typename> class HasAddAndSub;

template <typename Type, typename Tag, template <typename> class ...Features>
class HasAddAndSub<StrongType<Type, Tag, Features...>>
{
public:
    constexpr decltype(auto) operator++(int)
    {
        struct Operation
        {
            constexpr decltype(auto) operator()(Type& value)const { return value++; }
        };
        return Derived{this->derived().invoke(Operation{})};
    }
    constexpr decltype(auto) operator++()
    {
        struct Operation
        {
            constexpr void operator()(Type& value)const { ++value; }
        };
        this->derived().invoke(Operation{});
        return this->derived();
    }
    constexpr decltype(auto) operator+=(const Type& rhs)
    {
        struct Operation
        {
            constexpr explicit Operation(const Type& rhs)
                : rhs{rhs}
            { }
            constexpr void operator()(Type& value)const { value += rhs; }
            const Type& rhs;
        };
        this->derived().invoke(Operation{rhs});
        return this->derived();
    }
    constexpr decltype(auto) operator+(const Type& rhs)const
    {
        return Derived{this->derived().get() + rhs};
    }

    constexpr decltype(auto) operator--(int)
    {
        struct Operation
        {
            constexpr decltype(auto) operator()(Type& value)const { return value--; }
        };
        return Derived{this->derived().invoke(Operation{})};
    }
    constexpr decltype(auto) operator--()
    {
        struct Operation
        {
            constexpr void operator()(Type& value)const { --value; }
        };
        this->derived().invoke(Operation{});
        return this->derived();
    }
    constexpr decltype(auto) operator-=(const Type& rhs)
    {
        struct Operation
        {
            constexpr explicit Operation(const Type& rhs)
                : rhs{rhs}
            { }
            constexpr void operator()(Type& value)const { value -= rhs; }
            const Type& rhs;
        };
        this->derived().invoke(Operation{rhs});
        return this->derived();
    }
    constexpr decltype(auto) operator-(const Type& rhs)const
    {
        const auto& strong_type_instance = static_cast<const StrongType<Type, Tag, Features...>&>(*this);
        return Derived{strong_type_instance.get() - rhs};
    }
    constexpr decltype(auto) operator-(const StrongType<Type, Tag, Features...>& rhs)const
    {
        const auto& lhs = static_cast<const StrongType<Type, Tag, Features...>&>(*this);
        return lhs.get() - rhs.get();
    }
private:
    using Derived = StrongType<Type, Tag, Features...>;
    constexpr Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }
    constexpr const Derived& derived()const
    {
        return static_cast<const Derived&>(*this);
    }
    constexpr friend decltype(auto) operator+(const Type& lhs, const Derived& rhs)
    {
        return Derived{lhs + rhs.get()};
    }
    constexpr friend decltype(auto) operator-(const Type& lhs, const Derived& rhs)
    {
        return Derived{lhs - rhs.get()};
    }
};

}//namespace Experimental::Feature

void test(std::ostream& out)
{
    using Id = StrongType<int, struct Id_Tag_, Feature::HasPrint, Feature::HasAddAndSub>;
    constexpr Id zero{0};
    out << zero << std::endl;
    static_assert((++Id{zero}).get() == 1, "must be one");
    static_assert((Id{zero}++).get() == 1, "must be one");
    static_assert((zero + 1).get() == 1, "must be one");

    constexpr Id two{2};
    out << two << std::endl;
    static_assert((--Id{two}).get() == 1, "must be one");
    static_assert((Id{two}--).get() == 1, "must be one");
    static_assert((two - 1).get() == 1, "must be one");
    static_assert((two - Id{1}) == 1, "must be one");
}

}//namespace Experimental

#endif//STRONG_TYPE_HH_C2D14599F7BD03A83679131D1C49B135
