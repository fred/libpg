/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UNIQUE_TUPLE_HH_34F2FB9CBD3BB6E09C7A2CA76D3F2266//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UNIQUE_TUPLE_HH_34F2FB9CBD3BB6E09C7A2CA76D3F2266

#include "libpg/detail/has_only_unique_elements.hh"

#include <tuple>
#include <utility>

namespace LibPg {
namespace Detail {

template <typename ...Elements>
struct IndexedByType
{
    template <typename, typename = std::tuple<Elements...>> struct IsElement;
    template <typename Type, typename Head, typename ...Tail>
    struct IsElement<Type, std::tuple<Head, Tail...>>
        : std::integral_constant<bool, IsElement<Type, std::tuple<Tail...>>::value>
    { };
    template <typename Type, typename ...Seq>
    struct IsElement<Type, std::tuple<Type, Seq...>>
        : std::true_type
    { };
    template <typename Type>
    struct IsElement<Type, std::tuple<>>
        : std::false_type
    { };

    template <typename, typename = std::tuple<Elements...>> struct IndexOf;
    template <typename Type, typename ...Seq>
    struct IndexOf<Type, std::tuple<Type, Seq...>>
        : std::integral_constant<int, 0>
    { };
    template <typename Type, typename Head, typename ...Tail>
    struct IndexOf<Type, std::tuple<Head, Tail...>>
        : std::integral_constant<int, 1 + IndexOf<Type, std::tuple<Tail...>>::value>
    { };

    template <typename, typename = std::tuple<Elements...>> struct IsSame;
    template <typename ...A, typename ...B>
    struct IsSame<std::tuple<A...>, std::tuple<B...>>
        : std::is_same<std::tuple<A...>, std::tuple<B...>>::type
    { };

    template <typename = std::tuple<Elements...>, typename = std::tuple<>> struct AllElementsPresentJustOnce;
    template <typename SrcHead, typename ...SrcTail, typename ...Dst>
    struct AllElementsPresentJustOnce<std::tuple<SrcHead, SrcTail...>, std::tuple<Dst...>>
        : std::integral_constant<bool, !IsElement<SrcHead, std::tuple<Dst...>>::value &&
                                       AllElementsPresentJustOnce<std::tuple<SrcTail...>, std::tuple<SrcHead, Dst...>>::value>
    { };
    template <typename ...Dst>
    struct AllElementsPresentJustOnce<std::tuple<>, std::tuple<Dst...>>
        : std::true_type
    { };
    static_assert(AllElementsPresentJustOnce<>::value, "each element must present just once");

    template <typename, typename = std::tuple<Elements...>> struct IsSubset;
    template <typename SubsetHead, typename ...SubsetTail, typename ...Set>
    struct IsSubset<std::tuple<SubsetHead, SubsetTail...>, std::tuple<Set...>>
            : std::integral_constant<bool, IsElement<SubsetHead, std::tuple<Set...>>::value &&
                                           IsSubset<std::tuple<SubsetTail...>, std::tuple<Set...>>::value>
    { };
    template <typename ...Set>
    struct IsSubset<std::tuple<>, std::tuple<Set...>>
            : std::true_type
    { };
    template <typename A, typename B = std::tuple<Elements...>>
    struct HasTheSameElements : std::integral_constant<bool, IsSubset<A, B>::value &&
                                                             IsSubset<B, A>::value>
    { };
    template <typename A, typename B = std::tuple<Elements...>>
    struct DiffersJustInOrdering : std::integral_constant<bool, !std::is_same<A, B>::value &&
                                                                HasTheSameElements<A, B>::value>
    { };

    template <typename Type>
    static constexpr bool is_element()noexcept
    {
        return IsElement<Type>::value;
    }

    template <typename Type, std::enable_if_t<is_element<Type>()>* = nullptr>
    static constexpr decltype(auto) index_of()noexcept
    {
        return IndexOf<Type>::value;
    }

    template <typename Type, std::enable_if_t<!is_element<Type>()>* = nullptr>
    static constexpr decltype(auto) index_of()noexcept
    {
        static_assert(is_element<Type>(), "Type is not from Elements");
        return 0;
    }

    template <typename Type, typename StdGetArgument,
              std::enable_if_t<sizeof(std::get<index_of<Type>()>(std::declval<StdGetArgument>())) != 0>* = nullptr>
    static constexpr decltype(auto) get(StdGetArgument&& src)noexcept
    {
        return std::get<index_of<Type>()>(std::forward<StdGetArgument>(src));
    }

    template <typename Type, typename ...Args,
              std::enable_if_t<sizeof...(Args) == sizeof...(Elements)>* = nullptr>
    static constexpr decltype(auto) get_from_sequence(Args&& ...args)noexcept
    {
        return get<Type>(std::make_tuple(std::forward<Args>(args)...));
    }

    template <typename Type, typename ...Args,
              std::enable_if_t<IsSame<std::tuple<std::decay_t<Args>...>>::value &&
                               std::is_constructible<Type, Args...>::value>* = nullptr>
    static constexpr decltype(auto) make(Args&& ...properly_ordered_arguments)noexcept(noexcept(Type{std::declval<Args>()...}))
    {
        return Type{std::forward<Args>(properly_ordered_arguments)...};
    }

    template <typename Type, typename ...Args,
              std::enable_if_t<DiffersJustInOrdering<std::tuple<std::decay_t<Args>...>>::value>* = nullptr>
    static constexpr decltype(auto) make(Args&& ...incorrectly_ordered_arguments)noexcept
    {
        using ArgsOrdered = IndexedByType<std::decay_t<Args>...>;
        return make<Type>(ArgsOrdered::template get_from_sequence<Elements>(std::forward<Args>(incorrectly_ordered_arguments)...)...);
    }

    template <typename Type, typename ...Args,
              std::enable_if_t<!HasTheSameElements<std::tuple<std::decay_t<Args>...>>::value>* = nullptr>
    static constexpr void make(Args&&...)noexcept
    {
        static_assert(HasTheSameElements<std::tuple<std::decay_t<Args>...>>::value, "incompatible arguments");
    }

    template <typename ...UElements,
              std::enable_if_t<IsSame<std::tuple<std::decay_t<UElements>...>>::value>* = nullptr>
    static constexpr decltype(auto) reorder(const std::tuple<UElements...>& src)noexcept
    {
        return src;
    }

    template <typename ...UElements,
              std::enable_if_t<IsSame<std::tuple<std::decay_t<UElements>...>>::value>* = nullptr>
    static constexpr decltype(auto) reorder(std::tuple<UElements...>& src)noexcept
    {
        return src;
    }

    template <typename ...UElements,
              std::enable_if_t<IsSame<std::tuple<std::decay_t<UElements>...>>::value>* = nullptr>
    static constexpr decltype(auto) reorder(std::tuple<UElements...>&& src)noexcept
    {
        return src;
    }

    template <typename ...UElements,
              std::enable_if_t<DiffersJustInOrdering<std::tuple<std::decay_t<UElements>...>>::value>* = nullptr>
    static constexpr decltype(auto) reorder(const std::tuple<UElements...>& src)noexcept
    {
        using UOrdered = IndexedByType<std::decay_t<UElements>...>;
        return std::make_tuple(UOrdered::template get<Elements>(src)...);
    }

    template <typename ...UElements,
              std::enable_if_t<DiffersJustInOrdering<std::tuple<std::decay_t<UElements>...>>::value>* = nullptr>
    static constexpr decltype(auto) reorder(std::tuple<UElements...>&& src)noexcept
    {
        using UOrdered = IndexedByType<std::decay_t<UElements>...>;
        return std::make_tuple(UOrdered::template get<Elements>(std::move(src))...);
    }

    template <typename ...UElements>
    using EnableIfIsCompatible = std::enable_if_t<HasTheSameElements<std::tuple<std::decay_t<UElements>...>>::value>;
};

template <typename ...Elements>
class UniqueTuple : public std::tuple<Elements...>
{
public:
    using RawTuple = std::tuple<Elements...>;
    using DecayedTuple = std::tuple<std::decay_t<Elements>...>;
    static_assert(HasOnlyUniqueElements<DecayedTuple>::value, "decayed form of each type must present just once in the sequence of decayed forms");

    template <std::enable_if_t<std::is_default_constructible<RawTuple>::value>* = nullptr>
    constexpr UniqueTuple()
        : RawTuple{}
    { }

    /**
     * Implicitly constructs from complete list of arguments in a correct order
     * @tparam UElements their decayed forms corresponds with decayed forms of Elements
     * @param values list of arguments
     * @note the order of constructor arguments corresponds with order of Elements
     **/
    template <typename ...UElements,
              std::enable_if_t<std::is_same<DecayedTuple, std::tuple<std::decay_t<UElements>...>>::value &&
                               std::is_constructible<RawTuple, UElements...>::value>* = nullptr>
    constexpr UniqueTuple(UElements&& ...values)
        : RawTuple{std::forward<UElements>(values)...}
    { }

    /**
     * Implicitly constructs from complete list of arguments in a different order
     * @tparam UElements their decayed forms present in decayed forms of Elements (and conversely) but the order differs
     * @param values list of arguments
     * @note the order of constructor arguments does not have to correspond with order of UniqueTypes
     **/
    template <typename ...UElements,
              std::enable_if_t<HasTheSameElements<DecayedTuple, std::tuple<std::decay_t<UElements>...>>::value &&
                               !std::is_same<DecayedTuple, std::tuple<std::decay_t<UElements>...>>::value &&
                               std::is_constructible<RawTuple, UElements...>::value>* = nullptr>
    constexpr UniqueTuple(UElements&& ...values)
        : RawTuple{UniqueTuple<UElements...>{std::forward<UElements>(values)...}.template get<Elements>()...}
    { }

    /**
     * Implicitly converts from UniqueTuple with different order of elements
     * @param src UniqueTuple with different order of elements
     **/
    template <typename ...UElements,
             std::enable_if_t<HasTheSameElements<std::tuple<std::decay_t<UElements>...>, DecayedTuple>::value &&
                              !std::is_same<std::tuple<std::decay_t<UElements>...>, DecayedTuple>::value>* = nullptr>
    constexpr UniqueTuple(const UniqueTuple<UElements...>& src)
        : RawTuple{src.template get<Elements>()...}
    { }

    /**
     * Implicitly converts from UniqueTuple with different order of elements
     * @param src UniqueTuple with different order of elements
     **/
    template <typename ...UElements,
             std::enable_if_t<HasTheSameElements<std::tuple<std::decay_t<UElements>...>, DecayedTuple>::value &&
                              !std::is_same<std::tuple<std::decay_t<UElements>...>, DecayedTuple>::value>* = nullptr>
    constexpr UniqueTuple(UniqueTuple<UElements...>&& src)
        : RawTuple{std::move(src).template get<Elements>()...}
    { }

    template <typename Type>
    static constexpr bool has_element()noexcept
    {
        return IsElement<std::decay_t<Type>, DecayedTuple>::value;
    }
    template <typename Type>
    static constexpr auto index_of()noexcept
    {
        static_assert(has_element<Type>(), "Type must be element of this tuple");
        return IndexOfMember<std::decay_t<Type>, DecayedTuple>::value;
    }
    template <typename Type>
    constexpr decltype(auto) get()& noexcept
    {
        return std::get<index_of<Type>()>(*this);
    }
    template <typename Type>
    constexpr decltype(auto) get()&& noexcept
    {
        return std::get<index_of<Type>()>(*this);
    }
    template <typename Type>
    constexpr decltype(auto) get()const& noexcept
    {
        return std::get<index_of<Type>()>(*this);
    }
    template <typename Type>
    constexpr decltype(auto) get()const&& noexcept
    {
        return std::get<index_of<Type>()>(*this);
    }
    template <int index>
    constexpr decltype(auto) get()& noexcept
    {
        return std::get<index>(*this);
    }
    template <int index>
    constexpr decltype(auto) get()&& noexcept
    {
        return std::get<index>(*this);
    }
    template <int index>
    constexpr decltype(auto) get()const& noexcept
    {
        return std::get<index>(*this);
    }
    template <int index>
    constexpr decltype(auto) get()const&& noexcept
    {
        return std::get<index>(*this);
    }
    template <typename Type>
    constexpr decltype(auto) set(Type&& value)& noexcept
    {
        std::get<index_of<Type>()>(*this) = std::forward<Type>(value);
        return *this;
    }
private:
    template <typename, typename> struct IndexOfMember;
    template <typename Type, typename ...Seq>
    struct IndexOfMember<Type, std::tuple<Type, Seq...>>
        : std::integral_constant<int, 0>
    { };
    template <typename Type, typename Head, typename ...Tail>
    struct IndexOfMember<Type, std::tuple<Head, Tail...>>
        : std::integral_constant<int, 1 + IndexOfMember<Type, std::tuple<Tail...>>::value>
    { };
};

}//namespace LibPg::Detail
}//namespace LibPg

#endif//UNIQUE_TUPLE_HH_34F2FB9CBD3BB6E09C7A2CA76D3F2266
