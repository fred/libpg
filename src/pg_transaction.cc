/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/pg_transaction.hh"
#include "include/libpg/query.hh"

namespace LibPg {

namespace Unsafe {
namespace Dirty {
namespace Hack {

AliasForPGconn* get_raw_pointer(const PgTransaction& tx)
{
    return get_raw_pointer(tx.conn_);
}

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

template <>
const char* PgTransaction::cmd<PgTransaction::SessionDefault, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION";
}

template <>
const char* PgTransaction::cmd<PgTransaction::SessionDefault, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION READ WRITE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::SessionDefault, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION READ ONLY";
}

template <>
const char* PgTransaction::cmd<PgTransaction::ReadCommitted, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL READ COMMITTED";
}

template <>
const char* PgTransaction::cmd<PgTransaction::ReadCommitted, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL READ COMMITTED READ WRITE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::ReadCommitted, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL READ COMMITTED READ ONLY";
}

template <>
const char* PgTransaction::cmd<PgTransaction::ReadUncommitted, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL READ UNCOMMITTED";
}

template <>
const char* PgTransaction::cmd<PgTransaction::ReadUncommitted, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL READ UNCOMMITTED READ WRITE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::ReadUncommitted, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL READ UNCOMMITTED READ ONLY";
}

template <>
const char* PgTransaction::cmd<PgTransaction::RepeatableRead, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL REPEATABLE READ";
}

template <>
const char* PgTransaction::cmd<PgTransaction::RepeatableRead, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL REPEATABLE READ READ WRITE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::RepeatableRead, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL REPEATABLE READ READ ONLY";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::SessionDefault>, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::SessionDefault>, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ WRITE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::SessionDefault>, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ ONLY";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::Deferrable>, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE DEFERRABLE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::Deferrable>, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ WRITE DEFERRABLE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::Deferrable>, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ ONLY DEFERRABLE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::NotDeferrable>, PgTransaction::SessionDefault>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE NOT DEFERRABLE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::NotDeferrable>, PgTransaction::ReadWrite>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ WRITE NOT DEFERRABLE";
}

template <>
const char* PgTransaction::cmd<PgTransaction::Serializable<PgTransaction::NotDeferrable>, PgTransaction::ReadOnly>()noexcept
{
    return "START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ ONLY NOT DEFERRABLE";
}

PgTransaction::PgTransaction(PgTransaction&& src)
    : conn_{std::move(src.conn_)}
{ }

PgTransaction::~PgTransaction()
{ }

PgTransaction::operator const PgConnection&()const
{
    return conn_.get_useful_connection();
}

PgTransaction::Savepoint::Savepoint(const PgTransaction& transaction)
    : transaction_{transaction}
{
    exec(transaction_, make_query("SAVEPOINT _sp_"));
}

PgTransaction::Savepoint::~Savepoint()
{
    try
    {
        exec(transaction_, make_query("RELEASE SAVEPOINT _sp_"));
    }
    catch (...)
    {
    }
}

void PgTransaction::Savepoint::recovery()&& noexcept
{
    try
    {
        exec(transaction_, make_query("ROLLBACK TO SAVEPOINT _sp_"));
    }
    catch (...)
    {
    }
}

}//namespace LibPg
