/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/query_parameter.hh"

namespace LibPg {

std::string QueryParameterRuntimeIndex::as_counted_parameter()const
{
    return "$" + std::to_string(index_ + 1);
}

QueryParameterRuntimeType::QueryParameterRuntimeType(std::string type)
    : type_{std::move(type)}
{ }

std::string QueryParameterRuntimeType::as_type_suffix()const
{
    return type_.empty() ? std::string{}
                         : "::" + type_;
}

std::string QueryParameterCompiletimeType<void>::as_type_suffix()
{
    return std::string{};
}

}//namespace LibPg
