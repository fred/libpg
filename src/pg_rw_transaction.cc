/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libpg/pg_rw_transaction.hh"
#include "include/libpg/query.hh"

namespace LibPg {

namespace Unsafe {
namespace Dirty {
namespace Hack {

AliasForPGconn* get_raw_pointer(const PgRwTransaction& tx)
{
    return get_raw_pointer(tx.transaction_);
}

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

PgRwTransaction::PgRwTransaction(PgRwTransaction&& src)
    : transaction_{std::move(src.transaction_)}
{ }

PgRwTransaction::~PgRwTransaction()
{ }

PgRwTransaction::operator const PgConnection&()const
{
    return static_cast<const PgConnection&>(transaction_);
}

PgRwTransaction::operator const PgTransaction&()const
{
    return transaction_;
}

PgConnection PgRwTransaction::get_conn()&&
{
    return PgConnection{std::move(transaction_.conn_)};
}

PgConnection commit(PgRwTransaction transaction)
{
    PgConnection conn{std::move(transaction).get_conn()};
    exec(conn, make_query("COMMIT"));
    return conn;
}

PgConnection rollback(PgRwTransaction transaction)
{
    PgConnection conn{std::move(transaction).get_conn()};
    exec(conn, make_query("ROLLBACK"));
    return conn;
}

}//namespace LibPg
