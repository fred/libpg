/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/pg_connection.hh"
#include "include/libpg/pg_exception.hh"
#include "include/libpg/detail/libpq_layer_interface.hh"

#include <boost/numeric/conversion/cast.hpp>

#include <ios>
#include <sstream>
#include <vector>

namespace LibPg {

namespace Unsafe {
namespace Dirty {
namespace Hack {

AliasForPGconn* get_raw_pointer(const PgConnection& conn)
{
    return reinterpret_cast<AliasForPGconn*>(conn.conn_.get());
}

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

namespace {

class Dictionary
{
public:
    Dictionary(const Dsn& dsn)
    {
        this->push_back_if_has_value(dsn.host, "host");
        this->push_back_if_has_value(dsn.host_addr, "hostaddr");
        this->push_back_if_has_value(dsn.port, "port");
        this->push_back_if_has_value(dsn.db_name, "dbname");
        this->push_back_if_has_value(dsn.user, "user");
        this->push_back_if_has_value(dsn.password, "password");
        this->push_back_if_has_value(dsn.connect_timeout, "connect_timeout");
        values_.reserve(keywords_.size() + 1);
        for (const auto& value : value_storage_)
        {
            values_.push_back(value.c_str());
        }
        keywords_.push_back(nullptr);
        values_.push_back(nullptr);
    }
    const char* const* get_keywords()const
    {
        return keywords_.data();
    }
    const char* const* get_values()const
    {
        return values_.data();
    }
private:
    static const std::string& to_string(const std::string& value)
    {
        return value;
    }
    static std::string to_string(const boost::asio::ip::address& value)
    {
        return value.to_string();
    }
    static std::string to_string(std::uint16_t value)
    {
        return std::to_string(value);
    }
    static std::string to_string(const std::chrono::seconds& value)
    {
        return std::to_string(value.count());
    }
    template <typename T, typename Tag, template <typename> class Skills>
    Dictionary& push_back_if_has_value(
            const LibStrong::Optional<LibStrong::Type<T, Tag, Skills>>& option,
            const char* key)
    {
        if (option != LibStrong::Type<T, Tag, Skills>::nullopt)
        {
            keywords_.push_back(key);
            value_storage_.push_back(to_string(**option));
        }
        return *this;
    }
    std::vector<std::string> value_storage_;
    std::vector<const char*> keywords_;
    std::vector<const char*> values_;
};

AliasForPGconn* get_pg_conn(const Dsn& dsn)
{
    const Dictionary dict{dsn};
    static constexpr int do_not_expand_dbname{0};
    ::PGconn* const conn = Detail::libpq_layer().PQconnectdbParams(dict.get_keywords(), dict.get_values(), do_not_expand_dbname);
    return reinterpret_cast<AliasForPGconn*>(conn);
}

::PGconn* to_pg_conn(AliasForPGconn* conn)
{
    return reinterpret_cast<::PGconn*>(conn);
}

template <typename D>
::PGconn* to_pg_conn(const std::unique_ptr<AliasForPGconn, D>& conn)
{
    return reinterpret_cast<::PGconn*>(conn.get());
}

template <typename D>
::PGconn* to_not_null_pg_conn(const std::unique_ptr<AliasForPGconn, D>& conn)
{
    if (conn.get() == nullptr)
    {
        struct ConnIsNull : NoConnectionAvailable
        {
            const char* what()const noexcept override
            {
                return "conn is NULL";
            }
        };
        throw ConnIsNull{};
    }
    return to_pg_conn(conn);
}

void usability_required(const ::PGconn* conn)
{
    if (conn == nullptr)
    {
        throw std::runtime_error("conn is NULL");
    }
    switch (Detail::libpq_layer().PQstatus(conn))
    {
        case ::CONNECTION_OK:
            return;
        case ::CONNECTION_BAD:
        {
            struct ConnectionBad : ConnectFailure
            {
                const char* what()const noexcept override
                {
                    return "connection is in a CONNECTION_BAD state";
                }
            };
            throw ConnectionBad();
        }
        default:
            break;
    }
    struct ConnectionInUnknownState : ConnectFailure
    {
        const char* what()const noexcept override
        {
            return "connection is in an unknown state";
        }
    };
    throw ConnectionInUnknownState();
}

}//namespace LibPg::{anonymous}

PgConnection::PgConnection(const Dsn& dsn)
    : conn_(get_pg_conn(dsn))
{
    usability_required(to_pg_conn(conn_));
}

PgConnection::PgConnection(PgConnection&& src)
    : conn_(std::move(src.conn_))
{ }

PgConnection& PgConnection::operator=(PgConnection&& src)
{
    conn_ = std::move(src.conn_);
    return *this;
}

PgConnection::~PgConnection()
{ }

const PgConnection& PgConnection::get_useful_connection()const
{
    to_not_null_pg_conn(conn_);
    return *this;
}

PgConnection::ExecResult exec(const PgConnection& conn, const std::string& query)
{
    return conn.exec(query, nullptr, 0);
}

PgConnection::ExecResult PgConnection::exec(
        const std::string& query,
        const char* const* arguments,
        int number_of_arguments)const
{
    if (number_of_arguments == 0)
    {
        ::PGresult* const result = Detail::libpq_layer().PQexec(to_not_null_pg_conn(conn_), query.c_str());
        return PgConnection::ExecResult{reinterpret_cast<AliasForPGresult*>(result)};
    }
    static constexpr int result_in_text_format = 0;
    ::PGresult* const result = Detail::libpq_layer().PQexecParams(
            to_not_null_pg_conn(conn_),
            query.c_str(),
            number_of_arguments,
            nullptr,
            arguments,
            nullptr,
            nullptr,
            result_in_text_format);
    return ExecResult{reinterpret_cast<AliasForPGresult*>(result)};
}

void PgConnection::PGconnDeleter::operator()(AliasForPGconn* conn)const
{
    if (conn != nullptr)
    {
        Detail::libpq_layer().PQfinish(to_pg_conn(conn));
    }
}

std::ostream& operator<<(std::ostream& out, const PgConnection::ExecResult::ErrorCode& error_code)
{
    return out << std::string{error_code.major_.data(), sizeof(error_code.major_)}.append(error_code.minor_.data(), sizeof(error_code.minor_));
}

}//namespace LibPg
