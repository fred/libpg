/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/psql_type.hh"

namespace LibPg {
namespace Detail {

template <>
std::string get_psql_type_name<PsqlType::SmallInt>()
{
    return std::string{"SMALLINT"};
}

template <>
std::string get_psql_type_name<PsqlType::Integer>()
{
    return std::string{"INTEGER"};
}

template <>
std::string get_psql_type_name<PsqlType::BigInt>()
{
    return std::string{"BIGINT"};
}

template <>
std::string get_psql_type_name<PsqlType::Real>()
{
    return std::string{"REAL"};
}

template <>
std::string get_psql_type_name<PsqlType::DoublePrecision>()
{
    return std::string{"DOUBLE PRECISION"};
}

template <>
std::string get_psql_type_name<PsqlType::VarChar<-1>>()
{
    return std::string{"VARCHAR"};
}

template <>
std::string get_psql_type_name<PsqlType::Text>()
{
    return std::string{"TEXT"};
}

template <>
std::string get_psql_type_name<PsqlType::Boolean>()
{
    return std::string{"BOOLEAN"};
}

template <>
std::string get_psql_type_name<PsqlType::TimestampWithTimeZone>()
{
    return std::string{"TIMESTAMP WITH TIME ZONE"};
}

template <>
std::string get_psql_type_name<PsqlType::Timestamp>()
{
    return std::string{"TIMESTAMP WITHOUT TIME ZONE"};
}

template <>
std::string get_psql_type_name<PsqlType::Date>()
{
    return std::string{"DATE"};
}

template <>
std::string get_psql_type_name<PsqlType::TimeWithTimeZone>()
{
    return std::string{"TIME WITH TIME ZONE"};
}

template <>
std::string get_psql_type_name<PsqlType::Time>()
{
    return std::string{"TIME WITHOUT TIME ZONE"};
}

}//namespace LibPg::Detail
}//namespace LibPg
