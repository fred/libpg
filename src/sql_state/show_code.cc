/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/sql_state/code.hh"

#include <iostream>
#include <string>

namespace LibPg {
namespace SqlState {

std::ostream& show_code(std::ostream& out, char c0, char c1)
{
    return out << std::string({c0, c1});
}

std::ostream& show_code(std::ostream& out, char c0, char c1, char s0, char s1, char s2)
{
    return out << std::string({c0, c1, s0, s1, s2});
}

}//namespace LibPg::SqlState
}//namespace LibPg

