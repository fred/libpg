/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/detail/libpq_layer_interface.hh"
#include "include/libpg/detail/libpq_layer_impl.hh"

namespace LibPg {
namespace Detail {

namespace {

LibPqLayerInterface* libpq_layer_impl = nullptr;

}//namespace LibPg::Detail::{anonymous}

LibPqLayerInterface* set_libpq_layer_impl(LibPqLayerInterface* new_impl)noexcept
{
    auto* const old_impl = libpq_layer_impl;
    libpq_layer_impl = new_impl;
    return old_impl;
}

const LibPqLayerInterface& libpq_layer()noexcept
{
    if (libpq_layer_impl != nullptr)
    {
        return *libpq_layer_impl;
    }
    static const LibPqLayerSimpleImplementation libpq_layer_impl{};
    return static_cast<const LibPqLayerInterface&>(libpq_layer_impl);
}

LibPqLayerInterface::~LibPqLayerInterface()
{ }

}//namespace LibPg::Detail
}//namespace LibPg
