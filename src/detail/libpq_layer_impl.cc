/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libpg/detail/libpq_layer_impl.hh"

namespace LibPg {
namespace Detail {

LibPqLayerSimpleImplementation::~LibPqLayerSimpleImplementation()
{ }

::PGconn* LibPqLayerSimpleImplementation::PQconnectdbParams(
        const char* const* keywords,
        const char* const* values,
        int expand_dbname)const
{
    return ::PQconnectdbParams(keywords, values, expand_dbname);
}

::ConnStatusType LibPqLayerSimpleImplementation::PQstatus(const ::PGconn* conn)const
{
    return ::PQstatus(conn);
}

::PQnoticeReceiver LibPqLayerSimpleImplementation::PQsetNoticeReceiver(::PGconn *conn, ::PQnoticeReceiver proc, void *arg)const
{
    return ::PQsetNoticeReceiver(conn, proc, arg);
}

::PQnoticeProcessor LibPqLayerSimpleImplementation::PQsetNoticeProcessor(::PGconn *conn, ::PQnoticeProcessor proc, void *arg)const
{
    return ::PQsetNoticeProcessor(conn, proc, arg);
}

::PGresult* LibPqLayerSimpleImplementation::PQexec(::PGconn* conn, const char* query)const
{
    return ::PQexec(conn, query);
}

::PGresult* LibPqLayerSimpleImplementation::PQexecParams(
        ::PGconn* conn,
        const char* command,
        int nParams,
        const ::Oid* paramTypes,
        const char* const* paramValues,
        const int* paramLengths,
        const int* paramFormats,
        int resultFormat)const
{
    return ::PQexecParams(conn, command, nParams, paramTypes, paramValues, paramLengths, paramFormats, resultFormat);
}

void LibPqLayerSimpleImplementation::PQfinish(::PGconn* conn)const
{
    ::PQfinish(conn);
}

char* LibPqLayerSimpleImplementation::PQresultErrorField(const ::PGresult* res, int fieldcode)const
{
    return ::PQresultErrorField(res, fieldcode);
}

char* LibPqLayerSimpleImplementation::PQresStatus(::ExecStatusType status)const
{
    return ::PQresStatus(status);
}

void LibPqLayerSimpleImplementation::PQclear(::PGresult* res)const
{
    return ::PQclear(res);
}

::ExecStatusType LibPqLayerSimpleImplementation::PQresultStatus(const ::PGresult* res)const
{
    return ::PQresultStatus(res);
}

int LibPqLayerSimpleImplementation::PQntuples(const ::PGresult* res)const
{
    return ::PQntuples(res);
}

int LibPqLayerSimpleImplementation::PQnfields(const ::PGresult* res)const
{
    return  ::PQnfields(res);
}

int LibPqLayerSimpleImplementation::PQgetisnull(const ::PGresult* res, int tup_num, int field_num)const
{
    return ::PQgetisnull(res, tup_num, field_num);
}

int LibPqLayerSimpleImplementation::PQfnumber(const ::PGresult* res, const char* field_name)const
{
    return ::PQfnumber(res, field_name);
}

char* LibPqLayerSimpleImplementation::PQgetvalue(const ::PGresult* res, int tup_num, int field_num)const
{
    return ::PQgetvalue(res, tup_num, field_num);
}

int LibPqLayerSimpleImplementation::PQgetlength(const ::PGresult* res, int tup_num, int field_num)const
{
    return ::PQgetlength(res, tup_num, field_num);
}

char* LibPqLayerSimpleImplementation::PQfname(const ::PGresult* res, int field_num)const
{
    return ::PQfname(res, field_num);
}

char* LibPqLayerSimpleImplementation::PQcmdTuples(::PGresult* res)const
{
    return ::PQcmdTuples(res);
}

}//namespace LibPg::Detail
}//namespace LibPg
