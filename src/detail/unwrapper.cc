/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libpg/detail/unwrapper.hh"
#include "include/libpg/pg_exception.hh"

namespace LibPg {
namespace Detail {

void not_null_value_required(const char* value)
{
    if (value == nullptr)
    {
        struct ValueIsNull : PgException
        {
            const char* what()const noexcept override
            {
                return "value is NULL";
            }
        };
        throw ValueIsNull{};
    }
}

void throw_value_is_not_valid()
{
    struct ValueIsNotValid : PgException
    {
        const char* what()const noexcept override
        {
            return "src is not valid value of given arithmetic type";
        }
    };
    throw ValueIsNotValid{};
}

bool unwrap_nullable_string_into_bool(const char* src)
{
    not_null_value_required(src);
    static const auto is_properly_finished = [](const char* str) { return str[1] == '\0'; };
    switch (src[0])
    {
        case 'f':
            if (is_properly_finished(src))
            {
                return false;
            }
            break;
        case 't':
            if (is_properly_finished(src))
            {
                return true;
            }
            break;
    }
    struct ValueIsNotValid : PgException
    {
        const char* what()const noexcept override
        {
            return "src is not valid boolean value";
        }
    };
    throw ValueIsNotValid{};
}

std::string unwrap_nullable_string_into_string(const char* src)
{
    not_null_value_required(src);
    return std::string{src};
}

boost::gregorian::date unwrap_nullable_string_into_boost_date(const char* src)
{
    not_null_value_required(src);
    try
    {
        return boost::gregorian::from_string(std::string{src});
    }
    catch (const std::out_of_range&)
    {
        struct DateOutOfRange : PgException
        {
            const char* what()const noexcept override
            {
                return "date is out of range";
            }
        };
        throw DateOutOfRange{};
    }
    catch (...)
    {
        struct InvalidDate : PgException
        {
            const char* what()const noexcept override
            {
                return "invalid date";
            }
        };
        throw InvalidDate{};
    }
}

boost::posix_time::time_duration unwrap_nullable_string_into_boost_time_duration(const char* src)
{
    not_null_value_required(src);
    try
    {
        const std::string duration{src};
        static const auto is_sign = [](const std::string& str, int idx)
        {
            const char c = str[static_cast<std::size_t>(static_cast<int>(str.length()) + idx)];
            return (c == '+') || (c == '-');
        };
        static const auto is_cipher = [](const std::string& str, int idx)
        {
            const char c = str[static_cast<std::size_t>(static_cast<int>(str.length()) + idx)];
            return ('0' <= c) && (c <= '9');
        };
        if ((3u < duration.size()) && is_sign(duration, -3) && is_cipher(duration, -2) && is_cipher(duration, -1))
        {
            return boost::posix_time::duration_from_string(duration.substr(0, duration.length() - 3));
        }
        return boost::posix_time::duration_from_string(duration);
    }
    catch (const std::out_of_range&)
    {
        struct DurationOutOfRange : PgException
        {
            const char* what()const noexcept override
            {
                return "duration is out of range";
            }
        };
        throw DurationOutOfRange{};
    }
    catch (...)
    {
        struct InvalidDuration : PgException
        {
            const char* what()const noexcept override
            {
                return "invalid duration";
            }
        };
        throw InvalidDuration{};
    }
}

boost::posix_time::ptime unwrap_nullable_string_into_boost_ptime(const char* src)
{
    not_null_value_required(src);
    try
    {
        return boost::posix_time::time_from_string(std::string{src});
    }
    catch (const std::out_of_range&)
    {
        struct TimeOutOfRange : PgException
        {
            const char* what()const noexcept override
            {
                return "time is out of range";
            }
        };
        throw TimeOutOfRange{};
    }
    catch (...)
    {
        struct InvalidTime : PgException
        {
            const char* what()const noexcept override
            {
                return "invalid time";
            }
        };
        throw InvalidTime{};
    }
}

}//namespace LibPg::Detail
}//namespace LibPg
