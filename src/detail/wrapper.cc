/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libpg/detail/wrapper.hh"
#include "include/libpg/pg_exception.hh"
#include "include/libpg/time_zone.hh"

#include <iomanip>
#include <sstream>

namespace LibPg {
namespace Detail {

void not_null_string_required(const char* str)
{
    if (str == nullptr)
    {
        struct NullString : PgException
        {
            const char* what()const noexcept override
            {
                return "null string not allowed";
            }
        };
        throw NullString{};
    }
}
boost::optional<std::string> wrap_bool_into_psql_representation(bool is_true)
{
    return boost::optional<std::string>{std::string{is_true ? "t" : "f"}};
}

boost::optional<std::string> wrap_null_value_into_psql_representation()
{
    return boost::optional<std::string>{boost::none};
}

boost::optional<std::string> wrap_time_zone_into_psql_representation(const TimeZone& time_zone)
{
    return boost::optional<std::string>{to_psql(time_zone)};
}

boost::optional<std::string> wrap_boost_date_into_psql_representation(const boost::gregorian::date& date)
{
    const auto ymd = date.year_month_day();
    std::ostringstream out;
    out << ymd.year << "-"
        << std::setw(2) << std::setfill('0') << ymd.month.as_number() << "-"
        << std::setw(2) << std::setfill('0') << ymd.day.as_number();
    return boost::optional<std::string>{std::move(out).str()};
}

boost::optional<std::string> wrap_boost_ptime_into_psql_representation(const boost::posix_time::ptime& time)
{
    const auto date_part = wrap_boost_date_into_psql_representation(time.date());
    const auto time_of_day = time.time_of_day();
    std::ostringstream out;
    out << std::setw(2) << std::setfill('0') << time_of_day.hours() << ":"
        << std::setw(2) << std::setfill('0') << time_of_day.minutes() << ":"
        << std::setw(2) << std::setfill('0') << time_of_day.seconds() << "."
        << std::setw(time_of_day.num_fractional_digits()) << std::setfill('0') << time_of_day.fractional_seconds();
    return boost::optional<std::string>{*date_part + " " + out.str()};
}

boost::optional<std::string> wrap_boost_time_duration_into_psql_representation(const boost::posix_time::time_duration& duration)
{
    std::ostringstream out;
    out << std::setw(2) << std::setfill('0') << duration.hours() << ":"
        << std::setw(2) << std::setfill('0') << duration.minutes() << ":"
        << std::setw(2) << std::setfill('0') << duration.seconds() << "."
        << std::setw(duration.num_fractional_digits()) << std::setfill('0') << duration.fractional_seconds();
    return boost::optional<std::string>{std::move(out).str()};
}

}//namespace LibPg::Detail
}//namespace LibPg
