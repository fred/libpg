/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CODE_HH_4DDD69C9C562980A924BB767404A4125//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CODE_HH_4DDD69C9C562980A924BB767404A4125

#include "libpg/pg_connection.hh"

#include <iosfwd>

namespace LibPg {
namespace SqlState {

std::ostream& show_code(std::ostream&, char, char);
std::ostream& show_code(std::ostream&, char, char, char, char, char);

template <char, char> struct ClassPart;

template <char, char, char> struct SubclassPart;

template <typename, typename = void> struct Code;

template <char c0, char c1, char s0, char s1, char s2>
struct Code<ClassPart<c0, c1>, SubclassPart<s0, s1, s2>>
{
    bool operator==(const PgConnection::ExecResult::ErrorCode& error_code)const noexcept
    {
        return error_code.template has_class_part<c0, c1>() &&
               error_code.template has_subclass_part<s0, s1, s2>();
    }
    friend std::ostream& operator<<(std::ostream& out, Code)
    {
        return show_code(out, c0, c1, s0, s1, s2);
    }
};

template <char c0, char c1>
struct Code<ClassPart<c0, c1>>
{
    template <char s0, char s1, char s2>
    static constexpr auto subclass()noexcept
    {
        return Code<ClassPart<c0, c1>, SubclassPart<s0, s1, s2>>{};
    }
    static constexpr auto no_subclass = subclass<'0', '0', '0'>();

    bool operator==(const PgConnection::ExecResult::ErrorCode& error_code)const noexcept
    {
        return error_code.template has_class_part<c0, c1>();
    }
    friend std::ostream& operator<<(std::ostream& out, Code)
    {
        return show_code(out, c0, c1);
    }
};

template <typename Cls, typename Subcls>
bool operator==(const PgConnection::ExecResult::ErrorCode& error_code, Code<Cls, Subcls> sql_state)noexcept
{
    return sql_state == error_code;
}

template <typename Cls, typename Subcls>
bool operator!=(Code<Cls, Subcls> sql_state, const PgConnection::ExecResult::ErrorCode& error_code)noexcept
{
    return !(sql_state == error_code);
}

template <typename Cls, typename Subcls>
bool operator!=(const PgConnection::ExecResult::ErrorCode& error_code, Code<Cls, Subcls> sql_state)noexcept
{
    return sql_state != error_code;
}

template <char c0, char c1>
constexpr auto make_sql_state_class()noexcept
{
    return Code<ClassPart<c0, c1>>{};
}

template <char c0, char c1, char s0, char s1, char s2>
constexpr auto make_sql_state_code()noexcept
{
    make_sql_state_class<c0, c1>().template subclass<s0, s1, s2>();
}

constexpr auto connection_exception_class = make_sql_state_class<'0', '8'>();
constexpr auto connection_exception = connection_exception_class.no_subclass;
constexpr auto connection_does_not_exist = connection_exception_class.subclass<'0', '0', '3'>();
constexpr auto protocol_violation = connection_exception_class.subclass<'P', '0', '1'>();

constexpr auto data_exception_class = make_sql_state_class<'2', '2'>();
constexpr auto data_exception = data_exception_class.no_subclass;
constexpr auto string_data_right_truncation = data_exception_class.subclass<'0', '0', '1'>();
constexpr auto invalid_parameter_value = data_exception_class.subclass<'0', '2', '3'>();

constexpr auto integrity_constraint_violation_class = make_sql_state_class<'2', '3'>();
constexpr auto integrity_constraint_violation = integrity_constraint_violation_class.no_subclass;
constexpr auto not_null_violation = integrity_constraint_violation_class.subclass<'5', '0', '2'>();
constexpr auto foreign_key_violation = integrity_constraint_violation_class.subclass<'5', '0', '3'>();
constexpr auto unique_violation = integrity_constraint_violation_class.subclass<'5', '0', '5'>();

constexpr auto invalid_transaction_state_class = make_sql_state_class<'2', '5'>();
constexpr auto invalid_transaction_state = invalid_transaction_state_class.no_subclass;
constexpr auto read_only_sql_transaction = invalid_transaction_state_class.subclass<'0', '0', '6'>();

constexpr auto syntax_error_or_access_rule_violation_class = make_sql_state_class<'4', '2'>();
constexpr auto syntax_error_or_access_rule_violation = syntax_error_or_access_rule_violation_class.no_subclass;
constexpr auto syntax_error = syntax_error_or_access_rule_violation_class.subclass<'6', '0', '1'>();
constexpr auto undefined_object = syntax_error_or_access_rule_violation_class.subclass<'7', '0', '4'>();
constexpr auto undefined_table = syntax_error_or_access_rule_violation_class.subclass<'P', '0', '1'>();

}//namespace LibPg::SqlState
}//namespace LibPg

#endif//CODE_HH_4DDD69C9C562980A924BB767404A4125
