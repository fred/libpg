/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DSN_HH_CF20D56C0327ACE9FDE397F337629849//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DSN_HH_CF20D56C0327ACE9FDE397F337629849

#include "libpg/strong_types.hh"

#include <boost/asio/ip/address.hpp>

#include <chrono>
#include <cstdint>
#include <string>

namespace LibPg {

// DSN = Data Source Name
struct Dsn
{
    using Host = StrongString<struct DsnHostTag_>;
    using HostAddr = StrongComparable<boost::asio::ip::address, struct DsnHostAddrTag_>;
    using Port = StrongCountableStreamable<std::uint16_t, struct DsnPortTag_>;
    using DbName = StrongString<struct DsnDbNameTag_>;
    using User = StrongString<struct DsnUserTag_>;
    using Password = StrongString<struct DsnPasswordTag_>;
    using ConnectTimeout = StrongComparable<std::chrono::seconds, struct DsnConnectTimeoutTag_>;

    Host::Optional host;
    HostAddr::Optional host_addr;
    Port::Optional port;
    DbName::Optional db_name;
    User::Optional user;
    Password::Optional password;
    ConnectTimeout::Optional connect_timeout;
};

}//namespace LibPg

#endif//DSN_HH_CF20D56C0327ACE9FDE397F337629849
