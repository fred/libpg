/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PG_CONNECTION_HH_2CF56419AEFB2AAA1C2FC200F20BD333//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PG_CONNECTION_HH_2CF56419AEFB2AAA1C2FC200F20BD333

#include "libpg/dsn.hh"
#include "libpg/extendable_query_arguments.hh"
#include "libpg/fixed_size_query_arguments.hh"
#include "libpg/named_query_arguments.hh"

#include <array>
#include <iosfwd>
#include <memory>
#include <string>

namespace LibPg {

class PgConnection;
class PgResult;

struct AliasForPGconn;
struct AliasForPGresult;

namespace Unsafe {
namespace Dirty {
namespace Hack {

struct AliasForPGconn;

// Pointer ownership is still in a PgConnection possession
AliasForPGconn* get_raw_pointer(const PgConnection&);

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

class PgConnection
{
public:
    explicit PgConnection(const Dsn&);
    PgConnection(PgConnection&&);
    PgConnection& operator=(PgConnection&&);
    ~PgConnection();

    PgConnection() = delete;
    PgConnection(const PgConnection&) = delete;
    PgConnection& operator=(const PgConnection&) = delete;

    class ExecResult;

    const PgConnection& get_useful_connection()const;
private:
    template <typename ...> struct Exec;
    template <typename T> struct Exec<QueryArguments<T>>;
    ExecResult exec(const std::string& query, const char* const* arguments, int number_of_arguments)const;
    struct PGconnDeleter
    {
        void operator()(AliasForPGconn*)const;
    };
    std::unique_ptr<AliasForPGconn, PGconnDeleter> conn_;
    friend ExecResult exec(const PgConnection& conn, const std::string& query);
    template <typename ...Types>
    friend ExecResult exec(const PgConnection& conn, const std::string& query, Types&& ...arguments);
    friend Unsafe::Dirty::Hack::AliasForPGconn* Unsafe::Dirty::Hack::get_raw_pointer(const PgConnection&);
};

class PgConnection::ExecResult
{
public:
    ExecResult();
    ExecResult(ExecResult&&);
    ExecResult& operator=(ExecResult&&);
    ~ExecResult();

    bool is_tuples()const;

    ExecResult(const ExecResult&) = delete;
    ExecResult& operator=(const ExecResult&) = delete;

    using SizeType = int;
    class ErrorCode;
private:
    explicit ExecResult(AliasForPGresult*);

    void usability_required()const;
    ErrorCode make_error_code()const;
    void strict_usability_required()const;
    void tuples_ok_required()const;
    void command_ok_required()const;

    bool empty()const noexcept;
    SizeType get_number_of_rows()const noexcept;
    SizeType get_number_of_columns()const noexcept;
    bool is_null(SizeType row_idx, SizeType column_idx)const;
    std::string get_not_null_value(SizeType row_idx, SizeType column_idx)const;
    SizeType get_column_idx(const char* column_name)const;
    std::string get_column_name(SizeType column_idx)const;
    SizeType get_number_of_rows_affected()const;

    struct PGresultDeleter
    {
        void operator()(AliasForPGresult*)const noexcept;
    };
    std::unique_ptr<AliasForPGresult, PGresultDeleter> result_;
    friend class PgConnection;
    friend class PgResult;
};

template <typename ...Types>
PgConnection::ExecResult exec(const PgConnection& conn, const std::string& query, Types&& ...arguments)
{
    return PgConnection::Exec<std::decay_t<Types>...>{}(conn, query, std::forward<Types>(arguments)...);
}

template <typename ...>
struct PgConnection::Exec
{
    template <typename ...Args>
    ExecResult operator()(const PgConnection& conn, const std::string& query, Args&& ...args)const
    {
        const auto collected_arguments = make_fixed_size_query_arguments(std::forward<Args>(args)...);
        return Exec<FixedSizeQueryArguments<sizeof...(Args)>>{}(conn, query, collected_arguments);
    }
};

template <typename T>
struct PgConnection::Exec<QueryArguments<T>>
{
    PgConnection::ExecResult operator()(const PgConnection& conn, const std::string& query, const QueryArguments<T>& arguments)const
    {
        return conn.exec(query, arguments.get_arguments().data(), arguments.size());
    }
};

class PgConnection::ExecResult::ErrorCode
{
public:
    template <char c0, char c1>
    bool has_class_part()const noexcept
    {
        return (major_[0] == c0) &&
               (major_[1] == c1);
    }
    template <char s0, char s1, char s2>
    bool has_subclass_part()const noexcept
    {
        return (minor_[0] == s0) &&
               (minor_[1] == s1) &&
               (minor_[2] == s2);
    }
private:
    explicit ErrorCode(const char* error_code)noexcept;
    using Major = std::array<char, 2>;
    using Minor = std::array<char, 3>;
    const Major major_;
    const Minor minor_;
    friend class ExecResult;
    friend std::ostream& operator<<(std::ostream& out, const ErrorCode& error_code);
};

}//namespace LibPg

#endif//PG_CONNECTION_HH_2CF56419AEFB2AAA1C2FC200F20BD333
