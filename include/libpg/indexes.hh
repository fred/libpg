/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INDEXES_HH_E885FBFB2D6A2C3B1F6F46B29E7F3E4E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define INDEXES_HH_E885FBFB2D6A2C3B1F6F46B29E7F3E4E

#include "libpg/pg_connection.hh"
#include "libpg/strong_types.hh"

namespace LibPg {

using RowIndex = StrongCountableStreamable<PgConnection::ExecResult::SizeType, struct RowIndexTag_>;
using ColumnIndex = StrongCountableStreamable<PgConnection::ExecResult::SizeType, struct ColumnIndexTag_>;

}//namespace LibPg

#endif//INDEXES_HH_E885FBFB2D6A2C3B1F6F46B29E7F3E4E
