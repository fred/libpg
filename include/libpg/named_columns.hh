/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NAMED_COLUMNS_HH_25C627FB2EE93D4602AACFC0BAF920C7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define NAMED_COLUMNS_HH_25C627FB2EE93D4602AACFC0BAF920C7

#include "libpg/detail/has_only_unique_elements.hh"

#include "libstrong/type.hh"

#include <boost/optional.hpp>

#include <tuple>
#include <type_traits>
#include <utility>

namespace LibPg {

class PgResultTuples;

/**
 * Join columns types with names/tags. Order of types must correspond with order of columns in a row.
 *
 * @tparam UniqueTypes list of unique types; position in the list corresponds with a column in a result
 * @note How to use: SELECT a, b, c; => NamedColumns<StrongType<int, struct A>, StrongType<int, struct B>, StrongType<int, struct C>>
 **/
template <typename ...UniqueTypes>
class NamedColumns
{
private:
    using UniqueTypesList = std::tuple<UniqueTypes...>;
public:
    template <typename Item,
              std::enable_if_t<Detail::IsElement<Item, UniqueTypesList>::value>* = nullptr>
    constexpr auto& get()& noexcept
    {
        return std::get<column_index_of<Item>>(storage_);
    }
    template <typename Item,
              std::enable_if_t<Detail::IsElement<Item, UniqueTypesList>::value>* = nullptr>
    constexpr const auto& get()const& noexcept
    {
        return std::get<column_index_of<Item>>(storage_);
    }
    template <typename Item,
              std::enable_if_t<Detail::IsElement<Item, UniqueTypesList>::value>* = nullptr>
    constexpr auto&& get()&& noexcept
    {
        return std::get<column_index_of<Item>>(std::move(storage_));
    }
    template <typename Item,
              std::enable_if_t<Detail::IsElement<Item, UniqueTypesList>::value>* = nullptr>
    constexpr const auto&& get()const&& noexcept
    {
        return std::get<column_index_of<Item>>(std::move(storage_));
    }

    template <typename Item,
              std::enable_if_t<Detail::IsElement<boost::optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr bool is_null()const noexcept
    {
        return this->get<boost::optional<Item>>() == boost::none;
    }

    template <typename Item,
              std::enable_if_t<Detail::IsElement<boost::optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr auto& get_nullable()&
    {
        return *(this->get<boost::optional<Item>>());
    }
    template <typename Item,
             std::enable_if_t<Detail::IsElement<boost::optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr const auto& get_nullable()const&
    {
        return *(this->get<boost::optional<Item>>());
    }
    template <typename Item,
             std::enable_if_t<Detail::IsElement<boost::optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr auto&& get_nullable()&&
    {
        return *(this->get<boost::optional<Item>>());
    }
    template <typename Item,
             std::enable_if_t<Detail::IsElement<boost::optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr const auto&& get_nullable()const&&
    {
        return *(this->get<boost::optional<Item>>());
    }

    template <typename Item>
    constexpr std::enable_if_t<Detail::IsElement<LibStrong::Optional<Item>, UniqueTypesList>::value, bool> is_null()const noexcept
    {
        return this->get<LibStrong::Optional<Item>>() == Item::nullopt;
    }

    template <typename Item,
              std::enable_if_t<Detail::IsElement<LibStrong::Optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr auto& get_nullable() &
    {
        return *(this->get<LibStrong::Optional<Item>>());
    }
    template <typename Item,
             std::enable_if_t<Detail::IsElement<LibStrong::Optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr const auto& get_nullable() const&
    {
        return *(this->get<LibStrong::Optional<Item>>());
    }
    template <typename Item,
             std::enable_if_t<Detail::IsElement<LibStrong::Optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr auto&& get_nullable() &&
    {
        return *(this->get<LibStrong::Optional<Item>>());
    }
    template <typename Item,
             std::enable_if_t<Detail::IsElement<LibStrong::Optional<Item>, UniqueTypesList>::value>* = nullptr>
    constexpr const auto&& get_nullable() const&&
    {
        return *(this->get<LibStrong::Optional<Item>>());
    }
private:
    template <int index>
    using ColumnByIndex = std::tuple_element_t<index, UniqueTypesList>;
    template <typename What, typename ...Where> struct IndexOfType;
    template <typename What, typename ...Where>
    struct IndexOfType<What, What, Where...>
        : std::integral_constant<int, 0> { };
    template <typename What, typename Head, typename ...Tail>
    struct IndexOfType<What, Head, Tail...>
        : std::integral_constant<int, 1 + IndexOfType<What, Tail...>::value> { };
    template <typename Item>
    static constexpr int column_index_of = IndexOfType<Item, UniqueTypes...>::value;
    static_assert(Detail::HasOnlyUniqueElements<UniqueTypes...>::value, "all types must be unique in this sequence");
    template <typename ...Types,
              std::enable_if_t<std::is_constructible<UniqueTypesList, Types...>::value>* = nullptr>
    constexpr explicit NamedColumns(Types&& ...values)
        : storage_{std::forward<Types>(values)...}
    { }
    UniqueTypesList storage_;
    friend class PgResultTuples;
};

}//namespace LibPg

#endif//NAMED_COLUMNS_HH_25C627FB2EE93D4602AACFC0BAF920C7
