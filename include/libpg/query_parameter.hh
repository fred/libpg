/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef QUERY_PARAMETER_HH_C1A54AD910673FEFC84E9CE026A30DD5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define QUERY_PARAMETER_HH_C1A54AD910673FEFC84E9CE026A30DD5

#include "libpg/psql_type.hh"

#include <string>

#if !defined(__clang__) && (__GNUC__ <= 5)
    #define FRIEND_CONSTEXPR friend // fix gcc 5.x bug https://gcc.gnu.org/bugzilla/show_bug.cgi?id=65977
#else
    #define FRIEND_CONSTEXPR friend constexpr
#endif

namespace LibPg {

class QueryParameterRuntimeIndex;
template <int> class QueryParameterCompiletimeIndex;
class QueryParameterRuntimeType;
template <typename = void> class QueryParameterCompiletimeType;
using QueryParameterUntyped = QueryParameterCompiletimeType<>;

/**
 * Join position and type of query parameter
 * @tparam Index parameter position indexed from 0
 * @tparam Type psql type of parameter
 * @note meaning of QueryParameter<QueryParameterCompiletimeIndex<3>, QueryParameterCompiletimeType<PsqlType::Integer>> is "$4::INTEGER"
 **/
template <typename Index = QueryParameterRuntimeIndex, typename Type = QueryParameterUntyped>
class QueryParameter;

template <typename I, typename T>
std::string operator+(const std::string&, const QueryParameter<I, T>&);

class QueryParameterRuntimeIndex
{
public:
    QueryParameterRuntimeIndex() = delete;
    explicit constexpr QueryParameterRuntimeIndex(int index)
        : index_{index}
    { }
private:
    int index_;
    std::string as_counted_parameter()const;
    template <typename, typename>
    friend class QueryParameter;
};

template <int index>
class QueryParameterCompiletimeIndex
{
private:
    static std::string as_counted_parameter()
    {
        return "$" + std::to_string(index + 1);
    }
    template <typename, typename>
    friend class QueryParameter;
};

class QueryParameterRuntimeType
{
public:
    QueryParameterRuntimeType() = delete;
    explicit QueryParameterRuntimeType(std::string type);
private:
    std::string as_type_suffix()const;
    std::string type_;
    template <typename, typename>
    friend class QueryParameter;
};

template <typename Type>
class QueryParameterCompiletimeType
{
private:
    std::string as_type_suffix()const
    {
        return "::" + Detail::get_psql_type_name<Type>();
    }
    template <typename, typename>
    friend class QueryParameter;
};

template <>
class QueryParameterCompiletimeType<void>
{
private:
    static std::string as_type_suffix();
    template <typename, typename>
    friend class QueryParameter;
};

template <typename IndexPart, typename TypePart>
class QueryParameter
{
public:
    template <typename NewIndexPart>
    decltype(auto) at_position(NewIndexPart&& index)const
    {
        return QueryParameter<std::decay_t<NewIndexPart>, TypePart>{std::forward<NewIndexPart>(index), type_};
    }
private:
    constexpr QueryParameter(IndexPart index, TypePart type)
        : index_{std::move(index)},
          type_{std::move(type)}
    { }
    std::string as_counted_parameter()const
    {
        return index_.as_counted_parameter();
    }
    std::string as_type_suffix()const
    {
        return type_.as_type_suffix();
    }
    IndexPart index_;
    TypePart type_;
    template <typename, typename>
    friend class QueryParameter;
    friend std::string operator+<>(const std::string&, const QueryParameter&);
};

template <typename IndexPart>
class QueryParameter<IndexPart, QueryParameterRuntimeType>
{
public:
    template <typename NewIndexPart>
    decltype(auto) at_position(NewIndexPart&& index)const
    {
        return QueryParameter<std::decay_t<NewIndexPart>, QueryParameterRuntimeType>{std::forward<NewIndexPart>(index), type_};
    }
private:
    QueryParameter(IndexPart index, QueryParameterRuntimeType type)
        : index_{std::move(index)},
          type_{std::move(type)}
    { }
    std::string as_counted_parameter()const
    {
        return index_.as_counted_parameter();
    }
    std::string as_type_suffix()const
    {
        return type_.as_type_suffix();
    }
    IndexPart index_;
    QueryParameterRuntimeType type_;
    template <typename, typename>
    friend class QueryParameter;
    friend std::string operator+<>(const std::string&, const QueryParameter&);
};

template <typename IndexPart>
constexpr decltype(auto) make_query_parameter(IndexPart);

template <typename IndexPart>
class QueryParameter<IndexPart, QueryParameterUntyped>
{
public:
    decltype(auto) as(const QueryParameterRuntimeType& type)const
    {
        return QueryParameter<IndexPart, QueryParameterRuntimeType>{index_, type};
    }
    template <typename Type>
    constexpr decltype(auto) as()const
    {
        return QueryParameter<IndexPart, QueryParameterCompiletimeType<Type>>{index_, QueryParameterCompiletimeType<Type>{}};
    }
    decltype(auto) as_type(std::string type)const
    {
        return QueryParameter<IndexPart, QueryParameterRuntimeType>{index_, QueryParameterRuntimeType{std::move(type)}};
    }
    constexpr decltype(auto) as_small_int()const
    {
        return this->as<PsqlType::SmallInt>();
    }
    constexpr decltype(auto) as_integer()const
    {
        return this->as<PsqlType::Integer>();
    }
    constexpr decltype(auto) as_big_int()const
    {
        return this->as<PsqlType::BigInt>();
    }
    template <int precision, int scale = 0>
    constexpr decltype(auto) as_numeric()const
    {
        return this->as<PsqlType::Numeric<precision, scale>>();
    }
    template <int precision, int scale = 0>
    constexpr decltype(auto) as_decimal()const
    {
        return this->as<PsqlType::Decimal<precision, scale>>();
    }
    constexpr decltype(auto) as_real()const
    {
        return this->as<PsqlType::Real>();
    }
    constexpr decltype(auto) as_double_precision()const
    {
        return this->as<PsqlType::DoublePrecision>();
    }

    template <int fixed_size = 1>
    constexpr decltype(auto) as_char()const
    {
        return this->as<PsqlType::Char<fixed_size>>();
    }
    template <int max_size = -1>
    constexpr decltype(auto) as_varchar()const
    {
        return this->as<PsqlType::VarChar<max_size>>();
    }
    constexpr decltype(auto) as_text()const
    {
        return this->as<PsqlType::Text>();
    }

    constexpr decltype(auto) as_boolean()const
    {
        return this->as<PsqlType::Boolean>();
    }

    constexpr decltype(auto) as_timestamp()const
    {
        return this->as<PsqlType::Timestamp>();
    }
    constexpr decltype(auto) as_timestamp_without_time_zone()const
    {
        return this->as<PsqlType::TimestampWithoutTimeZone>();
    }
    constexpr decltype(auto) as_timestamp_with_time_zone()const
    {
        return this->as<PsqlType::TimestampWithTimeZone>();
    }
    constexpr decltype(auto) as_date()const
    {
        return this->as<PsqlType::Date>();
    }
    constexpr decltype(auto) as_time()const
    {
        return this->as<PsqlType::Time>();
    }
    constexpr decltype(auto) as_time_without_time_zone()const
    {
        return this->as<PsqlType::TimeWithoutTimeZone>();
    }
    constexpr decltype(auto) as_time_with_time_zone()const
    {
        return this->as<PsqlType::TimeWithTimeZone>();
    }

    template <typename NewIndexPart>
    decltype(auto) at_position(NewIndexPart&& index)const
    {
        return QueryParameter<std::decay_t<NewIndexPart>, QueryParameterUntyped>{std::forward<NewIndexPart>(index), type_};
    }
private:
    constexpr QueryParameter(IndexPart index, QueryParameterUntyped type)
        : index_{std::move(index)},
          type_{std::move(type)}
    { }
    std::string as_counted_parameter()const
    {
        return index_.as_counted_parameter();
    }
    std::string as_type_suffix()const
    {
        return type_.as_type_suffix();
    }
    IndexPart index_;
    QueryParameterUntyped type_;
    template <typename, typename>
    friend class QueryParameter;
    friend std::string operator+<>(const std::string&, const QueryParameter&);
    FRIEND_CONSTEXPR decltype(auto) make_query_parameter<>(IndexPart);
};

template <typename Index>
constexpr decltype(auto) make_query_parameter(Index index_part)
{
    return QueryParameter<Index, QueryParameterUntyped>{std::move(index_part), QueryParameterUntyped{}};
}

template <int value>
using QueryParameterAtPosition = QueryParameter<QueryParameterCompiletimeIndex<value>>;

constexpr decltype(auto) query_parameter_at_position(int value)
{
    return make_query_parameter(QueryParameterRuntimeIndex{value});
}

template <int value>
constexpr decltype(auto) query_parameter_at_position()
{
    return make_query_parameter(QueryParameterCompiletimeIndex<value>{});
}

template <typename I, typename T>
std::string operator+(const std::string& str, const QueryParameter<I, T>& parameter)
{
    return str + parameter.as_counted_parameter() + parameter.as_type_suffix();
}

template <typename I, typename T>
std::string operator+(const char* str, const QueryParameter<I, T>& parameter)
{
    return std::string{str} + parameter;
}

template <typename I, typename T>
std::string operator+(const QueryParameter<I, T>& parameter, const std::string& str)
{
    return (std::string{} + parameter) + str;
}

template <typename I, typename T>
std::string operator+(const QueryParameter<I, T>& parameter, const char* str)
{
    return parameter + std::string{str};
}

}//namespace LibPg

#endif//QUERY_PARAMETER_HH_C1A54AD910673FEFC84E9CE026A30DD5
