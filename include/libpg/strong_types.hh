/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRONG_TYPES_HH_9B19865CA5373B07D5F482DEDD19BCF6//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define STRONG_TYPES_HH_9B19865CA5373B07D5F482DEDD19BCF6

#include "libstrong/type.hh"

#include <string>

namespace LibPg {

template <typename T, typename Tag>
using StrongCountableStreamable = LibStrong::TypeWithSkills<T, Tag, LibStrong::Skill::Countable, LibStrong::Skill::Streamable>;

template <typename T, typename Tag>
using StrongComparable = LibStrong::Type<T, Tag, LibStrong::Skill::Comparable>;

template <typename T, typename Tag>
using StrongStreamable = LibStrong::Type<T, Tag, LibStrong::Skill::Streamable>;

template <typename Tag>
using StrongString = LibStrong::TypeWithSkills<std::string, Tag, LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>;

}//namespace LibPg

#endif//STRONG_TYPES_HH_9B19865CA5373B07D5F482DEDD19BCF6
