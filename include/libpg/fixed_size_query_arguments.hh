/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef FIXED_SIZE_QUERY_ARGUMENTS_HH_D3B4544D10298B5983DF05FFEFC2750A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define FIXED_SIZE_QUERY_ARGUMENTS_HH_D3B4544D10298B5983DF05FFEFC2750A

#include "libpg/query_arguments.hh"
#include "libpg/query_parameter.hh"

#include <array>
#include <type_traits>
#include <utility>

namespace LibPg {

template <int> struct NumberOfArguments;

template <int cnt>
using FixedSizeQueryArguments = QueryArguments<NumberOfArguments<cnt>>;

template <int cnt>
class QueryArguments<NumberOfArguments<cnt>>
{
public:
    static constexpr auto number_of_arguments = cnt;

    template <typename ...Types,
              std::enable_if_t<(sizeof...(Types) == number_of_arguments) &&
                               !std::is_same<std::tuple<std::decay_t<Types>...>, std::tuple<QueryArguments>>::value>* = nullptr>//disable copy and move constructors
    explicit QueryArguments(Types&& ...values)
        : arguments_{make_one_query_argument_from_nullable(std::forward<Types>(values))...}
    {
        static_assert(sizeof...(Types) == number_of_arguments, "incorrect number of arguments");
    }

    constexpr bool empty()const noexcept
    {
        return number_of_arguments == 0;
    }
    constexpr int size()const noexcept
    {
        return number_of_arguments;
    }
    template <int idx,
              std::enable_if_t<(0 <= idx) && (idx < number_of_arguments)>* = nullptr>
    static constexpr decltype(auto) parameter()
    {
        static_assert((0 <= idx) && (idx < number_of_arguments), "idx out of range");
        return query_parameter_at_position<idx>();
    }
    template <typename Type>
    FixedSizeQueryArguments<number_of_arguments + 1> push_back(Type&& value)const&
    {
        return FixedSizeQueryArguments<number_of_arguments + 1>{ArrayTools::join(arguments_, make_one_query_argument_from_nullable(std::forward<Type>(value)))};
    }
    template <typename Type>
    FixedSizeQueryArguments<number_of_arguments + 1> push_back(Type&& value)&&
    {
        return FixedSizeQueryArguments<number_of_arguments + 1>{ArrayTools::join(std::move(arguments_), make_one_query_argument_from_nullable(std::forward<Type>(value)))};
    }
private:
    using NullableString = boost::optional<std::string>;
    using Arguments = std::array<NullableString, number_of_arguments>;
    explicit QueryArguments(std::array<NullableString, number_of_arguments>&& arguments)
        : arguments_{std::move(arguments)}
    { }
    template <typename> struct IndexTools;
    template <std::size_t ...indexes>
    struct IndexTools<std::integer_sequence<std::size_t, indexes...>>
    {
        static constexpr auto size = sizeof...(indexes);
        static_assert(size == number_of_arguments, "incorrect size");
        template <typename Type>
        static std::array<NullableString, size + 1> join(const Arguments& arguments, Type&& value)
        {
            return std::array<NullableString, size + 1>{arguments[indexes]..., std::forward<Type>(value)};
        }
        template <typename Type>
        static std::array<NullableString, size + 1> join(Arguments&& arguments, Type&& value)
        {
            return std::array<NullableString, size + 1>{std::move(arguments[indexes])..., std::forward<Type>(value)};
        }
        static std::array<const char*, size> to_psql_arguments(const Arguments& arguments)
        {
            static const auto get_psql_argument = [](const NullableString& argument)
            {
                return argument == boost::none ? nullptr
                                               : argument->c_str();
            };
            return std::array<const char*, size>{get_psql_argument(arguments[indexes])...};
        }
    };
    using ArrayTools = IndexTools<std::make_index_sequence<number_of_arguments>>;
    std::array<const char*, number_of_arguments> get_arguments()const
    {
        return ArrayTools::to_psql_arguments(arguments_);
    }
    Arguments arguments_;
    template <typename> friend class QueryArguments;
    template <typename ...Types>
    friend FixedSizeQueryArguments<sizeof...(Types)> make_fixed_size_query_arguments(Types&& ...args);
    friend class PgConnection;
};

template <>
class QueryArguments<NumberOfArguments<0>>
{
public:
    static constexpr auto number_of_arguments = 0;

    constexpr bool empty()const noexcept
    {
        return number_of_arguments == 0;
    }
    constexpr int size()const noexcept
    {
        return number_of_arguments;
    }
    template <typename Type>
    static FixedSizeQueryArguments<1> push_back(Type&& value)
    {
        return FixedSizeQueryArguments<1>{std::forward<Type>(value)};
    }
private:
    static constexpr std::array<const char*, 0> get_arguments()
    {
        return std::array<const char*, 0>{};
    }
    template <typename> friend class QueryArguments;
    friend class PgConnection;
};

template <typename ...Types>
FixedSizeQueryArguments<sizeof...(Types)> make_fixed_size_query_arguments(Types&& ...args)
{
    return FixedSizeQueryArguments<sizeof...(Types)>{std::forward<Types>(args)...};
}

}//namespace LibPg

#endif//FIXED_SIZE_QUERY_ARGUMENTS_HH_D3B4544D10298B5983DF05FFEFC2750A
