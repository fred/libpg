/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EXTENDABLE_QUERY_ARGUMENTS_HH_FA8868903AD5D40CA469FD4E0424053F//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define EXTENDABLE_QUERY_ARGUMENTS_HH_FA8868903AD5D40CA469FD4E0424053F

#include "libpg/query_arguments.hh"
#include "libpg/query_parameter.hh"

#include <vector>
#include <utility>

namespace LibPg {

using ExtendableQueryArguments = QueryArguments<>;

template <>
class QueryArguments<>
{
public:
    QueryArguments();
    template <typename ...Types>
    explicit QueryArguments(Types&& ...values)
        : arguments_{make_one_query_argument_from_nullable(std::forward<Types>(values))...}
    { }

    template <typename T>
    QueryArguments& push_back(T&& value)
    {
        arguments_.push_back(make_one_query_argument_from_nullable(std::forward<T>(value)));
        return *this;
    }
    template <typename ArgumentType>
    QueryParameter<> add(ArgumentType&& value)
    {
        this->push_back(std::forward<ArgumentType>(value));
        return this->parameter(this->size() - 1);
    }
    template <typename ParamType, typename ArgumentType>
    QueryParameter<> add_as(ArgumentType&& value)
    {
        this->push_back(std::forward<ArgumentType>(value));
        return this->parameter(this->size() - 1).as<ParamType>();
    }

    bool empty()const noexcept;
    int size()const noexcept;
    QueryParameter<> parameter(int idx)const;
private:
    std::vector<const char*> get_arguments()const;
    using NullableString = boost::optional<std::string>;
    std::vector<NullableString> arguments_;
    friend class PgConnection;
};

template <typename T>
QueryParameter<> push_back_argument(ExtendableQueryArguments& arguments, T&& new_argument)
{
    arguments.push_back(std::forward<T>(new_argument));
    return arguments.parameter(arguments.size() - 1);
}

}//namespace LibPg

#endif//EXTENDABLE_QUERY_ARGUMENTS_HH_FA8868903AD5D40CA469FD4E0424053F
