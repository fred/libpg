/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PG_RO_TRANSACTION_HH_49D1924B1F457C033E6AA95956A4C88B//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PG_RO_TRANSACTION_HH_49D1924B1F457C033E6AA95956A4C88B

#include "libpg/pg_transaction.hh"

#include <utility>

namespace LibPg {

class PgRoTransaction;

namespace Unsafe {
namespace Dirty {
namespace Hack {

// Pointer ownership is still in a PgRoTransaction possession
AliasForPGconn* get_raw_pointer(const PgRoTransaction&);

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

class PgRoTransaction
{
public:
    template <typename Level = PgTransaction::SessionDefault>
    explicit PgRoTransaction(const Dsn& dsn, PgTransaction::IsolationLevel<Level> level = PgTransaction::session_default)
        : PgRoTransaction{PgConnection{dsn}, level}
    { }
    template <typename Level = PgTransaction::SessionDefault>
    explicit PgRoTransaction(PgConnection conn, PgTransaction::IsolationLevel<Level> level = PgTransaction::session_default)
        : transaction_{std::move(conn), level, PgTransaction::read_only}
    { }
    PgRoTransaction(PgRoTransaction&& src);
    ~PgRoTransaction();

    PgRoTransaction() = delete;
    PgRoTransaction(const PgRoTransaction&) = delete;
    PgRoTransaction& operator=(const PgRoTransaction&) = delete;
    PgRoTransaction& operator=(PgRoTransaction&&) = delete;

    operator const PgConnection&()const;
    operator const PgTransaction&()const;

    template <typename Action>
    auto recoverable(Action&& do_something)const;
private:
    PgTransaction transaction_;
    PgConnection get_conn()&&;
    friend PgConnection commit(PgRoTransaction);
    friend PgConnection rollback(PgRoTransaction);
    friend Unsafe::Dirty::Hack::AliasForPGconn* Unsafe::Dirty::Hack::get_raw_pointer(const PgRoTransaction&);
};

template <typename Action>
auto PgRoTransaction::recoverable(Action&& do_something)const
{
    PgTransaction::Savepoint savepoint{*this};
    try
    {
        return std::forward<Action>(do_something)(*this);
    }
    catch (const ExecFailure&)
    {
        std::move(savepoint).recovery();
        throw;
    }
}

template <typename Q, typename ...Args>
decltype(auto) try_exec(const PgRoTransaction& transaction, Q&& query, Args&& ...args)
{
    return transaction.recoverable([&](const PgRoTransaction& transaction)
                                   {
                                       return exec(transaction, std::forward<Q>(query), std::forward<Args>(args)...);
                                   });
}

}//namespace LibPg

#endif//PG_RO_TRANSACTION_HH_49D1924B1F457C033E6AA95956A4C88B
