/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PREPARED_TRANSACTION_HH_7CEA42D401A56D465A06B0B85D0D6E68//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PREPARED_TRANSACTION_HH_7CEA42D401A56D465A06B0B85D0D6E68

#include "libpg/pg_connection.hh"
#include "libpg/pg_rw_transaction.hh"

#include <string>

namespace LibPg {

class PreparedTransactionId
{
public:
    explicit PreparedTransactionId(std::string prepared_transaction_id);
    PreparedTransactionId() = delete;
    const std::string& get()const;
private:
    std::string id_;
};

PgConnection prepare_transaction(PgRwTransaction transaction, const PreparedTransactionId& id);
void commit_prepared_transaction(const PgConnection& conn, const PreparedTransactionId& id);
void rollback_prepared_transaction(const PgConnection& conn, const PreparedTransactionId& id);

}//namespace LibPg

#endif//PREPARED_TRANSACTION_HH_7CEA42D401A56D465A06B0B85D0D6E68
