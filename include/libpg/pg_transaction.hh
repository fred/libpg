/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PG_TRANSACTION_HH_3CFACD1BD555601655A9A7B7F6B81137//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PG_TRANSACTION_HH_3CFACD1BD555601655A9A7B7F6B81137

#include "libpg/pg_connection.hh"
#include "libpg/pg_exception.hh"
#include "libpg/pg_result.hh"

#include <utility>

namespace LibPg {

class PgTransaction;
class PgRoTransaction;
class PgRwTransaction;

namespace Unsafe {
namespace Dirty {
namespace Hack {

// Pointer ownership is still in a PgTransaction possession
AliasForPGconn* get_raw_pointer(const PgTransaction&);

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

class PgTransaction
{
public:
    struct SessionDefault;
    struct ReadCommitted;
    struct ReadUncommitted;
    struct RepeatableRead;
    struct Deferrable;
    struct NotDeferrable;
    template <typename = SessionDefault> struct Serializable;
    template <typename = SessionDefault> struct IsolationLevel { };
    static constexpr auto session_default = IsolationLevel<>{};
    static constexpr auto read_committed = IsolationLevel<ReadCommitted>{};
    static constexpr auto read_uncommitted = IsolationLevel<ReadUncommitted>{};
    static constexpr auto repeatable_read = IsolationLevel<RepeatableRead>{};
    static constexpr auto serializable = IsolationLevel<Serializable<>>{};
    static constexpr auto serializable_deferrable = IsolationLevel<Serializable<Deferrable>>{};
    static constexpr auto serializable_not_deferrable = IsolationLevel<Serializable<NotDeferrable>>{};

    operator const PgConnection&()const;

    template <typename Action>
    auto recoverable(Action&& do_something)const;

    PgTransaction() = delete;
    PgTransaction(const PgTransaction&) = delete;
    PgTransaction& operator=(const PgTransaction&) = delete;
    PgTransaction& operator=(PgTransaction&&) = delete;
private:
    struct ReadWrite;
    struct ReadOnly;
    template <typename> struct AccessMode {};
    static constexpr auto read_write = AccessMode<ReadWrite>{};
    static constexpr auto read_only = AccessMode<ReadOnly>{};
    template <typename Level, typename Access>
    explicit PgTransaction(const Dsn& dsn, IsolationLevel<Level> level, AccessMode<Access> access_mode)
        : PgTransaction{PgConnection{dsn}, level, access_mode}
    { }
    template <typename Level, typename Access>
    static const char* cmd()noexcept;
    template <typename Level, typename Access>
    explicit PgTransaction(PgConnection conn, IsolationLevel<Level>, AccessMode<Access>)
        : conn_{std::move(conn)}
    {
        PgResultNoData{exec(conn_, cmd<Level, Access>())};
    }
    PgTransaction(PgTransaction&& src);
    ~PgTransaction();
    PgConnection conn_;
    friend Unsafe::Dirty::Hack::AliasForPGconn* Unsafe::Dirty::Hack::get_raw_pointer(const PgTransaction&);
    friend class PgRoTransaction;
    friend class PgRwTransaction;
    class Savepoint;
};

class PgTransaction::Savepoint
{
public:
    explicit Savepoint(const PgTransaction& transaction);
    ~Savepoint();
    void recovery()&& noexcept;
private:
    const PgTransaction& transaction_;
};

template <typename Action>
auto PgTransaction::recoverable(Action&& do_something)const
{
    Savepoint savepoint{*this};
    try
    {
        return std::forward<Action>(do_something)(*this);
    }
    catch (const ExecFailure&)
    {
        std::move(savepoint).recovery();
        throw;
    }
}

template <typename Q, typename ...Args>
decltype(auto) try_exec(const PgTransaction& transaction, Q&& query, Args&& ...args)
{
    return transaction.recoverable([&](const PgTransaction& transaction)
                                   {
                                       return exec(transaction, std::forward<Q>(query), std::forward<Args>(args)...);
                                   });
}

}//namespace LibPg

#endif//PG_TRANSACTION_HH_3CFACD1BD555601655A9A7B7F6B81137
