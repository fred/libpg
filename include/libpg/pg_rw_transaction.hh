/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PG_RW_TRANSACTION_HH_6CB5B32E1570F4F36DBFA73D4C53F25E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PG_RW_TRANSACTION_HH_6CB5B32E1570F4F36DBFA73D4C53F25E

#include "libpg/pg_transaction.hh"

#include <utility>

namespace LibPg {

class PreparedTransactionId;

class PgRwTransaction;

namespace Unsafe {
namespace Dirty {
namespace Hack {

// Pointer ownership is still in a PgRwTransaction possession
AliasForPGconn* get_raw_pointer(const PgRwTransaction&);

}//namespace LibPg::Unsafe::Dirty::Hack
}//namespace LibPg::Unsafe::Dirty
}//namespace LibPg::Unsafe

class PgRwTransaction
{
public:
    template <typename Level = PgTransaction::SessionDefault>
    explicit PgRwTransaction(const Dsn& dsn, PgTransaction::IsolationLevel<Level> level = PgTransaction::session_default)
        : PgRwTransaction{PgConnection{dsn}, level}
    { }
    template <typename Level = PgTransaction::SessionDefault>
    explicit PgRwTransaction(PgConnection conn, PgTransaction::IsolationLevel<Level> level = PgTransaction::session_default)
        : transaction_{std::move(conn), level, PgTransaction::read_write}
    { }
    PgRwTransaction(PgRwTransaction&& src);
    ~PgRwTransaction();

    PgRwTransaction() = delete;
    PgRwTransaction(const PgRwTransaction&) = delete;
    PgRwTransaction& operator=(const PgRwTransaction&) = delete;
    PgRwTransaction& operator=(PgRwTransaction&&) = delete;

    operator const PgConnection&()const;
    operator const PgTransaction&()const;

    template <typename Action>
    auto recoverable(Action&& do_something)const;
private:
    PgTransaction transaction_;
    PgConnection get_conn()&&;
    friend PgConnection commit(PgRwTransaction);
    friend PgConnection rollback(PgRwTransaction);
    friend PgConnection prepare_transaction(PgRwTransaction, const PreparedTransactionId&);
    friend Unsafe::Dirty::Hack::AliasForPGconn* Unsafe::Dirty::Hack::get_raw_pointer(const PgRwTransaction&);
};

template <typename Action>
auto PgRwTransaction::recoverable(Action&& do_something)const
{
    PgTransaction::Savepoint savepoint{*this};
    try
    {
        return std::forward<Action>(do_something)(*this);
    }
    catch (const ExecFailure&)
    {
        std::move(savepoint).recovery();
        throw;
    }
}

template <typename Q, typename ...Args>
decltype(auto) try_exec(const PgRwTransaction& transaction, Q&& query, Args&& ...args)
{
    return transaction.recoverable([&](const PgRwTransaction& transaction)
                                   {
                                       return exec(transaction, std::forward<Q>(query), std::forward<Args>(args)...);
                                   });
}

}//namespace LibPg

#endif//PG_RW_TRANSACTION_HH_6CB5B32E1570F4F36DBFA73D4C53F25E
