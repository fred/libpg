/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef QUERY_HH_A90B7F46D86ECE2953CD691B21EA652D//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define QUERY_HH_A90B7F46D86ECE2953CD691B21EA652D

#include "libpg/pg_connection.hh"
#include "libpg/pg_result.hh"
#include "libpg/query_parameter.hh"
#include "libpg/detail/const_pseudo_iterator.hh"
#include "libpg/detail/has_only_unique_elements.hh"

#include <boost/optional.hpp>

#include <tuple>
#include <type_traits>

namespace LibPg {

template <typename Columns = std::tuple<>, typename Arguments = std::tuple<>>
class Query;

template <typename Columns> class View;

template <typename ColumnType>
struct TupleItem
{ };

template <typename ColumnType>
constexpr decltype(auto) item()
{
    return TupleItem<ColumnType>{};
}

template <typename T>
struct NullableItem
{
    using Type = boost::optional<T>;
};

template <typename T, typename Tag, template <typename> class Skills>
struct NullableItem<LibStrong::Type<T, Tag, Skills>>
{
    using Type = typename LibStrong::Type<T, Tag, Skills>::Optional;
};

template <typename ColumnType>
constexpr decltype(auto) nullable_item()
{
    return item<typename NullableItem<ColumnType>::Type>();
}

template <typename ArgumentType>
struct QueryParameterNoIndex
{
private:
    static decltype(auto) as_counted_parameter()
    {
        return std::string{};
    }
    template <typename, typename>
    friend class QueryParameter;
};

template <typename ArgumentType>
decltype(auto) parameter()
{
    return make_query_parameter(QueryParameterNoIndex<ArgumentType>{});
}

Query<> make_query(std::string);

template <typename ...Items, typename ...Args>
class Query<std::tuple<Items...>, std::tuple<Args...>>
{
public:
    Query() = delete;
    Query& operator<<(const std::string& sql)
    {
        sql_.append(sql);
        return *this;
    }
    template <typename ColumnType>
    decltype(auto) operator<<(const TupleItem<ColumnType>&)const
    {
        static_assert(!Detail::IsElement<ColumnType, std::tuple<Items...>>::value, "ColumnType must be unique");
        using NextQuery = Query<std::tuple<Items..., ColumnType>, std::tuple<Args...>>;
        return NextQuery{sql_ + (sizeof...(Items) == 0 ? " " : ",")};
    }
    template <typename UniqueArgument, typename ParameterType,
              std::enable_if_t<!Detail::IsElement<UniqueArgument, std::tuple<Args...>>::value>* = nullptr>
    decltype(auto) operator<<(const QueryParameter<QueryParameterNoIndex<UniqueArgument>, ParameterType>& parameter)const
    {
        using ExtendedUniqueArguments = std::tuple<Args..., UniqueArgument>;
        using NextQuery = Query<std::tuple<Items...>, ExtendedUniqueArguments>;
        return NextQuery{sql_ + parameter.at_position(NamedQueryArguments<Args..., UniqueArgument>::template get_index<UniqueArgument>())};
    }
    template <typename ExistingArgument, typename ParameterType,
              std::enable_if_t<Detail::IsElement<ExistingArgument, std::tuple<Args...>>::value>* = nullptr>
    decltype(auto) operator<<(const QueryParameter<QueryParameterNoIndex<ExistingArgument>, ParameterType>& parameter)
    {
        sql_.append(std::string{} + parameter.at_position(NamedQueryArguments<Args...>::template get_index<ExistingArgument>()));
        return *this;
    }
    decltype(auto) get_sql()const
    {
        return sql_;
    }
    using Arguments = NamedQueryArguments<Args...>;
private:
    explicit Query(std::string sql)
        : sql_{std::move(sql)}
    { }
    std::string sql_;
    template <typename, typename>
    friend class Query;
    friend Query<> make_query(std::string);
};

template <typename ...Is, typename ...Args,
          std::enable_if_t<0 < sizeof...(Args)>* = nullptr>
decltype(auto) exec(const PgConnection& conn,
                    const Query<std::tuple<Is...>, std::tuple<Args...>>& query,
                    const typename Query<std::tuple<Is...>, std::tuple<Args...>>::Arguments& args)
{
    using Result = std::conditional_t<sizeof...(Is) == 0, PgResultNoData,
                                                          View<std::tuple<Is...>>>;
    return Result{exec(conn, query.get_sql(), args)};
}

template <typename ...Is, typename ...Args,
          std::enable_if_t<sizeof...(Args) == 0>* = nullptr>
decltype(auto) exec(const PgConnection& conn, const Query<std::tuple<Is...>, std::tuple<Args...>>& query)
{
    using Result = std::conditional_t<sizeof...(Is) == 0, PgResultNoData,
                                                          View<std::tuple<Is...>>>;
    return Result{exec(conn, query.get_sql())};
}

template <typename ...Is, typename ...QueryArgs, typename Arg0, typename Arg1, typename ...Args>
decltype(auto) exec(const PgConnection& conn,
                    const Query<std::tuple<Is...>, std::tuple<QueryArgs...>>& query,
                    Arg0&& arg0,
                    Arg1&& arg1,
                    Args&& ...args)
{
    return exec(conn, query, {std::forward<Arg0>(arg0), std::forward<Arg1>(arg1), std::forward<Args>(args)...});
}

constexpr decltype(auto) make_query()
{
    struct EmptyQuery
    {
        Query<> operator<<(std::string cmd_part)const
        {
            return make_query(std::move(cmd_part));
        }
    };
    return EmptyQuery{};
}

[[noreturn]] void throw_unexpected_number_of_columns();

template <typename ...Types>
class View<std::tuple<Types...>>
{
public:
    View(PgConnection::ExecResult exec_result)
        : result_{std::move(exec_result)}
    {
        if (result_.get_number_of_columns() != ColumnIndex{static_cast<typename ColumnIndex::ValueType>(sizeof...(Types))})
        {
            throw_unexpected_number_of_columns();
        }
    }
    decltype(auto) size()const
    {
        return result_.size();
    }
    bool empty()const
    {
        return result_.empty();
    }
    decltype(auto) get_number_of_rows()const
    {
        return result_.get_number_of_rows();
    }
    decltype(auto) get_number_of_columns()const
    {
        return result_.get_number_of_columns();
    }
    decltype(auto) get_number_of_rows_affected()const
    {
        return result_.get_number_of_rows_affected();
    }
    using Row = NamedColumns<Types...>;
    Row operator[](RowIndex row_idx)const
    {
        return result_[row_idx].template as<Row>();
    }
    Row at(RowIndex row_idx)const
    {
        return result_.at(row_idx).template as<Row>();
    }

    class ConstRowPseudoIterator;
    ConstRowPseudoIterator begin()const
    {
        return ConstRowPseudoIterator{&result_, RowIndex{0}};
    }
    ConstRowPseudoIterator end()const
    {
        return ConstRowPseudoIterator{&result_, result_.size()};
    }
    ConstRowPseudoIterator cbegin()const
    {
        return this->begin();
    }
    ConstRowPseudoIterator cend()const
    {
        return this->end();
    }
    Row front()const
    {
        return *(this->begin());
    }
    Row back()const
    {
        return *(this->end() - 1);
    }
private:
    PgResultTuples result_;
};

template <typename ...Types>
class View<std::tuple<Types...>>::ConstRowPseudoIterator : public Detail::ConstPseudoIterator<ConstRowPseudoIterator, PgResult::SizeType>
{
public:
    ConstRowPseudoIterator() = delete;

    Row operator*()const
    {
        return (*result_)[RowIndex{row_index_}].template as<Row>();
    }
private:
    ConstRowPseudoIterator(const PgResultTuples* result, RowIndex row_index)noexcept
        : result_{result},
          row_index_{*row_index}
    { }
    ConstRowPseudoIterator& add(PgResult::SizeType offset)
    {
        row_index_ += offset;
        return *this;
    }
    ConstRowPseudoIterator& sub(PgResult::SizeType offset)
    {
        row_index_ -= offset;
        return *this;
    }
    PgResult::SizeType comparable()const
    {
        return row_index_;
    }
    PgResult::SizeType difference(const ConstRowPseudoIterator& rhs)const
    {
        return row_index_ - rhs.row_index_;
    }
    const PgResultTuples* result_;
    int row_index_;
    friend class View;
    friend class Detail::ConstPseudoIterator<ConstRowPseudoIterator, PgResult::SizeType>;
};

}//namespace LibPg

#endif//QUERY_HH_A90B7F46D86ECE2953CD691B21EA652D
