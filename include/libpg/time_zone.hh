/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIME_ZONE_HH_1D848D6E003D171DC2B113B3F2E82707//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define TIME_ZONE_HH_1D848D6E003D171DC2B113B3F2E82707

#include <string>

namespace LibPg {

class PgConnection;
class PgTransaction;
class PgRoTransaction;
class PgRwTransaction;

class TimeZone;

/**
 * Make time zone from hardcoded string.
 * @tparam psql_representation hardcoded string (must be declared as extern)
 * @return TimeZone instance with given content
 */
template <const char*& psql_representation>
decltype(auto) make_time_zone();

/**
 * Make time zone from runtime string. Its validity is guaranteed by presence in pg_timezone_names.
 * @param conn database connection for validity check
 * @param psql_representation time zone in Postgres format, like "Europe/Prague"
 * @return TimeZone instance with given content
 * @throw PgException if time zone is not valid
 */
TimeZone make_time_zone(const PgConnection& conn, std::string psql_representation);

/**
 * Make time zone from runtime string. Its validity is guaranteed by using in a real query.
 * Invalid time zone does not corrupt the transaction.
 * @param transaction database transaction for validity check
 * @param psql_representation time zone in Postgres format, like "Europe/Prague"
 * @return TimeZone instance with given content
 * @throw ExecFailure if the validity check fails unexpectedly
 * @throw PgException if time zone is not valid or something else is wrong
 */
TimeZone make_time_zone(const PgTransaction& transaction, std::string psql_representation);

TimeZone make_time_zone(const PgRoTransaction& transaction, std::string psql_representation);
TimeZone make_time_zone(const PgRwTransaction& transaction, std::string psql_representation);

class TimeZone
{
public:
    TimeZone() = delete;
    static TimeZone utc();
private:
    explicit TimeZone(std::string psql_representation);
    std::string psql_representation_;
    template <const char*& psql_representation>
    friend decltype(auto) make_time_zone()
    {
        return TimeZone{psql_representation};
    }
    friend TimeZone make_time_zone(const PgConnection& conn, std::string psql_representation);
    friend TimeZone make_time_zone(const PgTransaction& transaction, std::string psql_representation);
    friend const std::string& to_psql(const TimeZone& time_zone);
};

}//namespace LibPg

#endif//TIME_ZONE_HH_1D848D6E003D171DC2B113B3F2E82707
