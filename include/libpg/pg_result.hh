/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PG_RESULT_HH_9D6DA126BA9E1C7CC862A16F55F36DF9//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PG_RESULT_HH_9D6DA126BA9E1C7CC862A16F55F36DF9

#include "libpg/indexes.hh"
#include "libpg/named_columns.hh"
#include "libpg/null_value.hh"
#include "libpg/pg_connection.hh"
#include "libpg/detail/const_pseudo_iterator.hh"
#include "libpg/detail/indexed_tuple.hh"
#include "libpg/detail/unwrapper.hh"

#include "libstrong/type.hh"

#include <boost/optional.hpp>

#include <iterator>
#include <string>
#include <tuple>
#include <type_traits>

namespace LibPg {

class PgResultTuples;
class PgResultNoData;

class PgResult
{
public:
    PgResult(PgConnection::ExecResult);
    PgResult& operator=(PgConnection::ExecResult&&);

    PgResult();
    PgResult(PgResult&&);
    PgResult& operator=(PgResult&&);

    PgResult(const PgResult&) = delete;
    PgResult& operator=(const PgResult&) = delete;

    class Value;
    class ConstValuePseudoIterator;
    class Row;
    class ConstRowPseudoIterator;
    using SizeType = PgConnection::ExecResult::SizeType;

    bool empty()const noexcept;
    RowIndex size()const noexcept;
    RowIndex get_number_of_rows()const noexcept;
    ColumnIndex get_number_of_columns()const noexcept;
    std::string get_column_name(ColumnIndex column_idx)const;
    RowIndex get_number_of_rows_affected()const;

    Row operator[](RowIndex row_idx)const;
    Row at(RowIndex row_idx)const;

    ConstRowPseudoIterator begin()const;
    ConstRowPseudoIterator end()const;
    ConstRowPseudoIterator cbegin()const;
    ConstRowPseudoIterator cend()const;
private:
    void tuples_ok_required()const;
    void command_ok_required()const;
    bool is_null(SizeType row_idx, SizeType column_idx)const;
    std::string get_not_null_value(SizeType row_idx, SizeType column_idx)const;
    SizeType get_column_idx(const char* column_name)const;
    PgConnection::ExecResult result_;
    friend class PgResultTuples;
    friend class PgResultNoData;
};

template <typename T>
T value_cast(const PgResult::Value& value);

class PgResult::Value
{
public:
    Value()noexcept;
    Value(const Value&)noexcept;
    Value& operator=(const Value&)noexcept;

    bool is_null()const noexcept;
    bool operator==(NullValueT)const noexcept;
    bool operator!=(NullValueT)const noexcept;
    friend bool operator==(NullValueT, const Value&)noexcept;
    friend bool operator!=(NullValueT, const Value&)noexcept;

    template <typename T>
    T as()const
    {
        return value_cast<T>(*this);
    }
    template <typename T>
    void into(T& dst)const
    {
        dst = value_cast<T>(*this);
    }
private:
    explicit Value(std::string value)noexcept;
    explicit Value(std::nullptr_t)noexcept;
    const char* get_raw_data()const;
    boost::optional<std::string> str_value_;
    friend class Row;
    friend class ConstValuePseudoIterator;
    template <typename T>
    friend T value_cast(const Value&);
};

template <typename Dst, typename Tag, template <typename> class Skills>
LibStrong::Type<Dst, Tag, Skills> from_nullable_string(const LibStrong::Type<Dst, Tag, Skills>&, const char*);

template <typename Dst>
boost::optional<Dst> from_nullable_string(const boost::optional<Dst>&, const char*);

template <typename Dst>
LibStrong::Optional<Dst> from_nullable_string(const LibStrong::Optional<Dst>&, const char*);

template <typename T>
constexpr const T& fake_const_ref()
{
    constexpr const T* fake_ptr = nullptr;
    return *fake_ptr;
}

template <typename Dst>
Dst from_nullable_string(const Dst&, const char* src)
{
    using namespace Detail;
    return unwrap_from_psql_representation(fake_const_ref<Dst>(), src);
}

template <typename Dst>
boost::optional<Dst> from_nullable_string(const boost::optional<Dst>&, const char* src)
{
    if (src == nullptr)
    {
        return boost::optional<Dst>{boost::none};
    }
    return boost::optional<Dst>{from_nullable_string(fake_const_ref<Dst>(), src)};
}

template <typename Dst>
LibStrong::Optional<Dst> from_nullable_string(const LibStrong::Optional<Dst>&, const char* src)
{
    if (src == nullptr)
    {
        return LibStrong::Optional<Dst>{Dst::nullopt};
    }
    return LibStrong::Optional<Dst>{from_nullable_string(fake_const_ref<Dst>(), src)};
}

template <typename Dst, typename Tag, template <typename> class Skills>
LibStrong::Type<Dst, Tag, Skills> from_nullable_string(const LibStrong::Type<Dst, Tag, Skills>&, const char* src)
{
    return LibStrong::Type<Dst, Tag, Skills>{from_nullable_string(fake_const_ref<Dst>(), src)};
}

template <typename Dst>
Dst value_cast(const PgResult::Value& value)
{
    return from_nullable_string(fake_const_ref<Dst>(), value == null_value ? nullptr
                                                                           : value.get_raw_data());
}

class PgResult::ConstValuePseudoIterator : public Detail::ConstPseudoIterator<ConstValuePseudoIterator, SizeType>
{
public:
    ConstValuePseudoIterator() = delete;

    Value operator*()const;
private:
    ConstValuePseudoIterator(const Row* row, SizeType column_idx)noexcept;
    static bool can_be_dereferenced(const Row& row, SizeType column_idx)noexcept;
    ConstValuePseudoIterator& add(SizeType);
    ConstValuePseudoIterator& sub(SizeType);
    SizeType comparable()const;
    SizeType difference(const ConstValuePseudoIterator& rhs)const;
    const Row* row_;
    SizeType column_idx_;
    friend class PgResult;
    friend class Detail::ConstPseudoIterator<ConstValuePseudoIterator, SizeType>;
};

class PgResult::Row
{
public:
    Row()noexcept;
    Row(const Row&)noexcept;
    Row& operator=(const Row&)noexcept;

    bool empty()const noexcept;
    ColumnIndex size()const noexcept;
    ColumnIndex get_number_of_columns()const noexcept;
    std::string get_column_name(ColumnIndex column_idx)const;

    Value operator[](ColumnIndex column_idx)const;
    Value operator[](const char* column_name)const;
    Value operator[](const std::string& column_name)const;

    Value at(ColumnIndex column_idx)const;
    Value at(const char* column_name)const;
    Value at(const std::string& column_name)const;

    template <typename T>
    auto as()const
    {
        return ConvertInto<T>::get(*this);
    }

    ConstValuePseudoIterator begin()const;
    ConstValuePseudoIterator end()const;
    ConstValuePseudoIterator cbegin()const;
    ConstValuePseudoIterator cend()const;
private:
    Row(const PgResult* container, SizeType row_idx);
    template <typename Dst, typename = void>
    struct ConvertInto;
    template <typename Dst>
    struct ConvertInto<Dst, std::enable_if_t<std::is_constructible<Dst, Row>::value>>
    {
        static Dst get(const Row& row)
        {
            return Dst{row};
        }
    };
    template <typename Dst>
    struct ConvertInto<Dst, std::enable_if_t<std::is_constructible<Dst, ConstValuePseudoIterator, ConstValuePseudoIterator>::value>>
    {
        static Dst get(const Row& row)
        {
            return Dst{row.begin(), row.end()};
        }
    };
    template <typename ...DstTypes>
    struct ConvertInto<std::tuple<DstTypes...>>
    {
        static auto get(const Row& row)
        {
            return MakeTuple<Detail::IndexedTuple<DstTypes...>>::from(row);
        }
    private:
        template <typename> struct MakeTuple;
        template <typename ...IndexTypePairs>
        struct MakeTuple<std::tuple<IndexTypePairs...>>
        {
            static auto from(const Row& src)
            {
                src.required_number_of_columns(ColumnIndex{static_cast<typename ColumnIndex::ValueType>(sizeof...(IndexTypePairs))});
                return std::make_tuple(get_from_as<IndexTypePairs>(src)...);
            }
        };
        template <typename IndexTypePair>
        static auto get_from_as(const Row& row)
        {
            return row[ColumnIndex{IndexTypePair::Index::value}].template as<typename IndexTypePair::Type>();
        }
    };
    template <typename ...DstTypes>
    struct ConvertInto<NamedColumns<DstTypes...>>;
    void required_number_of_columns(ColumnIndex required_value)const;
    const PgResult* container_;
    SizeType idx_;
    friend class PgResult;
    friend class ConstRowPseudoIterator;
};

template <typename Dst>
Dst row_cast(const PgResult::Row& row)
{
    return row.as<Dst>();
}

class PgResult::ConstRowPseudoIterator : public Detail::ConstPseudoIterator<ConstRowPseudoIterator, SizeType>
{
public:
    ConstRowPseudoIterator() = delete;

    Row operator*()const;
private:
    ConstRowPseudoIterator(const PgResult& result, SizeType row_idx)noexcept;
    ConstRowPseudoIterator& add(SizeType);
    ConstRowPseudoIterator& sub(SizeType);
    SizeType comparable()const;
    SizeType difference(const ConstRowPseudoIterator& rhs)const;
    Row row_;
    friend class PgResult;
    friend class Detail::ConstPseudoIterator<ConstRowPseudoIterator, SizeType>;
};

class PgResultTuples
{
public:
    PgResultTuples(PgConnection::ExecResult exec_result);
    PgResultTuples& operator=(PgConnection::ExecResult&& exec_result);

    PgResultTuples();
    PgResultTuples(PgResultTuples&&);
    PgResultTuples& operator=(PgResultTuples&&);

    PgResultTuples(const PgResultTuples&) = delete;
    PgResultTuples& operator=(const PgResultTuples&) = delete;

    using ConstRowPseudoIterator = PgResult::ConstRowPseudoIterator;
    using Row = PgResult::Row;

    bool empty()const noexcept;
    RowIndex size()const noexcept;
    RowIndex get_number_of_rows()const noexcept;
    ColumnIndex get_number_of_columns()const noexcept;
    std::string get_column_name(ColumnIndex column_idx)const;
    RowIndex get_number_of_rows_affected()const;

    Row operator[](RowIndex row_idx)const;
    Row at(RowIndex row_idx)const;

    ConstRowPseudoIterator begin()const;
    ConstRowPseudoIterator end()const;
    ConstRowPseudoIterator cbegin()const;
    ConstRowPseudoIterator cend()const;
private:
    friend class PgResult::Row;
    template <typename, typename> struct MakeColumns;
    template <typename Columns, std::size_t ...indexes>
    struct MakeColumns<Columns, std::integer_sequence<std::size_t, indexes...>>
    {
        static auto from(const Row& row)
        {
            return Columns{row[ColumnIndex{static_cast<ColumnIndex::ValueType>(indexes)}].as<typename Columns::template ColumnByIndex<indexes>>()...};
        }
    };
    PgResult result_;
    template <typename, typename>
    friend struct PgResult::Row::ConvertInto;
};

template <typename ...DstTypes>
struct PgResult::Row::ConvertInto<NamedColumns<DstTypes...>>
{
    static auto get(const Row& row)
    {
        using Columns = NamedColumns<DstTypes...>;
        row.required_number_of_columns(ColumnIndex{static_cast<typename ColumnIndex::ValueType>(sizeof...(DstTypes))});
        return PgResultTuples::MakeColumns<Columns, std::make_index_sequence<sizeof...(DstTypes)>>::from(row);
    }
};

class PgResultNoData
{
public:
    PgResultNoData(PgConnection::ExecResult exec_result);

    PgResultNoData();
    PgResultNoData(PgResultNoData&&);
    PgResultNoData& operator=(PgResultNoData&&);

    PgResultNoData(const PgResultNoData&) = delete;
    PgResultNoData& operator=(const PgResultNoData&) = delete;

    RowIndex get_number_of_rows_affected()const;
private:
    PgResult result_;
};

}//namespace LibPg

#endif//PG_RESULT_HH_9D6DA126BA9E1C7CC862A16F55F36DF9
