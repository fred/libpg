/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef QUERY_ARGUMENTS_HH_AC6FF376D3C0AB84F63F6489E4899DED//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define QUERY_ARGUMENTS_HH_AC6FF376D3C0AB84F63F6489E4899DED

#include "libpg/detail/wrapper.hh"

#include "libstrong/type.hh"

#include <boost/optional.hpp>

#include <string>
#include <type_traits>

namespace LibPg {

template <typename = void>
class QueryArguments;

template <typename> struct IsOptionalType : std::false_type { };
template <typename Type> struct IsOptionalType<boost::optional<Type>> : std::true_type { };
template <typename Type> struct IsOptionalType<LibStrong::Optional<Type>> : std::true_type { };
template <typename T>
using IsOptional = IsOptionalType<std::decay_t<T>>;

template <typename> struct IsStrongType : std::false_type { };
template <typename Type, typename Tag, template <typename> class Skills> struct IsStrongType<LibStrong::Type<Type, Tag, Skills>> : std::true_type { };
template <typename T>
using IsStrong = IsStrongType<std::decay_t<T>>;

template <typename Type,
          std::enable_if_t<IsStrong<Type>::value>* = nullptr>
boost::optional<std::string> make_one_query_argument_from_nullable(Type&&);

template <typename Type,
          std::enable_if_t<IsOptional<Type>::value>* = nullptr>
boost::optional<std::string> make_one_query_argument_from_nullable(Type&&);

template <typename Type,
          std::enable_if_t<!IsOptional<Type>::value && !IsStrong<Type>::value>* = nullptr>
boost::optional<std::string> make_one_query_argument_from_nullable(Type&& src)
{
    using namespace Detail;
    return wrap_into_psql_representation(std::forward<Type>(src));
}

template <typename Type,
          std::enable_if_t<IsStrong<Type>::value>*>
boost::optional<std::string> make_one_query_argument_from_nullable(Type&& src)
{
    return make_one_query_argument_from_nullable(*std::forward<Type>(src));
}

template <typename T>
bool is_nullopt(const boost::optional<T>& value)
{
    return value == boost::none;
}

template <typename Type,
          std::enable_if_t<IsOptional<Type>::value>*>
boost::optional<std::string> make_one_query_argument_from_nullable(Type&& src)
{
    return is_nullopt(src) ? make_one_query_argument_from_nullable(null_value)
                           : make_one_query_argument_from_nullable(*std::forward<Type>(src));
}

}//namespace LibPg

#endif//QUERY_ARGUMENTS_HH_AC6FF376D3C0AB84F63F6489E4899DED
