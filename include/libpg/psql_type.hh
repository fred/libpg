/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PSQL_TYPE_HH_BE244CD222239C3A43D4CEED7FC4E377//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PSQL_TYPE_HH_BE244CD222239C3A43D4CEED7FC4E377

#include <string>

namespace LibPg {

struct PsqlType
{
    struct SmallInt;
    struct Integer;
    struct BigInt;
    template <int precision, int scale = 0>
    struct Numeric;
    template <int precision, int scale = 0>
    using Decimal = Numeric<precision, scale>;
    struct Real;
    struct DoublePrecision;

    template <int fixed_size = 1>
    struct Char;
    template <int max_size = -1>
    struct VarChar;
    struct Text;

    struct Boolean;

    struct Timestamp;
    using TimestampWithoutTimeZone = Timestamp;
    struct TimestampWithTimeZone;
    struct Date;
    struct Time;
    using TimeWithoutTimeZone = Time;
    struct TimeWithTimeZone;
};

namespace Detail {

template <typename> struct PsqlTypeName;

template <typename Type>
std::string get_psql_type_name()
{
    return PsqlTypeName<Type>::get();
}

template <>
std::string get_psql_type_name<PsqlType::SmallInt>();

template <>
std::string get_psql_type_name<PsqlType::Integer>();

template <>
std::string get_psql_type_name<PsqlType::BigInt>();

template <int precision, int scale>
struct PsqlTypeName<PsqlType::Numeric<precision, scale>>
{
    static_assert(0 < precision, "precision must be positive");
    static_assert(0 <= scale, "scale must be zero or positive");
    static std::string get()
    {
        auto result = "NUMERIC(" + std::to_string(precision);
        if (0 < scale)
        {
            result += "," + std::to_string(scale);
        }
        result += ")";
        return result;
    }
};

template <>
std::string get_psql_type_name<PsqlType::Real>();

template <>
std::string get_psql_type_name<PsqlType::DoublePrecision>();

template <int fixed_size>
struct PsqlTypeName<PsqlType::Char<fixed_size>>
{
    static_assert(0 < fixed_size, "length for type char must be at least 1");
    static std::string get()
    {
        auto result = std::string{"CHAR"};
        if (1 < fixed_size)
        {
            result += "(" + std::to_string(fixed_size) + ")";
        }
        return result;
    }
};

template <int max_size>
struct PsqlTypeName<PsqlType::VarChar<max_size>>
{
    static_assert(0 < max_size, "length for type varchar must be at least 1");
    static std::string get()
    {
        return std::string{"VARCHAR("} + std::to_string(max_size) + ")";
    }
};

template <>
std::string get_psql_type_name<PsqlType::VarChar<-1>>();

template <>
std::string get_psql_type_name<PsqlType::Text>();

template <>
std::string get_psql_type_name<PsqlType::Boolean>();

template <>
std::string get_psql_type_name<PsqlType::TimestampWithTimeZone>();

template <>
std::string get_psql_type_name<PsqlType::Timestamp>();

template <>
std::string get_psql_type_name<PsqlType::Date>();

template <>
std::string get_psql_type_name<PsqlType::TimeWithTimeZone>();

template <>
std::string get_psql_type_name<PsqlType::Time>();

}//namespace LibPg::Detail
}//namespace LibPg

#endif//PSQL_TYPE_HH_BE244CD222239C3A43D4CEED7FC4E377
