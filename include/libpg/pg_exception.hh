/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PG_EXCEPTION_HH_7900B759DFBB8955900A58BC6AD42B99//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PG_EXCEPTION_HH_7900B759DFBB8955900A58BC6AD42B99

#include "libpg/pg_connection.hh"

#include <exception>

namespace LibPg {

struct PgException : std::exception
{ };


struct ConnectFailure : PgException
{ };

struct ExecFailure : PgException
{
    virtual PgConnection::ExecResult::ErrorCode error_code()const noexcept = 0;
};

struct NoConnectionAvailable : PgException
{ };

}//namespace LibPg

#endif//PG_EXCEPTION_HH_7900B759DFBB8955900A58BC6AD42B99
