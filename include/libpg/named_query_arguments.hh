/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NAMED_QUERY_ARGUMENTS_HH_6E633BAF7F3C249F85BF7A13C7C3DAC5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define NAMED_QUERY_ARGUMENTS_HH_6E633BAF7F3C249F85BF7A13C7C3DAC5

#include "libpg/fixed_size_query_arguments.hh"
#include "libpg/detail/has_only_unique_elements.hh"

#include <array>
#include <type_traits>
#include <tuple>
#include <utility>

namespace LibPg {

template <typename ...UniqueTypes>
using NamedQueryArguments = QueryArguments<std::tuple<UniqueTypes...>>;

template <typename ...UniqueTypes>
class QueryArguments<std::tuple<UniqueTypes...>>
{
public:
    static_assert(Detail::HasOnlyUniqueElements<UniqueTypes...>::value, "all types must be unique in this sequence");
    static_assert(Detail::HasOnlyDecayedElements<UniqueTypes...>::value, "all types must be decayed");

    static constexpr auto number_of_arguments = sizeof...(UniqueTypes);

    /**
     * Construct from complete list of arguments
     * @param values list of arguments
     * @note the order of constructor arguments does not have to correspond with order of UniqueTypes
     **/
    template <typename ...Types,
              std::enable_if_t<Detail::HasTheSameElements<std::tuple<UniqueTypes...>, std::tuple<std::decay_t<Types>...>>::value>* = nullptr>
    QueryArguments(Types&& ...values)
        : storage_{GetParameter<UniqueTypes>::from(std::forward<Types>(values)...)...}
    {
        static_assert(Detail::HasOnlyUniqueElements<Types...>::value, "all types must be unique in this sequence");
        static_assert(number_of_arguments >= sizeof...(Types), "too much arguments");
        static_assert(sizeof...(Types) >= number_of_arguments, "insufficient number of arguments");
    }

    /**
     * Enables implicit conversion from QueryArguments with different order of UniqueTypes
     * @param src query arguments with different order of UniqueTypes
     **/
    template <typename ...Types,
             std::enable_if_t<Detail::HasTheSameElements<std::tuple<UniqueTypes...>, std::tuple<Types...>>::value &&
                              !std::is_same<std::tuple<UniqueTypes...>, std::tuple<Types...>>::value>* = nullptr>
    QueryArguments(QueryArguments<std::tuple<Types...>> src)
        : storage_{{std::move(src.storage_.arguments_[QueryArguments<std::tuple<Types...>>::template IndexOf<UniqueTypes>::value])...}}
    { }

    constexpr bool empty()const noexcept
    {
        return storage_.empty();
    }
    constexpr int size()const noexcept
    {
        return storage_.size();
    }

    /**
     * Returns untyped query parameter with an integer index corresponding with given Type
     * @tparam Type argument "name" used as an index
     * @return untyped QueryParameter at position defined by given Type
     **/
    template <typename Type>
    static constexpr decltype(auto) parameter()
    {
        static_assert(Detail::IsElement<Type, std::tuple<UniqueTypes...>>::value, "Type is not an element of UniqueTypes");
        return query_parameter_at_position<IndexOf<Type>::value>();
    }

    template <typename Type>
    static constexpr decltype(auto) get_index()
    {
        static_assert(Detail::IsElement<Type, std::tuple<UniqueTypes...>>::value, "Type is not an element of UniqueTypes");
        return QueryParameterCompiletimeIndex<IndexOf<Type>::value>{};
    }

    /**
     * Extend QueryParameter by one item
     * @param value appended argument value
     * @return new QueryParameter extended by the given Type
     **/
    template <typename Type>
    decltype(auto) push_back(Type&& value)const&
    {
        return NamedQueryArguments<UniqueTypes..., std::decay_t<Type>>{storage_.push_back(std::forward<Type>(value))};
    }

    /**
     * Extend QueryParameter by one item
     * @param value appended argument value
     * @return new QueryParameter extended by the given Type
     **/
    template <typename Type>
    decltype(auto) push_back(Type&& value)&&
    {
        return NamedQueryArguments<UniqueTypes..., std::decay_t<Type>>{std::move(storage_).push_back(std::forward<Type>(value))};
    }
private:
    using UnderlyingArguments = FixedSizeQueryArguments<number_of_arguments>;
    explicit QueryArguments(const UnderlyingArguments& arguments)
        : storage_{arguments}
    { }
    explicit QueryArguments(UnderlyingArguments&& arguments)
        : storage_{std::move(arguments)}
    { }
    template <typename, typename> struct IndexOfMember;
    template <typename Type, typename ...Seq>
    struct IndexOfMember<Type, std::tuple<Type, Seq...>>
        : std::integral_constant<int, 0>
    { };
    template <typename Type, typename Head, typename ...Tail>
    struct IndexOfMember<Type, std::tuple<Head, Tail...>>
        : std::integral_constant<int, 1 + IndexOfMember<Type, std::tuple<Tail...>>::value>
    { };
    template <typename Type>
    using IndexOf = IndexOfMember<Type, std::tuple<UniqueTypes...>>;
    template <typename SearchedType>
    struct GetParameter
    {
        template <typename Head, typename ...Tail,
                  std::enable_if_t<std::is_same<SearchedType, std::decay_t<Head>>::value>* = nullptr>
        static constexpr decltype(auto) from(Head&& head, Tail&&...)
        {
            return std::forward<Head>(head);
        }
        template <typename Head, typename ...Tail,
                  std::enable_if_t<!std::is_same<SearchedType, std::decay_t<Head>>::value>* = nullptr>
        static constexpr decltype(auto) from(Head&&, Tail&& ...tail)
        {
            return from(std::forward<Tail>(tail)...);
        }
    };
    decltype(auto) get_arguments()const
    {
        return storage_.get_arguments();
    }
    UnderlyingArguments storage_;
    template <typename> friend class QueryArguments;
    friend class PgConnection;
};

template <typename ...Types>
NamedQueryArguments<std::decay_t<Types>...> make_named_query_arguments(Types&& ...args)
{
    return NamedQueryArguments<std::decay_t<Types>...>{std::forward<Types>(args)...};
}

}//namespace LibPg

#endif//NAMED_QUERY_ARGUMENTS_HH_6E633BAF7F3C249F85BF7A13C7C3DAC5
