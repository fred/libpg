/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONST_PSEUDO_ITERATOR_HH_9630A8F4D601E0E4F89102C71B60C3B7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONST_PSEUDO_ITERATOR_HH_9630A8F4D601E0E4F89102C71B60C3B7

namespace LibPg {
namespace Detail {

template <typename Derived, typename SizeType = typename Derived::SizeType>
class ConstPseudoIterator
{
public:
    auto operator++(int)
    {
        Derived result{this->derived()};
        this->derived().add(1);
        return result;
    }
    auto& operator++()
    {
        this->derived().add(1);
        return this->derived();
    }
    auto operator--(int)
    {
        Derived result{this->derived()};
        this->derived().sub(1);
        return result;
    }
    auto& operator--()
    {
        this->derived().sub(1);
        return this->derived();
    }

    bool operator==(const Derived& rhs)const
    {
        return this->derived().comparable() == rhs.comparable();
    }
    bool operator!=(const Derived& rhs)const
    {
        return !(*this == rhs);
    }
    bool operator<(const Derived& rhs)const
    {
        return this->derived().comparable() < rhs.comparable();
    }
    bool operator<=(const Derived& rhs)const
    {
        return this->derived().comparable() <= rhs.comparable();
    }
    bool operator>(const Derived& rhs)const
    {
        return this->derived().comparable() > rhs.comparable();
    }
    bool operator>=(const Derived& rhs)const
    {
        return this->derived().comparable() >= rhs.comparable();
    }

    auto& operator+=(SizeType offset)
    {
        this->derived().add(offset);
        return this->derived();
    }
    auto& operator-=(SizeType offset)
    {
        this->derived().sub(offset);
        return this->derived();
    }

    auto operator+(SizeType offset)const
    {
        Derived result{this->derived()};
        result += offset;
        return result;
    }
    friend auto operator+(SizeType lhs, ConstPseudoIterator rhs)
    {
        return rhs + lhs;
    }
    auto operator-(SizeType offset)const
    {
        Derived result{this->derived()};
        result -= offset;
        return result;
    }
    SizeType operator-(const ConstPseudoIterator& rhs)const
    {
        return this->derived().difference(rhs);
    }
private:
    ~ConstPseudoIterator() { }
    const Derived& derived()const noexcept
    {
        return static_cast<const Derived&>(*this);
    }
    Derived& derived()noexcept
    {
        return static_cast<Derived&>(*this);
    }
    friend Derived;
};

}//namespace LibPg::Detail
}//namespace LibPg

#endif//CONST_PSEUDO_ITERATOR_HH_9630A8F4D601E0E4F89102C71B60C3B7
