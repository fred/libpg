/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef UNWRAPPER_HH_88436CFF08A3D80B36C8CAF97B51B4DD//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UNWRAPPER_HH_88436CFF08A3D80B36C8CAF97B51B4DD

#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>

#include <chrono>
#include <type_traits>
#include <string>

namespace LibPg {
namespace Detail {

bool unwrap_nullable_string_into_bool(const char*);

template <typename BuiltInType,
          std::enable_if_t<std::is_same<BuiltInType, bool>::value>* = nullptr>
BuiltInType unwrap_from_psql_representation(const BuiltInType&, const char* src)
{
    return unwrap_nullable_string_into_bool(src);
}

std::string unwrap_nullable_string_into_string(const char*);

template <typename BuiltInType,
          std::enable_if_t<std::is_same<BuiltInType, std::string>::value>* = nullptr>
BuiltInType unwrap_from_psql_representation(const BuiltInType&, const char* src)
{
    return unwrap_nullable_string_into_string(src);
}

void not_null_value_required(const char*);
[[noreturn]] void throw_value_is_not_valid();

template <typename BuiltInType,
          std::enable_if_t<std::is_arithmetic<BuiltInType>::value && !std::is_same<BuiltInType, bool>::value>* = nullptr>
BuiltInType unwrap_from_psql_representation(const BuiltInType&, const char* src)
{
    not_null_value_required(src);
    try
    {
        return boost::lexical_cast<BuiltInType>(src);
    }
    catch (const boost::bad_lexical_cast&)
    {
        throw_value_is_not_valid();
    }
}

boost::gregorian::date unwrap_nullable_string_into_boost_date(const char*);

template <typename BuiltInType,
          std::enable_if_t<std::is_same<BuiltInType, boost::gregorian::date>::value>* = nullptr>
BuiltInType unwrap_from_psql_representation(const BuiltInType&, const char* src)
{
    return unwrap_nullable_string_into_boost_date(src);
}

boost::posix_time::ptime unwrap_nullable_string_into_boost_ptime(const char*);

template <typename BuiltInType,
          std::enable_if_t<std::is_same<BuiltInType, boost::posix_time::ptime>::value>* = nullptr>
BuiltInType unwrap_from_psql_representation(const BuiltInType&, const char* src)
{
    return unwrap_nullable_string_into_boost_ptime(src);
}

template <typename Clock, typename Duration>
std::chrono::time_point<Clock, Duration> unwrap_from_psql_representation(const std::chrono::time_point<Clock, Duration>&, const char* src)
{
    const auto time_from_epoch = unwrap_nullable_string_into_boost_ptime(src) - boost::posix_time::from_time_t(0);
    return Clock::from_time_t(0) + std::chrono::microseconds{time_from_epoch.total_microseconds()};
}

boost::posix_time::time_duration unwrap_nullable_string_into_boost_time_duration(const char*);

template <typename BuiltInType,
          std::enable_if_t<std::is_same<BuiltInType, boost::posix_time::time_duration>::value>* = nullptr>
BuiltInType unwrap_from_psql_representation(const BuiltInType&, const char* src)
{
    return unwrap_nullable_string_into_boost_time_duration(src);
}

template <typename Rep, typename Period>
std::chrono::duration<Rep, Period> unwrap_from_psql_representation(const std::chrono::duration<Rep, Period>&, const char* src)
{
    const auto boost_duration = unwrap_nullable_string_into_boost_time_duration(src);
#ifdef BOOST_DATE_TIME_HAS_NANOSECONDS
    const std::chrono::nanoseconds chrono_duration{boost_duration.total_nanoseconds()};
#else
    const std::chrono::microseconds chrono_duration{boost_duration.total_microseconds()};
#endif
    return std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(chrono_duration);
}

template <typename T>
T* unwrap_from_psql_representation(const T* const&, const char*)
{
    static_assert(!std::is_same<T, char>::value, "conversion to C string is prohibited");
    static_assert(std::is_same<T, char>::value, "conversion to any pointer is prohibited");
}

}//namespace LibPg::Detail
}//namespace LibPg

#endif//UNWRAPPER_HH_88436CFF08A3D80B36C8CAF97B51B4DD
