/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INDEXED_TUPLE_HH_DEEA95A7E35A9715D2ADECFEECDD9A1D//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define INDEXED_TUPLE_HH_DEEA95A7E35A9715D2ADECFEECDD9A1D

#include <tuple>
#include <utility>

namespace LibPg {
namespace Detail {

template <typename, int = 0> struct TagByIndex;

template <int start>
struct TagByIndex<std::tuple<>, start>
{
    using IndexedTuple = std::tuple<>;
};

template <int start, typename Head, typename ...Tail>
struct TagByIndex<std::tuple<Head, Tail...>, start>
{
    struct IndexTypePair
    {
        using Index = std::integral_constant<int, start>;
        using Type = Head;
    };
    using This = std::tuple<IndexTypePair>;
    using Next = typename TagByIndex<std::tuple<Tail...>, start + 1>::IndexedTuple;
    using IndexedTuple = decltype(std::tuple_cat(std::declval<This>(), std::declval<Next>()));
};

template <typename ...Types>
using IndexedTuple = typename TagByIndex<std::tuple<Types...>>::IndexedTuple;

}//namespace LibPg::Detail
}//namespace LibPg

#endif//INDEXED_TUPLE_HH_DEEA95A7E35A9715D2ADECFEECDD9A1D
