/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBPQ_LAYER_IMPL_HH_4B7B1DBDE68C383AF36B1EF90829CF3E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIBPQ_LAYER_IMPL_HH_4B7B1DBDE68C383AF36B1EF90829CF3E

#include "libpg/detail/libpq_layer_interface.hh"

namespace LibPg {
namespace Detail {

struct LibPqLayerSimpleImplementation : LibPqLayerInterface
{
    ~LibPqLayerSimpleImplementation()override;
    ::PGconn *PQconnectdbParams(const char* const*, const char* const*, int)const override;
    ::ConnStatusType PQstatus(const ::PGconn*)const override;
    ::PQnoticeReceiver PQsetNoticeReceiver(::PGconn*, ::PQnoticeReceiver, void*)const override;
    ::PQnoticeProcessor PQsetNoticeProcessor(::PGconn*, ::PQnoticeProcessor, void*)const override;
    ::PGresult* PQexec(::PGconn*, const char*)const override;
    ::PGresult* PQexecParams(::PGconn*, const char*, int, const ::Oid*, const char* const*, const int*, const int*, int)const override;
    void PQfinish(::PGconn*)const override;
    char* PQresultErrorField(const ::PGresult*, int)const override;
    char* PQresStatus(::ExecStatusType)const override;
    void PQclear(::PGresult*)const override;
    ::ExecStatusType PQresultStatus(const ::PGresult*)const override;
    int PQntuples(const ::PGresult*)const override;
    int PQnfields(const ::PGresult*)const override;
    int PQgetisnull(const ::PGresult*, int, int)const override;
    int PQfnumber(const ::PGresult*, const char*)const override;
    char* PQgetvalue(const ::PGresult*, int, int)const override;
    int PQgetlength(const ::PGresult*, int, int)const override;
    char* PQfname(const ::PGresult*, int)const override;
    char* PQcmdTuples(::PGresult*)const override;
};

}//namespace LibPg::Detail
}//namespace LibPg

#endif//LIBPQ_LAYER_IMPL_HH_4B7B1DBDE68C383AF36B1EF90829CF3E
