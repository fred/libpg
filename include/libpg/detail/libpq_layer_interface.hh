/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBPQ_LAYER_INTERFACE_HH_024950A4FCE5C9F28B76BEEBB9503657//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIBPQ_LAYER_INTERFACE_HH_024950A4FCE5C9F28B76BEEBB9503657

#include "libpg/libpq_layer.hh"

#include <libpq-fe.h>

namespace LibPg {
namespace Detail {

struct LibPqLayerInterface : LibPqLayer
{
    virtual ~LibPqLayerInterface();
    virtual ::PGconn *PQconnectdbParams(const char* const*, const char* const*, int)const = 0;
    virtual ::ConnStatusType PQstatus(const ::PGconn*)const = 0;
    virtual ::PQnoticeReceiver PQsetNoticeReceiver(::PGconn*, ::PQnoticeReceiver, void*)const = 0;
    virtual ::PQnoticeProcessor PQsetNoticeProcessor(::PGconn*, ::PQnoticeProcessor, void*)const = 0;
    virtual ::PGresult* PQexec(::PGconn*, const char*)const = 0;
    virtual ::PGresult* PQexecParams(::PGconn*, const char*, int, const ::Oid*, const char* const*, const int*, const int*, int)const = 0;
    virtual void PQfinish(::PGconn*)const = 0;
    virtual char* PQresultErrorField(const ::PGresult*, int)const = 0;
    virtual char* PQresStatus(::ExecStatusType)const = 0;
    virtual void PQclear(::PGresult*)const = 0;
    virtual ::ExecStatusType PQresultStatus(const ::PGresult*)const = 0;
    virtual int PQntuples(const ::PGresult*)const = 0;
    virtual int PQnfields(const ::PGresult*)const = 0;
    virtual int PQgetisnull(const ::PGresult*, int, int)const = 0;
    virtual int PQfnumber(const ::PGresult*, const char*)const = 0;
    virtual char* PQgetvalue(const ::PGresult*, int, int)const = 0;
    virtual int PQgetlength(const ::PGresult*, int, int)const = 0;
    virtual char* PQfname(const ::PGresult*, int)const = 0;
    virtual char* PQcmdTuples(::PGresult*)const = 0;
};

const LibPqLayerInterface& libpq_layer()noexcept;

LibPqLayerInterface* set_libpq_layer_impl(LibPqLayerInterface*)noexcept;

}//namespace LibPg::Detail
}//namespace LibPg

#endif//LIBPQ_LAYER_INTERFACE_HH_024950A4FCE5C9F28B76BEEBB9503657
