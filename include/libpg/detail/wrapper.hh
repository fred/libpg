/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WRAPPER_HH_8E3D39F314C04EED6AD03CC24CD5225E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define WRAPPER_HH_8E3D39F314C04EED6AD03CC24CD5225E

#include "libpg/null_value.hh"

#include <boost/date_time.hpp>
#include <boost/optional.hpp>

#include <chrono>
#include <ctime>
#include <string>
#include <type_traits>

namespace LibPg {

class TimeZone;

namespace Detail {

boost::optional<std::string> wrap_bool_into_psql_representation(bool is_true);

template <typename T>
using IsBool = std::is_same<std::decay_t<T>, bool>;

template <typename T>
using IsNumeric = std::integral_constant<bool, std::is_integral<std::decay_t<T>>::value && !IsBool<T>::value>;

template <typename SupportedType,
          std::enable_if_t<IsBool<SupportedType>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return wrap_bool_into_psql_representation(std::forward<SupportedType>(src));
}

boost::optional<std::string> wrap_null_value_into_psql_representation();

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, NullValueT>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&&)
{
    return wrap_null_value_into_psql_representation();
}

template <typename SupportedType,
          std::enable_if_t<IsNumeric<SupportedType>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return boost::optional<std::string>{std::to_string(std::forward<SupportedType>(src))};
}

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, std::string>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return boost::optional<std::string>{std::forward<SupportedType>(src)};
}

template <int n, std::enable_if_t<(0 < n)>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(const char (&src)[n])
{
    return boost::optional<std::string>{std::string{src, n - 1}};
}

void not_null_string_required(const char*);

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, const char*>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    not_null_string_required(src);
    return boost::optional<std::string>{std::string{std::forward<SupportedType>(src)}};
}

boost::optional<std::string> wrap_time_zone_into_psql_representation(const TimeZone&);

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, TimeZone>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return wrap_time_zone_into_psql_representation(std::forward<SupportedType>(src));
}

boost::optional<std::string> wrap_boost_date_into_psql_representation(const boost::gregorian::date&);

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, boost::gregorian::date>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return wrap_boost_date_into_psql_representation(std::forward<SupportedType>(src));
}

boost::optional<std::string> wrap_boost_ptime_into_psql_representation(const boost::posix_time::ptime&);

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, boost::posix_time::ptime>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return wrap_boost_ptime_into_psql_representation(std::forward<SupportedType>(src));
}

template <typename Clock, typename Duration>
boost::optional<std::string> wrap_into_psql_representation(const std::chrono::time_point<Clock, Duration>& time)
{
    const auto nanoseconds_since_epoch = std::chrono::duration_cast<std::chrono::nanoseconds>(time.time_since_epoch()).count();
#ifdef BOOST_DATE_TIME_HAS_NANOSECONDS
    const auto ptime = boost::posix_time::from_time_t(std::time_t{0}) + boost::posix_time::nanoseconds(nanoseconds_since_epoch);
#else
    const auto ptime = boost::posix_time::from_time_t(std::time_t{0}) + boost::posix_time::microseconds((nanoseconds_since_epoch + 500) / 1000);
#endif
    return wrap_into_psql_representation(ptime);
}

boost::optional<std::string> wrap_boost_time_duration_into_psql_representation(const boost::posix_time::time_duration&);

template <typename SupportedType,
          std::enable_if_t<std::is_same<std::decay_t<SupportedType>, boost::posix_time::time_duration>::value>* = nullptr>
boost::optional<std::string> wrap_into_psql_representation(SupportedType&& src)
{
    return wrap_boost_time_duration_into_psql_representation(std::forward<SupportedType>(src));
}

template <typename Rep, typename Period>
boost::optional<std::string> wrap_into_psql_representation(const std::chrono::duration<Rep, Period>& duration)
{
#ifdef BOOST_DATE_TIME_HAS_NANOSECONDS
    const boost::posix_time::time_duration boost_duration{boost::posix_time::nanoseconds{std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count()}};
#else
    const boost::posix_time::time_duration boost_duration{boost::posix_time::microseconds{std::chrono::duration_cast<std::chrono::microseconds>(duration).count()}};
#endif
    return wrap_into_psql_representation(boost_duration);
}

}//namespace LibPg::Detail
}//namespace LibPg

#endif//WRAPPER_HH_8E3D39F314C04EED6AD03CC24CD5225E
