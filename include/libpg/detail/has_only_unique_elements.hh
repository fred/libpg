/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef HAS_ONLY_UNIQUE_ELEMENTS_HH_60CEF022DAEF2A4A797563CF1693E6B3//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define HAS_ONLY_UNIQUE_ELEMENTS_HH_60CEF022DAEF2A4A797563CF1693E6B3

#include <tuple>
#include <type_traits>
#include <utility>

namespace LibPg {
namespace Detail {

template <typename, typename> struct IsElement;

template <typename Type, typename ...Seq>
struct IsElement<Type, std::tuple<Type, Seq...>>
    : std::true_type {};

template <typename Type, typename Head, typename ...Tail>
struct IsElement<Type, std::tuple<Head, Tail...>>
    : std::integral_constant<bool, IsElement<Type, std::tuple<Tail...>>::value> {};

template <typename Type>
struct IsElement<Type, std::tuple<>>
    : std::false_type {};

template <typename...> struct HasOnlyUniqueElements;

template <typename Head, typename ...Tail>
struct HasOnlyUniqueElements<Head, Tail...>
    : std::integral_constant<bool, !IsElement<Head, std::tuple<Tail...>>::value &&
                                   HasOnlyUniqueElements<Tail...>::value> {};

template <>
struct HasOnlyUniqueElements<>
    : std::true_type {};

template <typename ...Elements>
using HasOnlyDecayedElements = std::integral_constant<bool, std::is_same<std::tuple<Elements...>, std::tuple<std::decay_t<Elements>...>>::value>;

template <typename, typename> struct IsSubset;

template <typename MainSet, typename Head, typename ...Tail>
struct IsSubset<std::tuple<Head, Tail...>, MainSet>
    : std::integral_constant<bool, IsElement<Head, MainSet>::value &&
                                   IsSubset<std::tuple<Tail...>, MainSet>::value>
{ };

template <typename Rhs>
struct IsSubset<std::tuple<>, Rhs>
    : std::integral_constant<bool, true>
{ };

template <typename SetA, typename SetB>
struct HasTheSameElements
    : std::integral_constant<bool, IsSubset<SetA, SetB>::value &&
                                   IsSubset<SetB, SetA>::value>
{ };

}//namespace LibPg::Detail
}//namespace LibPg

#endif//HAS_ONLY_UNIQUE_ELEMENTS_HH_60CEF022DAEF2A4A797563CF1693E6B3
